﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using WebAutomation;

namespace ClickPortal.UI.Tests.Automation.Pages
{
    
    public class SelectOneOrMorePersons
    {
        public CCElement
            OkButton = new CCElement(By.CssSelector("input[value='OK']")),
            BtnCancel = new CCElement(By.CssSelector("input[value='Cancel']")),
            BtnAddUser = new CCElement(By.Id("createUserSelection")),
            DivTable = new CCElement(By.Id("_webrRSV_DIV_0"));
        // TODO
        // FilterBy, Go, Clear, Add User, Cancel
            

        public static string WindowTitle = "Select One or More Persons";

        public void selectPerson(string lastName)
        {
            PopUpWindow.SwitchTo(SelectOneOrMorePersons.WindowTitle);
            // Switch to the frame within this popup dialog
            //CCWebUIAuto.webDriver.SwitchTo()
            //    .Frame(CCWebUIAuto.webDriver.FindElement(By.Id("ifrmAttributeTable")));
            List<CCElement> lastNameTds = DivTable.GetDescendants(".//table/tbody/tr/td[2]");
            CCElement lastNameTd = lastNameTds.FirstOrDefault(n => n.Text == lastName);
            CCElement parentTr = lastNameTd.GetParent();
            CCElement targetCheckBox = parentTr.GetDescendant(".//td/input[1]");
            targetCheckBox.Click();
            OkButton.Click();
        }
    }
}
