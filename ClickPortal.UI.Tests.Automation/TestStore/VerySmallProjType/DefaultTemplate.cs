﻿using CCWebUIAuto;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.PrimitiveElements;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.Pages.TestStore.VerySmallProjType
{
	public class DefaultTemplate
	{
		public readonly string InstanceId;

		public TextBox
			TxtStatus = new TextBox(By.XPath("//*[@id='com.webridge.entity.Entity[OID[9A51E151F68EA94BB112EE7E2DF25837]]_control']"));

		public readonly Link
			LnkEditProject = new Link(By.LinkText("Edit Project")),
			LnkExecuteEasyActivityDropdown = new Link(By.LinkText("Execute Easy Activity...")),
			LnkToggleStateActivity = new Link(By.LinkText("Toggle State"));

		public DefaultTemplate(string instanceId)
		{
			InstanceId = instanceId;
		}

		public void NavigateTo()
		{
			Web.Navigate(Store.BaseUrl +
							   "/Rooms/DisplayPages/LayoutInitial?ProjectType=_VerySmallProjType&ProjectID=" + InstanceId);
		}
	}
}
