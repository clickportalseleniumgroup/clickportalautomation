﻿using CCWebUIAuto.Pages.BasePages;

namespace ClickPortal.UI.Tests.Automation.TestStore
{
	public class Home : LayoutInitialBasedPage
	{
		public Home() : base("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity[OID[DEB587629FEF584FA5EDB40FA974D7E6]]") { }
	}
}
