﻿using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.Components;

namespace ClickPortal.UI.Tests.Automation.TestStore
{
	class ProjectListingTesting : LayoutInitialBasedPage
	{
		public readonly ProjectListingComponent 
			SingleType = new ProjectListingComponent("A Single Type Sorted by int in Script"),
			TwoTypes = new ProjectListingComponent("Two Types Sorted By Date Created Minimal Paging Bar"),
			ReplaceSetInScript = new ProjectListingComponent("Replace the Project Set UI Configure _TestProjType in Script Set _VerySmallProjType"),
			NullProjectsSet = new ProjectListingComponent("Null Project Set "),
			IconLinks = new ProjectListingComponent("Icon SF and Activity Links"),
			TextLinks = new ProjectListingComponent("Text SF and Activity Links");

		public ProjectListingTesting() : base("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity[OID[F97CCE0E515AFB4AA974101E5267874A]]") { }
	}
}
