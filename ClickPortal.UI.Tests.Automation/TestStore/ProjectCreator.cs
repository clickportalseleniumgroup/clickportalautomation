﻿using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.PrimitiveElements;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.Pages.TestStore
{
	public class ProjectCreator : LayoutInitialBasedPage
	{
		public readonly Select
			SelProjectType = new Select(By.Name("projectTypeBeingSubmitted"));

		public readonly Button
			BtnCreate = new Button(By.CssSelector("input[value='Create...']"));

		public ProjectCreator() : base("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity[OID[7877F9B25DB48D45A56A4E11D806AAA1]]") { }
	}
}
