﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.PrimitiveElements;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestStore.TestProjType
{
	public class ConfirmLoginNoMessageActivity : ActivityPopup
	{
		public readonly TextBox TxtNotesAsStr = new TextBox(By.Name("_TestProjType_Confirm Login without Message.notesAsStr"));

		public readonly LoginConfirmationDialog DlgLoginConfirmation = new LoginConfirmationDialog();

		public ConfirmLoginNoMessageActivity(string id) : base(id, "Confirm Login without Message") {}
	}
}
