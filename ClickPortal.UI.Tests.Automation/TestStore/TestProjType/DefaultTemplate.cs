﻿using CCWebUIAuto;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages.Components;
using CCWebUIAuto.PrimitiveElements;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.Pages.TestStore.TestProjType
{
	public class DefaultTemplate
	{
		public readonly string InstanceId;

		public readonly Container
			DivStatus = new Container(By.XPath("//*[@id='com.webridge.entity.Entity[OID[9A51E151F68EA94BB112EE7E2DF25837]]_control']"));

		public readonly Link
			LnkEditProject = new Link(By.LinkText("Edit Project")),
			LnkViewProject = new Link(By.LinkText("View Project")),
			LnkExecuteEasyActivityDropdown = new Link(By.LinkText("Execute Easy Activity...")),
			LnkToggleStateActivity = new Link(By.LinkText("Toggle State")),
			LnkConfirmLoginNoMessageActivity = new Link(By.LinkText("Confirm Login without Message")),
			LnkHistoryTab = new Link(By.LinkText("History")),
			LnkReviewerNotesTab = new Link(By.LinkText("Reviewer Notes")),
			LnkDataTab = new Link(By.LinkText("Data")),
			LnkSetsTab = new Link(By.LinkText("Sets")),
			LnkContextualCustomSearchTab = new Link(By.LinkText("Contextual Custom Search")),
			LnkProjectListingTab = new Link(By.LinkText("Project Listing"));

		public readonly Container
			ReadOnlyTxtId = new Container(By.XPath("//span[text()='ID:']/../../td[3]/span"));

		public readonly ProjectLogComponent ProjectLog = new ProjectLogComponent("Project Log");
		public readonly CustomSearchComponent ContextualCustomSearch = new CustomSearchComponent("Contextual Custom Search");
		public readonly ProjectListingComponent ProjectListing = new ProjectListingComponent("Project Listing");
		public readonly ReviewerNotesComponent ReviewerNotes = new ReviewerNotesComponent("Reviewer Notes");

		public readonly EasyActivitiesDropdownLinksContainer EasyActivitiesDropdownLinks = new EasyActivitiesDropdownLinksContainer();

		public class EasyActivitiesDropdownLinksContainer
		{
			public readonly Link
				LnkComment = new Link(By.XPath("//a[@class='hyperlink' and text()='Comment']")),
				LnkToggleState = new Link(By.XPath("//a[@class='hyperlink' and text()='Toggle State']")),
				SendMail = new Link(By.XPath("//a[@class='hyperlink' and text()='Send Mail']")),
				ValidateSmartForm = new Link(By.XPath("//a[@class='hyperlink' and text()='Validate SmartForm']")),
				ToggleMultiTabTemplate = new Link(By.XPath("//a[@class='hyperlink' and text()='Toggle Multi-Tab Template']"));
		}

		public DefaultTemplate(string instanceId)
		{
			InstanceId = instanceId;
		}

		public void NavigateTo()
		{
			Web.Navigate(Store.BaseUrl +
							   "/Rooms/DisplayPages/LayoutInitial?ProjectType=_TestProjType&ProjectID=" + InstanceId);
		}
	}
}
