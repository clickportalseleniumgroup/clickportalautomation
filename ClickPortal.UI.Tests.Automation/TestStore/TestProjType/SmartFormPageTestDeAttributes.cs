﻿using CCWebUIAuto.EntityViewControls;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.PrimitiveElements;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.Pages.TestStore.TestProjType
{
	public class SmartFormPageTestDeAttributes : SmartFormPage
	{
		public readonly Checkbox
			ChkBoolean = new Checkbox(By.Name("_TestProjType.customAttributes.testDE.customAttributes.boolean"));

		public readonly TextBox
			TxtString = new TextBox(By.Name("_TestProjType.customAttributes.testDE.customAttributes.string")),
			TxtInteger = new TextBox(By.Name("_TestProjType.customAttributes.testDE.customAttributes.integer")),
			TxtCurrency = new TextBox(By.Name("_TestProjType.customAttributes.testDE.customAttributes.currency")),
			TxtFloat = new TextBox(By.Name("_TestProjType.customAttributes.testDE.customAttributes.float")),
			TxtDouble = new TextBox(By.Name("_TestProjType.customAttributes.testDE.customAttributes.double")),
			TxtDate = new TextBox(By.Name("_TestProjType.customAttributes.testDE.customAttributes.date"));

		public readonly SetOfDataEntryCdtEntityViewControl
			SetTestDe = new SetOfDataEntryCdtEntityViewControl("_TestProjType.customAttributes.testDE.customAttributes.setTestDE");

		public readonly PersonEntity
			EntPersonEntity = new PersonEntity("_TestProjType.customAttributes.testDE.customAttributes.entityPerson");

	}

}
