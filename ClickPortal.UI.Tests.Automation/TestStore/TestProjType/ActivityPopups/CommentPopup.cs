﻿using System;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.PrimitiveElements;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestStore.TestProjType.ActivityPopups
{
	public class CommentPopup : ActivityPopup
	{
		public readonly TextBox TxtComment = new TextBox(By.Name("_TestProjType_Comment.notesAsStr"));

		public CommentPopup(String projectId) : base(projectId, "Comment") { }
	}
}
