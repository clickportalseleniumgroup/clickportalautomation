﻿using System;
using CCWebUIAuto.Pages.BasePages;

namespace ClickPortal.UI.Tests.Automation.TestStore.TestProjType.ActivityPopups
{
	class ToggleStatePopup : ActivityPopup
	{
		public ToggleStatePopup(String projectId) : base(projectId, "Toggle State") { }
	}
}
