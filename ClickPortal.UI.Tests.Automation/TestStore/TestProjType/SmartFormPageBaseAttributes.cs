﻿using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.PrimitiveElements;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.Pages.TestStore.TestProjType
{
	public class SmartFormPageBaseAttributes : SmartFormPage
	{
		public readonly TextBox
			TxtId = new TextBox(By.Name("_TestProjType.ID")),
			TxtName = new TextBox(By.Name("_TestProjType.name"));

		public readonly Checkbox
			ChkToggleState = new Checkbox(By.Name("_TestProjType.customAttributes.toggleState"));
	}
}
