﻿using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.Components;

namespace ClickPortal.UI.Tests.Automation.Pages.TestStore
{
	class CustomSearchTesting : LayoutInitialBasedPage
	{
		public readonly CustomSearchComponent ListOfSearches = new CustomSearchComponent("List of Searches");
		public readonly CustomSearchComponent PrimitiveCustomAttributes = new CustomSearchComponent("TestProjType Custom Attribute Primitives");
		public readonly CustomSearchComponent EntitiesAndExportableMethods = new CustomSearchComponent("TestProjType Entities and Exportable Methods");
		public readonly CustomSearchComponent UserInterfaceMethods = new CustomSearchComponent("TestProjType UI Methods");
		public readonly CustomSearchComponent Sets = new CustomSearchComponent("TestProjType Sets");

		public CustomSearchTesting() : base("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity[OID[33FF860D40BF6F4592A0593D14A846E6]]") { }
	}
}
