﻿using System;
using System.Threading;
using CCWebUIAuto.EntityViewControls;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.PrimitiveElements;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestStore.CdtForms
{
	public class PersonQuickCreationPage : CCPage
	{
		public readonly TextBox TxtFirstName = new TextBox(By.Name("Person.firstName"));

		public readonly TextBox TxtLastName = new TextBox(By.Name("Person.lastName"));

		public readonly TextBox TxtEmail = new TextBox(By.Name("Person.contactInformation.emailPreferred.eMailAddress"));

		public readonly OrganizationEntity EntEmpEntity = new OrganizationEntity("Person.employer");

		public readonly Button BtnOk = new Button(By.XPath("//button[contains(@id, '_OK')]"));

		public readonly Button BtnCancel = new Button(By.XPath("//button[contains(@id, '_Cancel')]"));

		public override void NavigateTo()
		{
			throw new NotImplementedException();
		}

		public void CreatePerson(String firstName, String lastName, String email)
		{
			Thread.Sleep(1500); // see if a delay helps prevent the problem of the first name being erased
			const String linkText = "Click Commerce, Inc.";
			TxtFirstName.Value = firstName;
			TxtLastName.Value = lastName;
			TxtEmail.Value = email;
			EntEmpEntity.BtnSelect.Click();
			var empChooser = new ChooserPopup("Organization");
			empChooser.SwitchTo();
			empChooser.SelectValue(linkText);
			empChooser.BtnOk.Click();
			empChooser.SwitchBackToParent(WaitForPopupToClose.Yes);
			Wait.Until(d => EntEmpEntity.LnkCdtEntLink.Text.Equals(linkText));
			BtnOk.Click();
		}
	}
}
