﻿using System;
using CCWebUIAuto.EntityViewControls;
using CCWebUIAuto.PrimitiveElements;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.Pages.TestStore.CdtForms
{
	public class AddTestDePopup : DataEntryCdtAddDataPopup
	{
		public override String Title { get { return "Add TestDE"; } }

		public readonly TextBox
			TxtString = new TextBox(By.XPath("//input[@name='_TestDE.customAttributes.string']"));
	}
}
