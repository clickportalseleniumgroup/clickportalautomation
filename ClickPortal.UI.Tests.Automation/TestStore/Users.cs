﻿using CCWebUIAuto.Helpers;

namespace ClickPortal.UI.Tests.Automation.Pages.TestStore
{
	public class Users
	{
		public static readonly UserAccount Admin = new UserAccount("administrator", "1234");
		public static readonly UserAccount TestRead = new UserAccount("testRead", "1234");
		public static readonly UserAccount TestWrite = new UserAccount("testWrite", "1234");
		public static readonly UserAccount TestRespond = new UserAccount("testRespond", "1234");
		public static readonly UserAccount Pi = new UserAccount("pi", "1234");
		public static readonly UserAccount Irbd = new UserAccount("irbd", "1234");
		public static readonly UserAccount Comm4 = new UserAccount("comm4", "1234");
	}
}
