﻿using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.Components;

namespace ClickPortal.UI.Tests.Automation.Pages.TestStore
{
	public class ProjectsCustomSearch : LayoutInitialBasedPage
	{
		public readonly CustomSearchComponent ProjectsComponent = new CustomSearchComponent("Custom Search");

		public ProjectsCustomSearch() : base("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity[OID[DA10EF10AC06224EB82013CBA4193FE6]]") { }
	}
}
