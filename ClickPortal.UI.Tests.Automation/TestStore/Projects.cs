﻿using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.Components;

namespace ClickPortal.UI.Tests.Automation.Pages.TestStore
{
	public class Projects : LayoutInitialBasedPage
	{
		public readonly ProjectListingComponent ProjectsComponent = new ProjectListingComponent("Project Listing");

		public Projects() : base("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity[OID[E4F6319EACFDAB43BD9BA12FF672D6AC]]") { }
	}
}
