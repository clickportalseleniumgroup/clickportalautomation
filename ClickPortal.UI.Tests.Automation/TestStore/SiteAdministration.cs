﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CCWebUIAuto.Pages.BasePages;

namespace ClickPortal.UI.Tests.Automation.TestStore
{
    public class SiteAdministration : LayoutInitialBasedPage
    {
        public SiteAdministration() : base("/Rooms/DisplayPages/LayoutInitial?Container=com.webridge.entity.Entity%5BOID%5BE446EE42C68B2A49A5E7D36FAA86D39C%5D%5D") { }

        // content
        // contacts
        // project configuration
        // notifications
        // site options
        // help
    }
}
