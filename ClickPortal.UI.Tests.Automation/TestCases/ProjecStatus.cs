﻿using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.BasePages.DataTypeCenter;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using NUnit.Framework;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
    [TestFixture]
    class ProjectStatus : BaseTest
    {
        public const string DataTypeName = "ProjectStatus";

        [Test(Description = "1.3.1 ProjectStatus Add Attribute -- Boolean")]
        public void AddAttribute_Boolean()
        {       
            var myDataTypePropertiesTab = new PropertiesTab(DataTypeName);
            const string prefix = "boolean_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            myDataTypePropertiesTab.NavigateTo();
            myDataTypePropertiesTab.CreateNewCustomAttribute(
                CustomAttributeDataType.Boolean,
                displayName,
                internalName,
                description,
                booleanDisplay: "On/Off");

            PopUpWindow.SwitchTo("Data Type (Project Status)");
            Assert.IsTrue(myDataTypePropertiesTab.CustomAttributeExists(internalName),"internalName not found.");
        }

        [Test(Description = "1.3.1 ProjectStatus Add Attribute -- Currency")]
        public void AddAttribute_Currency()
        {
            var myDataTypePropertiesTab = new PropertiesTab(DataTypeName);
            const string prefix = "currency_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            myDataTypePropertiesTab.NavigateTo();
            myDataTypePropertiesTab.CreateNewCustomAttribute(
                CustomAttributeDataType.Currency, 
                displayName, 
                internalName, 
                description, 
                currencyType: "Chilean Peso/CL [1146] ($)");

            PopUpWindow.SwitchTo("Data Type (Project Status)");
            Assert.IsTrue(myDataTypePropertiesTab.CustomAttributeExists(internalName), "internalName could not be found.");

        }

        [Test(Description = "1.3.1 ProjectStatus Add Attributes -- Date")]
        public void AddAttribute_Date()
        {    
            var myDataTypePropertiesTab = new PropertiesTab(DataTypeName);
            const string prefix = "date_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            myDataTypePropertiesTab.NavigateTo();
            myDataTypePropertiesTab.CreateNewCustomAttribute(
                CustomAttributeDataType.Date,
                displayName,
                internalName,
                description,
                defaultValue: "1/1/01", 
                displayFormat: "Verbose");
            PopUpWindow.SwitchTo("Data Type (Project Status)");
            Assert.IsTrue(myDataTypePropertiesTab.CustomAttributeExists(internalName), "internalName not found.");
        }

        [Test(Description = "1.3.1 ProjectStatus Add Attributes -- Document Content")]
        public void AddAttribute_DocumentContent()
        {
            var myDataTypePropertiesTab = new PropertiesTab(DataTypeName);
            const string prefix = "docContent_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            myDataTypePropertiesTab.NavigateTo();
            myDataTypePropertiesTab.CreateNewCustomAttribute(
                CustomAttributeDataType.DocumentContent,
                displayName,
                internalName,
                description,
                defaultValue: "doc test", 
                readOnlyString: "readOnlyDocString");
            PopUpWindow.SwitchTo("Data Type (Project Status)");
            Assert.IsTrue(myDataTypePropertiesTab.CustomAttributeExists(internalName), "internalName does not exist when expected.");
        }

        [Test(Description = "1.3.1 ProjectStatus Add Attributes -- Double")]
        public void AddAttribute_Double()
        {
            var myDataTypePropertiesTab = new PropertiesTab(DataTypeName);
            const string prefix = "double_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            myDataTypePropertiesTab.NavigateTo();
            myDataTypePropertiesTab.CreateNewCustomAttribute(
                CustomAttributeDataType.Double,
                displayName,
                internalName,
                description, 
                defaultValue: "1.23456789");
            PopUpWindow.SwitchTo("Data Type (Project Status)");
            Assert.IsTrue(myDataTypePropertiesTab.CustomAttributeExists(internalName), "internalName find aDouble when expected.");
        }

        [Test(Description = "1.3.1 ProjectStatus Add Attributes -- Entity of Type")]
        public void AddAttribute_EntityType()
        {
            var myDataTypePropertiesTab = new PropertiesTab(DataTypeName);
            const string prefix = "entity_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            myDataTypePropertiesTab.NavigateTo();
            // TODO need to put in default value, but need to select EType
            myDataTypePropertiesTab.CreateNewCustomAttribute(
                CustomAttributeDataType.EntityOfType,
                displayName,
                internalName,
                description, 
                readOnlyString: "PersonEntityType_ReadOnlyString", 
                entityType: "Person");
            PopUpWindow.SwitchTo("Data Type (Project Status)");
            Assert.IsTrue(myDataTypePropertiesTab.CustomAttributeExists(internalName), "internalName cannot be found.");
        }

        [Test(Description = "1.3.1 ProjectStatus Add Attributes -- Entity of Float")]
        public void AddAttribute_Float()
        {
            var myDataTypePropertiesTab = new PropertiesTab(DataTypeName);
            const string prefix = "float_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            myDataTypePropertiesTab.NavigateTo();
            myDataTypePropertiesTab.CreateNewCustomAttribute(
                CustomAttributeDataType.Float,
                displayName,
                internalName,
                description, 
                defaultValue: "1.234567");
            PopUpWindow.SwitchTo("Data Type (Project Status)");
            Assert.IsTrue(myDataTypePropertiesTab.CustomAttributeExists(internalName), "internalName not found.");
        }

        [Test(Description = "1.3.1 ProjectStatus Add Attributes -- Integer")]
        public void AddAttribute_Integer()
        {
            var myDataTypePropertiesTab = new PropertiesTab(DataTypeName);
            const string prefix = "integer_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            myDataTypePropertiesTab.NavigateTo();
            myDataTypePropertiesTab.CreateNewCustomAttribute(
                CustomAttributeDataType.Integer,
                displayName,
                internalName,
                description,  
                defaultValue: "1");
            PopUpWindow.SwitchTo("Data Type (Project Status)");
            Assert.IsTrue(myDataTypePropertiesTab.CustomAttributeExists(internalName));
        }

        [Test(Description = "1.3.1 ProjectStatus Add Attributes -- SetOfType ")]
        public void AddAttribute_SetOfType()
        {
            var myDataTypePropertiesTab = new PropertiesTab(DataTypeName);
            const string prefix = "eset_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            myDataTypePropertiesTab.NavigateTo();
            myDataTypePropertiesTab.CreateNewCustomAttribute(
                CustomAttributeDataType.SetOfType,
                displayName,
                internalName,
                description,  
                setOfType: "Project", 
                readOnlyString: "SetTypesReadOnlyString");
            PopUpWindow.SwitchTo("Data Type (Project Status)");
            Assert.IsTrue(myDataTypePropertiesTab.CustomAttributeExists(internalName), "internalName not found.");
        }

        [Test(Description = "1.3.1 ProjectStatus Add Attributes -- String  ")]
        public void AddAttribute_String()
        {
            var myDataTypePropertiesTab = new PropertiesTab(DataTypeName);
            const string prefix = "string_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            myDataTypePropertiesTab.NavigateTo();
            myDataTypePropertiesTab.CreateNewCustomAttribute(
                CustomAttributeDataType.String,
                displayName,
                internalName,
                description, 
                defaultValue: "stringDefaultValue");
            PopUpWindow.SwitchTo("Data Type (Project Status)");
            Assert.IsTrue(myDataTypePropertiesTab.CustomAttributeExists(internalName), "internalName not found.");
        }

        [Test(Description = "1.3.1 ProjectStatus Add Attributes -- String Multiline ")]
        public void AddAttribute_multiString()
        {
            var myDataTypePropertiesTab = new PropertiesTab(DataTypeName);
            const string prefix = "multiString_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            myDataTypePropertiesTab.NavigateTo();
            myDataTypePropertiesTab.CreateNewCustomAttribute(
                CustomAttributeDataType.String,
                displayName,
                internalName,
                description, 
                defaultValue: "multiStringDefaultValue", 
                multiLineString: true);
            PopUpWindow.SwitchTo("Data Type (Project Status)");
            Assert.IsTrue(myDataTypePropertiesTab.CustomAttributeExists(internalName), "internalName not found");
        }

        [Test(Description = "1.3.5 ProjectStatus Create View")]
        public void CreateView()
        {
            var viewsTab = new ViewsTab(DataTypeName);
            var viewName = "testView_" + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            viewsTab.NavigateTo();
            Assert.IsFalse(viewsTab.ViewExists(viewName), "view already exists.");
            viewsTab.CreateNewView(viewName);
            PopUpWindow.SwitchTo("Data Type (Project Status)");
            Assert.IsTrue(viewsTab.ViewExists(viewName), "view does not exist.");
        }

        // It is tempting to split these into different tests for purity, but since the edit and delete
        // tests would have to create attributes, it's easiest to combine them into one.  An alternative
        // would be to have pre-created attributes for edit and deletion tests.
        [Test(Description = "1.3.1, 1.3.3, 1.3.4 Add, edit, and delete custom attribute")]
        public void ManageCustomAttributes()
        {
            var propertiesTab = new PropertiesTab(DataTypeName);
            var internalName = DataGen.String(8);
            var displayName = DataGen.String(8);
            var modifiedDisplayName = DataGen.String(8);

            // Login and open ProjectStatus
            Store.LoginAsUser(Users.Admin);
            propertiesTab.NavigateTo();

            // Create new attribute
            Assert.IsFalse(propertiesTab.CustomAttributeExists(internalName));
            propertiesTab.CreateNewCustomAttribute(CustomAttributeDataType.Boolean, displayName, internalName);
            //PopUpWindow.SwitchTo("Project Type (VerySmallProjType)");
            PopUpWindow.SwitchTo("Data Type (Project Status)");
            Assert.IsTrue(propertiesTab.CustomAttributeExists(internalName));

            // Edit attribute
            Assert.IsTrue(propertiesTab.CustomAttributesForm.Text.Contains(displayName));
            Assert.IsFalse(propertiesTab.CustomAttributesForm.Text.Contains(modifiedDisplayName));
            propertiesTab.EditCustomAttribute(CustomAttributeDataType.Boolean, modifiedDisplayName, internalName);
            PopUpWindow.SwitchTo("Data Type (Project Status)");
            Assert.IsFalse(propertiesTab.CustomAttributesForm.Text.Contains(displayName));
            Assert.IsTrue(propertiesTab.CustomAttributesForm.Text.Contains(modifiedDisplayName));

            // Remove attribute
            propertiesTab.DeleteCustomAttribute(internalName);
            Assert.IsFalse(propertiesTab.CustomAttributeExists(internalName));
        }
    }
}
