﻿using System;
using System.Xml;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.PrimitiveElements;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using NUnit.Framework;
using OpenQA.Selenium;


namespace ClickPortal.UI.Tests.Automation
{
    [TestFixture]
    public class InstallerTests : BaseTest
    {
		[Test(Description = "This test just utilizes the setup initialize() to just do a simple login")]
        public void SimpleAdminLogin()
        {
            Store.LoginAsUser(Users.Admin);
 
            // DO NOTHING BUT LOG IN TO TEST THAT THE SITE IS ACTUALLY UP
            // Assert that the login worked and you are on the Site Administration page (logging in with administrator)
            Assert.IsTrue(new Container(By.XPath("//span[text() = 'Site Administration']")).Exists);
        }

        [Test(Description = "This test verifies patch build, Click Portal Framework version, and Click Portal (store) version")]
        public void LoginAndVerifyBuild()
        {
            Store.LoginAsUser(Users.Admin);

            string cpversion = ClickPortalUI.AutoConfig["CPVersion"];
            string cpbuild = ClickPortalUI.AutoConfig["PortalBuild"];

            var versionInformation = new VersionInformation();
            versionInformation.NavigateTo();
            Assert.IsTrue(versionInformation.ValidateClickPortalFrameworkVersion(cpversion),"Framework version is not:  " + cpversion);
            Assert.IsTrue(versionInformation.ValidateClickPortalStoreVersion(cpversion), "Portal version (store) is not:  " + cpversion);
            Assert.IsTrue(versionInformation.ValidatePatchId(cpbuild), "Patch build id cannot be found: " + cpbuild);
        }
    }
}
