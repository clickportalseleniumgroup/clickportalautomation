﻿using System.Threading;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages.ProjectTypeCenter;
using CCWebUIAuto.PrimitiveElements;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
    public class ProjectTypeCenterTemplatesTabTests : BaseTest
    {
        const string TestProjType = "_VerySmallProjType";

        [Test(Description = "1.8.1, 1.8.2, 1.8.9, Take a template through it's life cycle")]
        public void CreateEditDeleteTemplate()
        {
            var name = "TestTemplate_" + DataGen.String(8);
            var modName = "TestTemplate_" + DataGen.String(8);
            var templatesTab = new TemplatesTab(TestProjType);
            Store.LoginAsUser(Users.Admin);
            templatesTab.NavigateTo();
            templatesTab.CreateNewTemplate(name, "2 Column Tabbed");
            Assert.IsTrue(new Link(By.LinkText(name)).Exists);
            templatesTab.SetProperties(name, modName);
			Assert.IsFalse(new Link(By.LinkText(name)).Exists);
			Assert.IsTrue(new Link(By.LinkText(modName)).Exists);
            templatesTab.DeleteTemplate(modName);
			Assert.IsFalse(new Link(By.LinkText(name)).Exists);
			Assert.IsFalse(new Link(By.LinkText(modName)).Exists);
        }

        [Test(Description = "1.8.3, Add template component")]
        public void AddTemplateComponent()
        {
            var componentName = "TestComponent" + DataGen.String(4);
            var templatesTab = new TemplatesTab(TestProjType);
			Store.LoginAsUser(Users.Admin);
            templatesTab.NavigateTo();

            // open the template components for the default template
            templatesTab.OpenTemplateComponentsPopup("Default");
            var parentWindow = templatesTab.GetTitle();
            PopUpWindow.SwitchTo("Edit Page Layout");

            // in area 0 (left side) add a text block component
            var selComponent = new Select(By.Name("Column0"));
            selComponent.SelectOption("Text Block");
            Thread.Sleep(500);
            var btnAdd = new Button(By.XPath("//select[@name='Column0']/../../TD[2]/a/img"));
            btnAdd.Click();
            Thread.Sleep(500);

            // open the properties of the new component, first the edit drop down then the properties link
            var lnkEditDropdown = new Link(By.XPath("//select[@name='Column0']/../../../../../table[last()]/tbody/tr/td[2]/a"));
            lnkEditDropdown.Click();
            Thread.Sleep(500);
            var btnProperties = new Button(By.XPath("//select[@name='Column0']/../../../../../table[last()]/tbody/tr/td[2]/div/div/table/tbody/tr[1]/td"));
            btnProperties.Click();

            // set the component's display name
            PopUpWindow.SwitchTo("Text Block Properties");
            var title = new TextBox(By.Name("RoomComponent.DisplayName"));
            title.Value = componentName;
            var btnOk = new Button(By.CssSelector("input[value='OK']"));
            btnOk.Click();
            Thread.Sleep(500);

            // close the template editor
            PopUpWindow.SwitchTo("Edit Page Layout");
            btnOk.Click();
            Thread.Sleep(500);
            PopUpWindow.SwitchTo(parentWindow);

            // go to the workspace and verify the new component is present
            var workspace = new Pages.TestStore.VerySmallProjType.DefaultTemplate("a-00000001");
            workspace.NavigateTo();
            var divComponent = new Container(By.XPath("//div[@data-display-name='" + componentName + "']"));
            Assert.IsTrue(divComponent.Exists);
        }

        [Test(Description="1.8.6, Workspace renders correctly with standard tabs")]
        public void WorkspaceRendersWithStandardTabs()
        {
			var templatesTab = new TemplatesTab("_TestProjType");
			Store.LoginAsUser(Users.Admin);
            templatesTab.NavigateTo();
            templatesTab.SetProperties("Default", useAsynchTabs: false);

			var workspace = new Pages.TestStore.TestProjType.DefaultTemplate("X-00000001");
            workspace.NavigateTo();
            var divProjectLog = new Container(By.XPath("//div[@data-display-name='Project Log']"));
            Assert.True(divProjectLog.Exists);
            workspace.LnkProjectListingTab.Click();
            var divProjectInfo = new Container(By.XPath("//div[@data-display-name='Project Listing']"));
            Wait.Until(d => divProjectInfo.Exists);
            Assert.True(divProjectInfo.Exists);
        }

        [Test(Description = "1.8.7, Workspace renders correctly with async tabs")]
        public void WorkspaceRendersWithAsyncTabs()
        {
			var templatesTab = new TemplatesTab("_TestProjType");
            Store.LoginAsUser(Users.Admin);
            templatesTab.NavigateTo();
            templatesTab.SetProperties("Default", useAsynchTabs: true);

			var workspace = new Pages.TestStore.TestProjType.DefaultTemplate("X-00000001");
            workspace.NavigateTo();
            var divProjectLog = new Container(By.XPath("//div[@data-display-name='Project Log']"));
			Wait.Until(d => divProjectLog.Exists);
	        Thread.Sleep(1500);
            workspace.LnkProjectListingTab.Click();
            var divProjectInfo = new Container(By.XPath("//div[@data-display-name='Project Listing']"));
            Wait.Until(d => divProjectInfo.Exists);
        }
    }
}