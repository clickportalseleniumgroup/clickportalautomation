﻿using System.Threading;
using CCWebUIAuto.Helpers;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using ClickPortal.UI.Tests.Automation.Pages.TestStore.TestProjType;
using NUnit.Framework;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
	[TestFixture]
	class SmartFormTests : BaseTest
	{
		[Test(Description="Make a change, then prompt for a save")]
		public void PromptForSaveOnExit()
		{
			var projCreatorPage = new ProjectCreator();
			var id = DataGen.String(8);
			var workspace = new DefaultTemplate(id);
			var sfPage1 = new SmartFormPageBaseAttributes();

			Store.LoginAsUser(Users.Admin);
			projCreatorPage.NavigateTo();
			projCreatorPage.NavigateTo(); // clear created successfully message
			projCreatorPage.SelProjectType.SelectOption("TestProjType");
			projCreatorPage.BtnCreate.Click();
			sfPage1.TxtId.Value = id;
			sfPage1.TxtName.Value = id;
			sfPage1.BtnContinue.Click();

			// make no change, receive no prompt
			workspace.NavigateTo();
			workspace.LnkEditProject.Click();
			sfPage1.LnkExit.Click();
			Thread.Sleep(1000);
			Wait.Until(d => !sfPage1.SaveChangesPrompt.BtnSaveAndExit.Displayed);

			// make a change, get prompt, ignore and exit
			workspace.NavigateTo();
			workspace.LnkEditProject.Click();
			sfPage1.TxtName.Value = id + "_modA";
			sfPage1.LnkExit.Click();
			Thread.Sleep(1000);
			Wait.Until(d => sfPage1.SaveChangesPrompt.BtnSaveAndExit.Displayed);
			sfPage1.SaveChangesPrompt.BtnIgnoreChangesAndExit.Click();
			workspace.LnkEditProject.Click();
			Assert.AreEqual(id, sfPage1.TxtName.Value);

			// make a change, save, exit, get no prompt
			workspace.NavigateTo();
			workspace.LnkEditProject.Click();
			sfPage1.TxtName.Value = id + "_modB";
			sfPage1.LnkSave.Click();
			Thread.Sleep(1000);
			sfPage1.LnkExit.Click();
			Thread.Sleep(1000);
			Wait.Until(d => !sfPage1.SaveChangesPrompt.BtnSaveAndExit.Displayed);
			workspace.LnkEditProject.Click(); // should be back at the workspace
			Assert.AreEqual(id + "_modB", sfPage1.TxtName.Value);

			// make a change, get prompt, save and exit
			workspace.NavigateTo();
			workspace.LnkEditProject.Click();
			sfPage1.TxtName.Value = id + "_modC";
			sfPage1.LnkExit.Click();
			Thread.Sleep(1000);
			Wait.Until(d => sfPage1.SaveChangesPrompt.BtnSaveAndExit.Displayed);
			sfPage1.SaveChangesPrompt.BtnSaveAndExit.Click();
			workspace.LnkEditProject.Click();
			Assert.AreEqual(id + "_modC", sfPage1.TxtName.Value);
		}
	}
}
