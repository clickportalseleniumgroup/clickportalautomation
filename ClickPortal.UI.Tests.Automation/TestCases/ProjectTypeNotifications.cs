﻿using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.BasePages.ProjectTypeCenter;
using CCWebUIAuto.PrimitiveElements;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
    [TestFixture]
    public class ProjectTypeNotifications : BaseTest
    {
        public const string TestProjType = "_VerySmallProjType";

        /// <summary>
        /// Possibly a duplicate of test AddActivityNotification() in ProjectTypeActivity
        /// </summary>
        public void A1_CreateNotification()
        {
            var myProjectTypeActivitiesTab = new ActivitiesTab(TestProjType);

            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();
            myProjectTypeActivitiesTab.CreateNewActivity("Act_Test_1_6");
            Assert.IsTrue(myProjectTypeActivitiesTab.VerifyActivityExists("Act_Test_1_6"));

            myProjectTypeActivitiesTab.AddNotification("Comment", "owner", "Comment_Activity_Notification");
			var imageLink = new Button(By.XPath("//a[text()='Comment']/../../td[7]/table/tbody/tr/td/a[3]/img[1]"));
			imageLink.Click();
			var popup = new ActivityNotificationPopup();
			popup.SwitchTo();
			Assert.IsTrue(popup.VerifyNotificationExists("Comment_Activity_Notification"));
			popup.BtnOk.Click();
			popup.SwitchBackToParent();
        }

        // duplicate 
        public void ModNotificationName()
        {
            //reportProgress("1.6.2 Edit");
            //_popup("Activity Notifications Editor")._click(_link("Act_Test_1_6 Activity_Notification"));
            //_popup("Configure E-mail")._setValue(_textbox("EmailNotificationSetting.subject"), "Act_Test_1_6 Activity_Notification_Mod");
            //_popup("Configure E-mail")._click(_button("OK"));
            //_wait(3000);
            //_popup("Activity Notifications Editor")._assertExists(_link("Act_Test_1_6 Activity_Notification_Mod"));
        }

        public void AddCriteriaScript()
        {
            var myProjectTypeActivitiesTab = new ActivitiesTab(TestProjType);

            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();
            //myProjectTypeActivitiesTab.AddNotification("Comment", "owner", "Comment_Activity_Notification_3");
            //myProjectTypeActivitiesTab.OpenActivityNotification("Comment");
            //Assert.IsTrue(myProjectTypeActivitiesTab.ActivationNotificationEditor.VerifyNotificationExists("FOOO"));
            // myProjectTypeActivitiesTab.OpenActivityNotification("Comment");
            // myProjectTypeActivitiesTab.ActivationNotificationEditor.OpenNotificationCriteriaScript("Comment_Activity_Notification_3");

            //Assert.IsTrue(myProjectTypeActivitiesTab.VerifyActivityExists("Act_Test_1_6"));

            //reportProgress("1.6.3 Add Criteria Script");
            //_popup("Activity Notifications Editor")._click(_image("Criteria Script"));
            //_wait(3000);
            //_popup("Workflow Script Editor")._setValue(_textarea("WorkflowScript.script"), "// criteria script");
            //_popup("Workflow Script Editor")._click(_button("OK"));
            //_wait(3000);
            //_popup("Activity Notifications Editor")._click(_image("Criteria Script"));
            //_wait(3000);
            //_popup("Workflow Script Editor")._assertEqual("// criteria script", _textarea("WorkflowScript.script").value);
            //_popup("Workflow Script Editor")._click(_button("OK"));
            //_wait(3000);
        }

        
        
    }
}

//_include("standard.sah"); 


//reportProgress("1.6.4 Add recipients, Single Email Mode");
//_popup("Activity Notifications Editor")._click(_link("Act_Test_1_6 Activity_Notification_Mod"));
//reportProgress("1.6.4.1 using Add Property");
//_popup("Configure E-mail")._click(_button("Add Property..."));
//_wait(3000);
//_click(_span("owner"));
//_click(_button("OK"));
//_wait(3000);
//_popup("Configure E-mail")._setSelected(_select("recipients"), "owner"); // select functions as assert in this case

//reportProgress("1.6.4.2 using Add User");
//_popup("Configure E-mail")._click(_button("Add User ..."));
//_wait(3000);
//_popup("Select One or More Persons")._setValue(_textbox("_webrRSV_FilterValue_0_0"), "Administrator");
//_popup("Select One or More Persons")._click(_button("Go"));
//_wait(3000);
//_popup("Select One or More Persons")._click(_checkbox("webrRSV__SelectedItem_0"));
//_popup("Select One or More Persons")._click(_button("OK"));
//_wait(3000);
//_popup("Configure E-mail")._setSelected(_select("recipients"), "System Administrator"); // select functions as assert in this case

//reportProgress("1.6.4.3 using query");
//_popup("Configure E-mail")._click(_button("..."));
//_wait(3000);
//_popup("FindSavedSearches")._setSelected(_select("searchesList"), "Person Display Properties");
//_popup("FindSavedSearches")._setSelected(_select("returnTypes"), ["(Person Display Properties)"]);
//_popup("FindSavedSearches")._click(_image("OK"));
//_wait(3000);
//_assertEqual(_popup("Configure E-mail")._getValue(_textbox("sQueryName")), "_identity_(Person Display Properties) in Person Display Properties");

//reportProgress("1.6.4.4 using script");
//_popup("Configure E-mail")._click(_link("Custom Recipient Script"));
//_wait(3000);
//_popup("Workflow Script Editor")._setValue(_textarea("WorkflowScript.script"), "// recipient script");
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(3000);
//_popup("Configure E-mail")._click(_link("Custom Recipient Script"));
//_wait(3000);
//_popup("Workflow Script Editor")._assertEqual("// recipient script", _textarea("WorkflowScript.script").value);
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(3000);

//reportProgress("1.6.5 Add recipients, Multi-Email Mode, To");
//_popup("Configure E-mail")._click(_button("Cancel"));
//_wait(3000);
//_popup("Activity Notifications Editor")._click(_link("Act_Test_1_6 Activity_Notification_Mod"));
//_wait(3000);
//_popup("Configure E-mail")._click(_checkbox("oneMailAllRecipient"));
//_popup("Configure E-mail")._click(_button("Apply"));
//_wait(3000);

//reportProgress("1.6.5.1 using Add Property");
//_popup("Configure E-mail")._click(_button("Add Property..."));
//_wait(3000);
//_click(_span("owner"));
//_click(_button("OK"));
//_wait(3000);
//_popup("Configure E-mail")._setSelected(_select("recipients"), "owner"); // select functions as assert in this case

//reportProgress("1.6.5.2 using Add User");
//_popup("Configure E-mail")._click(_button("Add User ..."));
//_wait(3000);
//_popup("Select One or More Persons")._setValue(_textbox("_webrRSV_FilterValue_0_0"), "Administrator");
//_popup("Select One or More Persons")._click(_button("Go"));
//_wait(3000);
//_popup("Select One or More Persons")._click(_checkbox("webrRSV__SelectedItem_0"));
//_popup("Select One or More Persons")._click(_button("OK"));
//_wait(3000);
//_popup("Configure E-mail")._setSelected(_select("recipients"), "System Administrator"); // select functions as assert in this case

//reportProgress("1.6.5.3 using query");
//_popup("Configure E-mail")._click(_button("..."));
//_wait(3000);
//_popup("FindSavedSearches")._setSelected(_select("searchesList"), "Person Display Properties");
//_popup("FindSavedSearches")._setSelected(_select("returnTypes"), ["(Person Display Properties)"]);
//_popup("FindSavedSearches")._click(_image("OK"));
//_wait(3000);
//_assertEqual(_popup("Configure E-mail")._getValue(_textbox("sQueryName")), "_identity_(Person Display Properties) in Person Display Properties");

//reportProgress("1.6.5.4 using script");
//_popup("Configure E-mail")._click(_link("Custom Recipient Script"));
//_wait(3000);
//_popup("Workflow Script Editor")._setValue(_textarea("WorkflowScript.script"), "// recipient script");
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(3000);
//_popup("Configure E-mail")._click(_link("Custom Recipient Script"));
//_wait(3000);
//_popup("Workflow Script Editor")._assertEqual("// recipient script", _textarea("WorkflowScript.script").value);
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(3000);

//reportProgress("1.6.6 Add recipients, Multi-Email Mode, CC");
//_popup("Configure E-mail")._click(_link("CC"));

//reportProgress("1.6.6.1 using Add Property");
//_popup("Configure E-mail")._click(_button("Add Property...[2]"));
//_wait(3000);
//_click(_span("owner"));
//_click(_button("OK"));
//_wait(3000);
//_popup("Configure E-mail")._setSelected(_select("ccRecipients"), "owner"); // select functions as assert in this case

//reportProgress("1.6.6.2 using Add User");
//_popup("Configure E-mail")._click(_button("Add User ...[2]"));
//_wait(3000);
//_popup("Select One or More Persons")._setValue(_textbox("_webrRSV_FilterValue_0_0"), "Administrator");
//_popup("Select One or More Persons")._click(_button("Go"));
//_wait(3000);
//_popup("Select One or More Persons")._click(_checkbox("webrRSV__SelectedItem_0"));
//_popup("Select One or More Persons")._click(_button("OK"));
//_wait(3000);
//_popup("Configure E-mail")._setSelected(_select("ccRecipients"), "System Administrator"); // select functions as assert in this case

//reportProgress("1.6.6.3 using query");
//_popup("Configure E-mail")._click(_button("...[2]"));
//_wait(3000);
//_popup("FindSavedSearches")._setSelected(_select("searchesList"), "Person Display Properties");
//_popup("FindSavedSearches")._setSelected(_select("returnTypes"), ["(Person Display Properties)"]);
//_popup("FindSavedSearches")._click(_image("OK"));
//_wait(3000);
//_assertEqual(_popup("Configure E-mail")._getValue(_textbox("sCcQueryName")), "_identity_(Person Display Properties) in Person Display Properties");

//reportProgress("1.6.6.4 using script");
//_popup("Configure E-mail")._click(_link("Custom Recipient Script[2]"));
//_wait(3000);
//_popup("Workflow Script Editor")._setValue(_textarea("WorkflowScript.script"), "// cc recipient script");
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(3000);
//_popup("Configure E-mail")._click(_link("Custom Recipient Script[2]"));
//_wait(3000);
//_popup("Workflow Script Editor")._assertEqual("// cc recipient script", _textarea("WorkflowScript.script").value);
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(3000);

//reportProgress("1.6.7 Add recipients, Multi-Email Mode, BCC");
//_popup("Configure E-mail")._click(_link("BCC"));

//reportProgress("1.6.7.1 using Add Property");
//_popup("Configure E-mail")._click(_button("Add Property...[1]"));
//_wait(3000);
//_click(_span("owner"));
//_click(_button("OK"));
//_wait(3000);
//_popup("Configure E-mail")._setSelected(_select("bccRecipients"), "owner"); // select functions as assert in this case

//reportProgress("1.6.7.2 using Add User");
//_popup("Configure E-mail")._click(_button("Add User ...[1]"));
//_wait(3000);
//_popup("Select One or More Persons")._setValue(_textbox("_webrRSV_FilterValue_0_0"), "Administrator");
//_popup("Select One or More Persons")._click(_button("Go"));
//_wait(3000);
//_popup("Select One or More Persons")._click(_checkbox("webrRSV__SelectedItem_0"));
//_popup("Select One or More Persons")._click(_button("OK"));
//_wait(3000);
//_popup("Configure E-mail")._setSelected(_select("bccRecipients"), "System Administrator"); // select functions as assert in this case

//reportProgress("1.6.7.3 using query");
//_popup("Configure E-mail")._click(_button("...[1]"));
//_wait(3000);
//_popup("FindSavedSearches")._setSelected(_select("searchesList"), "Person Display Properties");
//_popup("FindSavedSearches")._setSelected(_select("returnTypes"), ["(Person Display Properties)"]);
//_popup("FindSavedSearches")._click(_image("OK"));
//_wait(3000);
//_assertEqual(_popup("Configure E-mail")._getValue(_textbox("sBccQueryName")), "_identity_(Person Display Properties) in Person Display Properties");

//reportProgress("1.6.7.4 using script");
//_popup("Configure E-mail")._click(_link("Custom Recipient Script[1]"));
//_wait(3000);
//_popup("Workflow Script Editor")._setValue(_textarea("WorkflowScript.script"), "// bcc recipient script");
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(3000);
//_popup("Configure E-mail")._click(_link("Custom Recipient Script[1]"));
//_wait(3000);
//_popup("Workflow Script Editor")._assertEqual("// bcc recipient script", _textarea("WorkflowScript.script").value);
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(3000);

//reportProgress("1.6.7 Add recipients, Multi-Email Mode, BCC");
//_popup("Configure E-mail")._click(_link("Audit Mailbox"));
//_popup("Configure E-mail")._setValue(_textbox("EmailNotificationSetting.catchAllRecipientText"), "auditbox@test.com");

//reportProgress("persist these changes, in a way that we can confirm it was successful");
//_popup("Configure E-mail")._setValue(_textbox("EmailNotificationSetting.subject"), "Act_Test_1_6 Activity_Notification_Mod2");
//_popup("Configure E-mail")._click(_button("OK"));
//_wait(3000);
//_popup("Activity Notifications Editor")._assertExists(_link("Act_Test_1_6 Activity_Notification_Mod2"));
//_popup("Activity Notifications Editor")._click(_link("Act_Test_1_6 Activity_Notification_Mod2"));
//_wait(3000);
//_popup("Configure E-mail")._assertEqual("auditbox@test.com", _textbox("EmailNotificationSetting.catchAllRecipientText").value);

//reportProgress("1.6.9	Add from script");
//_popup("Configure E-mail")._click(_link("Custom From Address Script"));
//_wait(3000);
//_popup("Workflow Script Editor")._setValue(_textarea("WorkflowScript.script"), "// from address script");
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(3000);
//_popup("Configure E-mail")._click(_link("Custom From Address Script"));
//_wait(3000);
//_popup("Workflow Script Editor")._assertEqual("// from address script", _textarea("WorkflowScript.script").value);
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(3000);

//reportProgress("1.6.10 Add reply to script");
//_popup("Configure E-mail")._click(_link("Custom Reply To Address script"));
//_wait(3000);
//_popup("Workflow Script Editor")._setValue(_textarea("WorkflowScript.script"), "// reply address script");
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(3000);
//_popup("Configure E-mail")._click(_link("Custom Reply To Address script"));
//_wait(3000);
//_popup("Workflow Script Editor")._assertEqual("// reply address script", _textarea("WorkflowScript.script").value);
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(3000);

//reportProgress("1.6.11 Add subject script");
//_popup("Configure E-mail")._click(_link("Custom Subject Script"));
//_wait(3000);
//_popup("Workflow Script Editor")._setValue(_textarea("WorkflowScript.script"), "// subject script");
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(3000);
//_popup("Configure E-mail")._click(_link("Custom Subject Script"));
//_wait(3000);
//_popup("Workflow Script Editor")._assertEqual("// subject script", _textarea("WorkflowScript.script").value);
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(3000);

//reportProgress("1.6.12 Checkin/checkout with owner -- TEST MANUALLY");
//reportProgress("1.6.13 Can be sent both when the 2 part BGQ jobs are created and not. -- TEST MANUALLY");
//reportProgress("1.6.14 Delete");
//_popup("Configure E-mail")._click(_button("OK"));
//_wait(3000);
//_popup("Activity Notifications Editor")._click(_checkbox("EmailNotificationSetting"));
//_popup("Activity Notifications Editor")._click(_button("Delete"));
//_wait(3000);
//_popup("Activity Notifications Editor")._assertNotExists(_link("Act_Test_1_6 Activity_Notification_Mod2"));