﻿using System;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.Components;
using CCWebUIAuto.PrimitiveElements;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using ClickPortal.UI.Tests.Automation.Pages.TestStore.TestProjType;
using ClickPortal.UI.Tests.Automation.TestStore.TestProjType.ActivityPopups;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
	[TestFixture]
	class ProjectLogComponentTests : BaseTest
	{
		[Test(Description = "4.4.1 Component pages correctly")]
		public void Pages()
		{
			var workspace = new DefaultTemplate("X-00000001");
			var component = workspace.ProjectLog;

			Store.LoginAsUser(Users.Admin);
			workspace.NavigateTo();
			component.SetPageSize("3");
			Wait.Until(d => component.BtnNextPage.Enabled && component.TxtRowsPerPage.Value == "3");

			Assert.AreEqual("1", component.TxtPage.Value);
			component.BtnNextPage.Click();
			Wait.Until(d => component.BtnNextPage.Enabled && "2" == component.TxtPage.Value);
			component.BtnNextPage.Click();
			Wait.Until(d => component.BtnPrevPage.Enabled && "3" == component.TxtPage.Value);

			component.BtnPrevPage.Click();
			Wait.Until(d => component.BtnPrevPage.Enabled && "2" == component.TxtPage.Value);
			component.BtnPrevPage.Click();
			Wait.Until(d => !component.BtnPrevPage.Enabled && "1" == component.TxtPage.Value);
			component.SetPageSize("10");
		}

		[Test(Description = "4.4.2 Details page displays, 4.4.3 Next/prev in details")]
		public void ShowDetails()
		{
			var name = "TestProj_" + DataGen.String(4);
			var projCreatorPage = new ProjectCreator();
			var sfPage1 = new SmartFormPageBaseAttributes();
			
			Store.LoginAsUser(Users.Admin);
			projCreatorPage.NavigateTo();
			projCreatorPage.SelProjectType.SelectOption("TestProjType");
			projCreatorPage.BtnCreate.Click();
			sfPage1.TxtId.Value = name;
			sfPage1.TxtName.Value = name;
			sfPage1.BtnContinue.Click();

			var workspace = new DefaultTemplate(name);
			var component = workspace.ProjectLog;
			var commentPopup = new CommentPopup(name);
			var comment = DataGen.String(8);
			workspace.NavigateTo();
			workspace.LnkExecuteEasyActivityDropdown.Click();
			workspace.EasyActivitiesDropdownLinks.LnkComment.Click();
			commentPopup.SwitchTo();
			commentPopup.TxtComment.Value = comment;
			commentPopup.BtnOk.Click();
			commentPopup.SwitchBackToParent();
			Wait.Until(d => Link(By.LinkText(name)).Exists);

			var toggleStatePopup = new ToggleStatePopup(name);
			workspace.LnkExecuteEasyActivityDropdown.Click();
			workspace.EasyActivitiesDropdownLinks.LnkToggleState.Click();
			toggleStatePopup.SwitchTo();
			toggleStatePopup.BtnOk.Click();
			toggleStatePopup.SwitchBackToParent();
			Wait.Until(d => Link(By.LinkText(name)).Exists);

			var lnkCommentDetail = new Link(By.XPath(component.XpathPrefix + "//a[contains(text(), 'Comment')]"));
			var spanCommentHeader = new Container(By.XPath("//span[@class='PageHead']/span[text()='Comment']"));
			var spanToggleStateHeader = new Container(By.XPath("//span[@class='PageHead']/span[text()='Toggle State']"));
			var details = new ActivityDetails();
			lnkCommentDetail.Click();
			Wait.Until(d => spanCommentHeader.Displayed);
			details.LnkPrev.Click();
			Wait.Until(d => spanToggleStateHeader.Displayed);
			details.LnkReturnToWorkspace.Click();
			Wait.Until(d => component.DivComponentArea.Displayed);
		}

		[Test(Description="4.4.6 Activity Date is filterable and sortable (minute and day resolutions)")]
		public void ActivityDateFilterableAndSortable()
		{
			var workspace = new DefaultTemplate("X-00000001");
			var component = workspace.ProjectLog;
			const String valueA = "4/10/2013 3:49 PM";
			const String valueB = "4/10/2013 12:06 PM";

			Store.LoginAsUser(Users.Admin);
			workspace.NavigateTo();
			component.SetCriteria("Activity Date", "4/10/2013 3:49 PM");
			Wait.Until(d => component.GetNumberOfRowsDisplayed() == 1);
			component.SetCriteria("Activity Date", "4/10/2013");
			Wait.Until(d => component.GetNumberOfRowsDisplayed() == 2);
			var firstVal = component.GetValueAt(3, 0);
			Assert.IsTrue(firstVal == valueA || firstVal == valueB);
			var expectedSecondVal = firstVal == valueA ? valueB : valueA;
			Link(By.LinkText("Activity Date")).Click();
			component.WaitUntilReady();
			var secondVal = component.GetValueAt(3, 0);
			Assert.AreEqual(secondVal, expectedSecondVal);
		}

		[Test(Description="Custom columns work in polymorphic set")]
		public void CustomColumnsWorkInPolymorphicSet()
		{
			var workspace = new DefaultTemplate("X-00000001");
			var component = workspace.ProjectLog;

			Store.LoginAsUser(Users.Admin);
			workspace.NavigateTo();
			component.SetCriteria("Activity", "Has Custom Attribute");
			Wait.Until(d => new Container(By.XPath("//span[contains(text(), 'I am custom on a single type!')]")).Exists);
			Assert.IsTrue(new Container(By.XPath("//option[contains(text(), 'Activity')]")).Exists);
			Assert.IsTrue(new Container(By.XPath("//td[contains(text(), 'Custom String')]")).Exists);
			Assert.IsFalse(new Container(By.XPath("//option[contains(text(), 'Custom String')]")).Exists);
		}
	}
}
