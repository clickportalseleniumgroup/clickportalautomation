﻿using System;
using System.Collections.Generic;
using System.Linq;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.BasePages.ProjectTypeCenter;
using CCWebUIAuto.PrimitiveElements;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using ClickPortal.UI.Tests.Automation.Pages.TestStore.TestProjType;
using ClickPortal.UI.Tests.Automation.TestStore.TestProjType.ActivityPopups;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
    class ProjectTypeCenterStateTransitionsTabTests : BaseTest
    {
        private const string UseProjType = "_VerySmallProjType";

        [Test(Description = "1.7.1 State Transitions, Create")]
        public void AAA_Create() // Added AAA prefix so this test runs first
        {
            var stateTransTab = new StateTransitionsTab(UseProjType);
            Store.LoginAsUser(Users.Admin);
            stateTransTab.NavigateTo();
            stateTransTab.Create("[All]", "Open");

            // newly created states should be at the bottom of the table
            var tableRow = stateTransTab.GetRow(getLastRow: true);
            Assert.AreEqual("All", tableRow.CurrentState);
            Assert.AreEqual("Open", tableRow.NextState);
            Assert.AreEqual(StateTransitionVersionIncrementAction.None, tableRow.VersionIncrementAction);
        }

        [Test(Description = "1.7.2 State Transitions, Edit")]
        public void Edit()
        {
            var stateTransTab = new StateTransitionsTab(UseProjType);
            Store.LoginAsUser(Users.Admin);
            stateTransTab.NavigateTo();
            stateTransTab.Create("[All]", "Open");
            stateTransTab.Edit("[Initial]", "Open", getLastRow: true);

            // newly created states should be at the bottom of the table
            var tableRow = stateTransTab.GetRow(getLastRow: true);
            Assert.AreEqual("Initial", tableRow.CurrentState);
            Assert.AreEqual("Open", tableRow.NextState);
            Assert.AreEqual(StateTransitionVersionIncrementAction.None, tableRow.VersionIncrementAction);
        }

        [Test(Description = "1.7.4 State Transitions, Add activity criteria")]
        public void ActivityCriteria()
        {
            var suffix = DataGen.String(4);
            var activityNameA = "Test_1_7_4_Use_" + suffix;
            var activityNameB = "Test_1_7_4_DoNotUse_" + suffix;
            var activitiesToUse = new List<string> { activityNameA };
            var activitiesToNotUse = new List<string> { activityNameB };

            Store.LoginAsUser(Users.Admin);

            // create activities to use
            var myProjectTypeActivitiesTab = new ActivitiesTab(UseProjType);
            myProjectTypeActivitiesTab.NavigateTo();
            myProjectTypeActivitiesTab.CreateNewActivity(activityNameA);
            myProjectTypeActivitiesTab.CreateNewActivity(activityNameB);

            // create a state transition
            var stateTransTab = new StateTransitionsTab(UseProjType);
            stateTransTab.NavigateTo();
            stateTransTab.Create("[All]", "Open");

            // assign activity as criteria, but leave one unassigned
            stateTransTab.SetActivityCriteria(activitiesToUse);

            // verify that only the expected activity is selected
            Assert.IsTrue(stateTransTab.AreActivitiesSelectedAsCriteria(activitiesToUse));
            Assert.IsFalse(stateTransTab.AreActivitiesSelectedAsCriteria(activitiesToNotUse));
			Assert.IsTrue(new Link(By.LinkText(activityNameA)).Exists);
			Assert.IsFalse(new Link(By.LinkText(activityNameB)).Exists);
        }

        [Test(Description = "1.7.5 State Transitions, Add property criteria")]
        public void PropertyCriteria()
        {
            var stateTransTab = new StateTransitionsTab(UseProjType);
            Store.LoginAsUser(Users.Admin);
            stateTransTab.NavigateTo();
			stateTransTab.Create("[All]", "Open");
            stateTransTab.AddPropertyCriteria("gBoolean", "True", getLastRow: true);

			Wait.Until(d => {
				stateTransTab.NavigateTo();
				var criteria = stateTransTab.GetPropertyCriteria(getLastRow: true);
				return criteria.Contains("gBoolean = True");
			});
        }

        [Test(Description = "1.7.6 State Transitions, Add script criteria")]
        public void ScriptCriteria()
        {
            var testScript = "// " + DataGen.String(20);
            var stateTransTab = new StateTransitionsTab(UseProjType);
            Store.LoginAsUser(Users.Admin);
            stateTransTab.NavigateTo();
			stateTransTab.Create("[All]", "Open");
            stateTransTab.SetScriptCriteria(testScript, getLastRow: true);
            var readScript = stateTransTab.GetScriptCriteria(getLastRow: true);
            Assert.AreEqual(testScript, readScript);
        }

        [Test(Description = "1.7.7 State Transitions, Add pre-processing script")]
        public void PreProcessingScript()
        {
            var testScript = "// " + DataGen.String(20);
            var stateTransTab = new StateTransitionsTab(UseProjType);
            Store.LoginAsUser(Users.Admin);
            stateTransTab.NavigateTo();
			stateTransTab.Create("[All]", "Open");
            stateTransTab.SetPreProcessingScript(testScript, getLastRow: true);
            var readScript = stateTransTab.GetPreProcessingScript(getLastRow: true);
            Assert.AreEqual(testScript, readScript);
        }

        [Test(Description = "1.7.8 State Transitions, Add property criteria")]
        public void ChangeProperties()
        {
            var stateTransTab = new StateTransitionsTab(UseProjType);
            Store.LoginAsUser(Users.Admin);
            stateTransTab.NavigateTo();
			stateTransTab.Create("[All]", "Open");
            stateTransTab.AddChangeProperty("gBoolean", "True", getLastRow: true);
            var criteria = stateTransTab.GetChangeProperty(getLastRow: true);
            Assert.IsTrue(criteria.Contains("gBoolean = True"));
        }

        [Test(Description = "1.7.9 State Transitions, Add notification")]
        [Ignore]
        public void AddNotification()
        {
            var stateTransTab = new StateTransitionsTab(UseProjType);
            Store.LoginAsUser(Users.Admin);
            stateTransTab.NavigateTo();
            //TODO: Write me.
        }

        [Test(Description = "1.7.10 State Transitions, Add post-processing script")]
        public void PostProcessingScript()
        {
            var testScript = "// " + DataGen.String(20);
            var stateTransTab = new StateTransitionsTab(UseProjType);
            Store.LoginAsUser(Users.Admin);
            stateTransTab.NavigateTo();
			stateTransTab.Create("[All]", "Open");
            stateTransTab.SetPostProcessingScript(testScript, getLastRow: true);
            var readScript = stateTransTab.GetPostProcessingScript(getLastRow: true);
            Assert.AreEqual(testScript, readScript);
        }

        [Test(Description = "1.7.11 State Transitions, Add global post-script")]
        public void SetGlobalPostProcessingScript()
        {
            var stateTransTab = new StateTransitionsTab(UseProjType);
            var testScript = "// " + DataGen.String(20);
            Store.LoginAsUser(Users.Admin);
            stateTransTab.NavigateTo();
            stateTransTab.SetGlobalPostProcessingScript(testScript);
            var readScript = stateTransTab.GetGlobalPostProcessingScript();
            Assert.AreEqual(testScript, readScript);
        }

        // a slightly compound test
        // -a transition is launched on creation moving the project from initial to open
        // -an activity is used to move the project from open to closed
        // to prepare for the activity test a new project is created, implicitely testing the creation transition
        [Test(Description = "1.7.12.1 & 1.7.12.3 State Transitions, can be triggered by creation or activity")]
        public void TriggeredByCreationOrActivity()
        {
            var id = "Test_1_7_12_1_" + DataGen.String(4);
            Store.LoginAsUser(Users.Admin);
            
            var projCreatorPage = new ProjectCreator();
            projCreatorPage.NavigateTo();
            projCreatorPage.SelProjectType.SelectOption("TestProjType");
            projCreatorPage.BtnCreate.Click();

			var form = new SmartFormPageBaseAttributes();
            form.TxtId.Value = id;
            form.TxtName.Value = id;
            form.BtnContinue.Click();
            
            var workspace = new DefaultTemplate(id);
            workspace.NavigateTo();
            Assert.IsTrue(workspace.DivStatus.Text.Contains("Open")); // from initial to open tests 1.7.12.3

            workspace.LnkExecuteEasyActivityDropdown.Click();
            workspace.LnkToggleStateActivity.Click();

			var popup = new ActivityPopup(id, "Toggle State");
			popup.SwitchTo();
			popup.BtnOk.Click();
			popup.SwitchBackToParent(WaitForPopupToClose.Yes);

			workspace.NavigateTo(); // ensure workspace refreshes with the change
            Assert.IsTrue(workspace.DivStatus.Text.Contains("Closed"));
        }

        // a very compound test
        // -test that SmartForm edit can launch a transition
        // -the transition requires criteria, so do an open -> closed transition with property criteria
        // -do the same SmartForm action again, but being in a different state means a differt transition is test closed -> open
        // -use a script criteria for the second transition
        // I didn't want to clutter the SmartForm with many options for triggering a transition so I made only one, toggleState boolean,
        // since testing the second one with a new test would require moving the new project to a closed state first, it didn't seem
        // worthwhile to split them up
        [Test(Description = "1.7.12.2, 1.7.13, 1.7.14 State Transitions, can be triggered by SmartForm edit, respects property & script criteria")]
        public void TriggeredBySmartForm()
        {
            var id = "Test_1_7_12_2_" + DataGen.String(4);
            Store.LoginAsUser(Users.Admin);

            var projCreatorPage = new ProjectCreator();
            projCreatorPage.NavigateTo();
            projCreatorPage.SelProjectType.SelectOption("TestProjType");
            projCreatorPage.BtnCreate.Click();

            var form = new SmartFormPageBaseAttributes();
            form.TxtId.Value = id;
            form.TxtName.Value = id;
            form.BtnContinue.Click();
            var workspace = new DefaultTemplate(id);
            workspace.NavigateTo();
            Wait.Until(d => workspace.DivStatus.Text.Contains("Open"));

            // test transition open -> closed defined with property criteria
            workspace.LnkEditProject.Click();
            form.ChkToggleState.Click();
            form.LnkSave.Click();
			workspace.NavigateTo();
            Wait.Until(d => workspace.DivStatus.Text.Contains("Closed"));

            // since we're already here, test transition closed -> open defined with script criteria
            workspace.LnkEditProject.Click();
            form.ChkToggleState.Click();
            form.LnkSave.Click();
			workspace.NavigateTo();
            Wait.Until(d => workspace.DivStatus.Text.Contains("Open"));
        }

        [Test(Description = "1.7.15 State Transitions, delete transition")]
        public void Delete()
        {
            var stateTransTab = new StateTransitionsTab(UseProjType);
            Store.LoginAsUser(Users.Admin);
            stateTransTab.NavigateTo();
            stateTransTab.Create("[All]", "Open");
            var countA = stateTransTab.GetNumTransitions();
            stateTransTab.Delete(getLastRow: true);
            var countB = stateTransTab.GetNumTransitions();
            Assert.IsTrue(countB == countA - 1);
        }

		[Test(Description = "Verify Global Activity Post Processing Script Runs")]
		public void GlobalStateTransitionPostProcessingScriptRuns()
		{
			var errorText = "_" + DataGen.String(8); // for some reason when the error is displayed it tried to capitalize the first letter
			var stateTransitionTab = new StateTransitionsTab("_TestProjType");
			Store.LoginAsUser(Users.Admin);
			stateTransitionTab.NavigateTo();
			stateTransitionTab.SetGlobalPostProcessingScript("throw new Error(-1, '" + errorText + "');");

			const String projId = "X-00000001";
			var workspace = new DefaultTemplate(projId);
			workspace.NavigateTo();
			workspace.LnkExecuteEasyActivityDropdown.Click();
			workspace.EasyActivitiesDropdownLinks.LnkToggleState.Click();

			var popup = new ToggleStatePopup(projId);
			popup.SwitchTo();
			try {
				popup.BtnOk.Click();
				Wait.Until(d => new Container(By.XPath("//span[contains(.,'" + errorText + "')]")).Exists);
			} finally {
				popup.Close();
				popup.SwitchBackToParent();
				stateTransitionTab.NavigateTo();
				stateTransitionTab.SetGlobalPostProcessingScript("// empty");
			}
		}
    }
}
