﻿using System;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.BasePages.ProjectTypeCenter;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using NUnit.Framework;

namespace ClickPortal.UI.Tests.Automation.TestCases
{

    [TestFixture]
    public class ProjectTypeAttributes : BaseTest
    {
        public const string TestType = "_TestProjType";

        [Test(Description = "Test Project Type Add Attribute -- Boolean")]
        public void AddAttribute_Boolean()
        {
            var myProjectTypePropertiesTab = new PropertiesTab(TestType);
            
            Store.LoginAsUser(Users.Admin);
            myProjectTypePropertiesTab.NavigateTo();
            myProjectTypePropertiesTab.CreateNewCustomAttribute(
                CustomAttributeDataType.Boolean,
                "a bool",
                "abool",
                "a bool description...",
                booleanDisplay: "On/Off");
            PopUpWindow.SwitchTo("Project Type (TestProjType)");
            Assert.IsTrue(myProjectTypePropertiesTab.CustomAttributeExists("abool"));
        }


        [Test(Description = "Test Project Type Add Attribute -- Currency")]
        public void AddAttribute_Currency()
        {
            var myProjectTypePropertiesTab = new PropertiesTab(TestType);

            Store.LoginAsUser(Users.Admin);
            myProjectTypePropertiesTab.NavigateTo();
            myProjectTypePropertiesTab.CreateNewCustomAttribute(
                CustomAttributeDataType.Currency, 
                "a currency", 
                "acurrency", 
                "currency Test Description", 
                currencyType: "Chilean Peso/CL [1146] ($)");
            PopUpWindow.SwitchTo("Project Type (TestProjType)");
            Assert.IsTrue(myProjectTypePropertiesTab.CustomAttributeExists("acurrency"));

        }

        [Test(Description = "Test Project Type Add Attribute -- Date")]
        public void AddAttribute_Date()
        {
            var myProjectTypePropertiesTab = new PropertiesTab(TestType);

            Store.LoginAsUser(Users.Admin);
            myProjectTypePropertiesTab.NavigateTo();
            myProjectTypePropertiesTab.CreateNewCustomAttribute(CustomAttributeDataType.Date, "a date", "adate", "a date description...", defaultValue: "1/1/01", displayFormat: "Verbose");
            PopUpWindow.SwitchTo("Project Type (TestProjType)");
            Assert.IsTrue(myProjectTypePropertiesTab.CustomAttributeExists("adate"));
        }

        [Test(Description = "Test Project Type Add Attribute -- Document Content")]
        public void AddAttribute_DocumentContent()
        {
            var myProjectTypePropertiesTab = new PropertiesTab(TestType);

            Store.LoginAsUser(Users.Admin);
            myProjectTypePropertiesTab.NavigateTo();
            myProjectTypePropertiesTab.CreateNewCustomAttribute(CustomAttributeDataType.DocumentContent, "a doccontent", "aDocContent", "a doc description...", defaultValue: "doc test", readOnlyString: "readOnlyDocString");
            PopUpWindow.SwitchTo("Project Type (TestProjType)");
            Assert.IsTrue(myProjectTypePropertiesTab.CustomAttributeExists("aDocContent"));
        }

        [Test(Description = "Test Project Type Add Attribute -- Double")]
        public void AddAttribute_Double()
        {
            var myProjectTypePropertiesTab = new PropertiesTab(TestType);

            Store.LoginAsUser(Users.Admin);
            myProjectTypePropertiesTab.NavigateTo();
            myProjectTypePropertiesTab.CreateNewCustomAttribute(CustomAttributeDataType.Double, "a double",
                "aDouble", "a double description...", defaultValue: "1.23456789");
            PopUpWindow.SwitchTo("Project Type (TestProjType)");
            Assert.IsTrue(myProjectTypePropertiesTab.CustomAttributeExists("aDouble"));
        }

        [Test(Description = "Test Project Type Add Attribute -- Entity of Type")]
        public void AddAttribute_EntityType()
        {
            var myProjectTypePropertiesTab = new PropertiesTab(TestType);

            Store.LoginAsUser(Users.Admin);
            myProjectTypePropertiesTab.NavigateTo();
            // TODO need to put in default value, but need to select EType
            myProjectTypePropertiesTab.CreateNewCustomAttribute(CustomAttributeDataType.EntityOfType, "an entRef", "anEntRef", "an entity ref description...",
                readOnlyString: "PersonEntityType_ReadOnlyString", entityType: "Person");
            PopUpWindow.SwitchTo("Project Type (TestProjType)");
            Assert.IsTrue(myProjectTypePropertiesTab.CustomAttributeExists("anEntRef"));
        }

        [Test(Description = "Test Project Type Add Attribute -- Float")]
        public void AddAttribute_Float()
        {
            var myProjectTypePropertiesTab = new PropertiesTab(TestType);

            Store.LoginAsUser(Users.Admin);
            myProjectTypePropertiesTab.NavigateTo();
            myProjectTypePropertiesTab.CreateNewCustomAttribute(CustomAttributeDataType.Float, "a float", "afloat", "a float description...", defaultValue: "1.234567");
            PopUpWindow.SwitchTo("Project Type (TestProjType)");
            Assert.IsTrue(myProjectTypePropertiesTab.CustomAttributeExists("afloat"));
        }

        [Test(Description = "Test Project Type Add Attribute -- Integer")]
        public void AddAttribute_Integer()
        {
            var myProjectTypePropertiesTab = new PropertiesTab(TestType);

            Store.LoginAsUser(Users.Admin);
            myProjectTypePropertiesTab.NavigateTo();
            myProjectTypePropertiesTab.CreateNewCustomAttribute(CustomAttributeDataType.Integer, "an integer", "aninteger", "an integer description...", defaultValue: "1");
            PopUpWindow.SwitchTo("Project Type (TestProjType)");
            Assert.IsTrue(myProjectTypePropertiesTab.CustomAttributeExists("aninteger"));
        }

        [Test(Description = "Test Project Type Add Attribute -- SetOfType ")]
        public void AddAttribute_SetOfType()
        {
            var myProjectTypePropertiesTab = new PropertiesTab(TestType);

            Store.LoginAsUser(Users.Admin);
            myProjectTypePropertiesTab.NavigateTo();
            myProjectTypePropertiesTab.CreateNewCustomAttribute(CustomAttributeDataType.SetOfType, "a set", "aset", "a set description...", setOfType: "Project", readOnlyString: "SetTypesReadOnlyString");
            PopUpWindow.SwitchTo("Project Type (TestProjType)");
            Assert.IsTrue(myProjectTypePropertiesTab.CustomAttributeExists("aset"));
        }

        [Test(Description = "Test Project Type Add Attribute -- String  ")]
        public void AddAttribute_String()
        {
            var myProjectTypePropertiesTab = new PropertiesTab(TestType);

            Store.LoginAsUser(Users.Admin);
            myProjectTypePropertiesTab.NavigateTo();
            myProjectTypePropertiesTab.CreateNewCustomAttribute(CustomAttributeDataType.String, "a string", "astring", "a string description...", defaultValue: "stringDefaultValue");
            PopUpWindow.SwitchTo("Project Type (TestProjType)");
            Assert.IsTrue(myProjectTypePropertiesTab.CustomAttributeExists("astring"));
        }

        [Test(Description = "Test Project Type Add Attribute -- String Multiline ")]
        public void AddAttribute_multiString()
        {
            var myProjectTypePropertiesTab = new PropertiesTab(TestType);
            const string prefix = "string_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            myProjectTypePropertiesTab.NavigateTo();
            myProjectTypePropertiesTab.CreateNewCustomAttribute(CustomAttributeDataType.String,
                displayName,
                internalName,
                description,
                defaultValue: "multiStringDefaultValue", 
                multiLineString: true);
            PopUpWindow.SwitchTo("Project Type (TestProjType)");
            Assert.IsTrue(myProjectTypePropertiesTab.CustomAttributeExists(internalName));
        }

        [Test(Description = "Test Project Type Add Attribute -- enumerated String Multiline ")]
        public void AddAttribute_enumeratedStringList()
        {
            var myProjectTypePropertiesTab = new PropertiesTab(TestType);
            const string prefix = "enum_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            myProjectTypePropertiesTab.NavigateTo();
            String[] listValues =  {"a","b","c"};
            myProjectTypePropertiesTab.CreateNewCustomAttribute(
                CustomAttributeDataType.Enumeration,
                displayName,
                internalName,
                description,
                enumValues: listValues);
            PopUpWindow.SwitchTo("Project Type (TestProjType)");
            Assert.IsTrue(myProjectTypePropertiesTab.CustomAttributeExists(internalName));
        }

        [Test(Description = "Project Type -- Delete Custom Attribute ")]
        public void DeleteCustomAttribute()
        {
            var myProjectTypePropertiesTab = new PropertiesTab(TestType);

            Store.LoginAsUser(Users.Admin);
            myProjectTypePropertiesTab.NavigateTo();
            myProjectTypePropertiesTab.CreateNewCustomAttribute(CustomAttributeDataType.String, "delMe", "delMe");
            PopUpWindow.SwitchTo("Project Type (TestProjType)");
            myProjectTypePropertiesTab.DeleteCustomAttribute("delMe");
            Assert.IsFalse(myProjectTypePropertiesTab.CustomAttributeExists("delMe"));
        }
    }
}
