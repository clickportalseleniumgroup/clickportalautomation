﻿using System;
using System.Collections.Generic;
using System.Threading;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.BasePages.ProjectTypeCenter;
using CCWebUIAuto.Pages.Components;
using CCWebUIAuto.PrimitiveElements;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using ClickPortal.UI.Tests.Automation.Pages.TestStore.CdtForms;
using ClickPortal.UI.Tests.Automation.Pages.TestStore.TestProjType;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
    [TestFixture]
    public class CustomSearchComponentTests : BaseTest
    {
		[Test(Description="4.1.1 Paging")]
		public void Paging()
		{
			var page = new ProjectsCustomSearch();
			var component = page.ProjectsComponent;
			Store.LoginAsUser(Users.Admin);
			page.NavigateTo();
			component.SetPageSize("3");
			Wait.Until(d => component.BtnNextPage.Enabled && component.TxtRowsPerPage.Value == "3");

			Assert.AreEqual("1", component.TxtPage.Value);
			component.BtnNextPage.Click();
			Wait.Until(d => page.ProjectsComponent.BtnNextPage.Enabled && "2" == component.TxtPage.Value);
			component.BtnNextPage.Click();
			Wait.Until(d => page.ProjectsComponent.BtnPrevPage.Enabled && "3" == component.TxtPage.Value);

			component.BtnPrevPage.Click();
			Wait.Until(d => page.ProjectsComponent.BtnPrevPage.Enabled && "2" == component.TxtPage.Value);
			component.BtnPrevPage.Click();
			Wait.Until(d => !page.ProjectsComponent.BtnPrevPage.Enabled && "1" == component.TxtPage.Value);
			component.SetPageSize("10");
		}

		[Test(Description="4.1.2 Filters 'primitives'")]
		public void FiltersPrimitives()
		{
			var id = "Test_4_1_2_" + DataGen.String(4);
			var integerValue = DataGen.NumberString(6);
			var currencyValue = "$" + DataGen.NumberString(3);
			var floatValue = DataGen.NumberString(4) + "." + DataGen.NumberString(2);
			var doubleValue = DataGen.NumberString(4) + "." + DataGen.NumberString(2);

			Store.LoginAsUser(Users.Admin);
			var projCreatorPage = new ProjectCreator();
			projCreatorPage.NavigateTo();
			projCreatorPage.SelProjectType.SelectOption("TestProjType");
			projCreatorPage.BtnCreate.Click();
			var sfPage1 = new SmartFormPageBaseAttributes();
			sfPage1.TxtId.Value = id;
			sfPage1.TxtName.Value = id;
			sfPage1.BtnContinue.Click();
			var sfPage2 = new SmartFormPageTestDeAttributes();
			sfPage2.TxtInteger.Value = integerValue;
			sfPage2.TxtCurrency.Value = currencyValue;
			sfPage2.TxtFloat.Value = floatValue;
			sfPage2.TxtDouble.Value = doubleValue;
			sfPage2.BtnContinue.Click();

			var page = new ProjectsCustomSearch();
			var component = page.ProjectsComponent;
			page.NavigateTo();
			component.SetCriteria("Name", id);
			Wait.Until(d => 1 == component.GetNumberOfRowsDisplayed());
			component.BtnClear.Click();
			Wait.Until(d => !component.BtnClear.Enabled && 1 != component.GetNumberOfRowsDisplayed());

			component.SetCriteria("Integer", integerValue);
			Wait.Until(d => 1 == component.GetNumberOfRowsDisplayed());
			component.BtnClear.Click();
			Wait.Until(d => !component.BtnClear.Enabled && 1 != component.GetNumberOfRowsDisplayed());

			component.SetCriteria("Currency", currencyValue);
			Wait.Until(d => 1 == component.GetNumberOfRowsDisplayed());
			component.BtnClear.Click();
			Wait.Until(d => !component.BtnClear.Enabled && 1 != component.GetNumberOfRowsDisplayed());

			component.SetCriteria("Float", floatValue);
			Wait.Until(d => 1 == component.GetNumberOfRowsDisplayed());
			component.BtnClear.Click();
			Wait.Until(d => !component.BtnClear.Enabled && 1 !=  component.GetNumberOfRowsDisplayed());

			component.SetCriteria("Double", doubleValue);
			Wait.Until(d => 1 == component.GetNumberOfRowsDisplayed());
			component.BtnClear.Click();
			Wait.Until(d => !component.BtnClear.Enabled && 1 != component.GetNumberOfRowsDisplayed());

			component.SetCriteria("Boolean", "no");
			Wait.Until(d => "no" == component.GetValueAt(2, 0));
			component.BtnClear.Click();
			Wait.Until(d => !component.BtnClear.Enabled);

			component.SetCriteria("Boolean", "false");
			Wait.Until(d => "no" == component.GetValueAt(2, 0));
			component.BtnClear.Click();
			Wait.Until(d => !component.BtnClear.Enabled);

			component.SetCriteria("Boolean", "off");
			Wait.Until(d => "no" == component.GetValueAt(2, 0));
			component.BtnClear.Click();
			Wait.Until(d => !component.BtnClear.Enabled);

			component.SetCriteria("Boolean", "yes");
			Wait.Until(d => "yes" == component.GetValueAt(2, 0));
			component.BtnClear.Click();
			Wait.Until(d => !component.BtnClear.Enabled);

			component.SetCriteria("Boolean", "true");
			Wait.Until(d => "yes" == component.GetValueAt(2, 0));
			component.BtnClear.Click();
			Wait.Until(d => !component.BtnClear.Enabled);

			component.SetCriteria("Boolean", "on");
			Wait.Until(d => "yes" == component.GetValueAt(2, 0));
			component.BtnClear.Click();
			Wait.Until(d => !component.BtnClear.Enabled);
		}

		[Test(Description="4.1.3 Date attributes can be filtered with day or minute resolution")]
		public void DatesFilterToDayOrMinute()
		{
			var random = new Random();
			var month = random.Next(2, 8);
			var day = random.Next(2, 25);
			var hourA = random.Next(2, 9);
			var hourB = random.Next(2, 9);
			var minuteA = random.Next(31, 55);
			var minuteB = random.Next(2, 30);
			var dayString = month + "/" + day + "/2010 ";
			var dateTimeA = dayString + hourA + ":" + minuteA + " AM";
			var dateTimeB = dayString + hourB + ":" + minuteB + " PM";

			var idA = "Test_4_1_3_" + DataGen.String(4);
			var idB = "Test_4_1_3_" + DataGen.String(4);

			Store.LoginAsUser(Users.Admin);

			var projCreatorPage = new ProjectCreator();
			projCreatorPage.NavigateTo();
			projCreatorPage.SelProjectType.SelectOption("TestProjType");
			projCreatorPage.BtnCreate.Click();
			var sfPage1 = new SmartFormPageBaseAttributes();
			sfPage1.TxtId.Value = idA;
			sfPage1.TxtName.Value = idA;
			sfPage1.BtnContinue.Click();
			var sfPage2 = new SmartFormPageTestDeAttributes();
			sfPage2.TxtDate.Value = dateTimeA;
			sfPage2.BtnContinue.Click();

			projCreatorPage.NavigateTo(); // the first time we return to this page it displays a "success" message
			projCreatorPage.NavigateTo(); // so hit it a second time to get the project creator to render normally
			projCreatorPage.SelProjectType.SelectOption("TestProjType");
			projCreatorPage.BtnCreate.Click();
			sfPage1.TxtId.Value = idB;
			sfPage1.TxtName.Value = idB;
			sfPage1.BtnContinue.Click();
			sfPage2.TxtDate.Value = dateTimeB;
			sfPage2.BtnContinue.Click();

			var page = new ProjectsCustomSearch();
			var component = page.ProjectsComponent;
			page.NavigateTo();
			component.SetCriteria("Date", dayString);
			Assert.AreEqual(2, component.GetNumberOfRowsDisplayed());
			component.SetCriteria("Date", dateTimeA);
			Assert.AreEqual(1, component.GetNumberOfRowsDisplayed());
			component.SetCriteria("Date", dateTimeB);
			Assert.AreEqual(1, component.GetNumberOfRowsDisplayed());
		}

		[Test(Description="4.1.4 Links are correct")]
		public void LinksAreCorrect()
		{
			var id = "Test_4_1_4_" + DataGen.String(4);

			Store.LoginAsUser(Users.Admin);
			var projCreatorPage = new ProjectCreator();
			projCreatorPage.NavigateTo();
			projCreatorPage.SelProjectType.SelectOption("TestProjType");
			projCreatorPage.BtnCreate.Click();
			var sfPage1 = new SmartFormPageBaseAttributes();
			sfPage1.TxtId.Value = id;
			sfPage1.TxtName.Value = id;
			sfPage1.BtnContinue.Click();

			var page = new ProjectsCustomSearch();
			var component = page.ProjectsComponent;
			page.NavigateTo();

			// filter the component so we know the expected link is displayed
			component.SetCriteria("ID", id);
			var link = new Link(By.LinkText(id));
			link.Click();

			// verify that this is the workspace
			var idField = new Container(By.XPath("//tr[@id='__TestProjType.ID_container']/td[3]/span[text()='" + id + "']"));
			Assert.IsTrue(idField.Exists);
		}

		[Test(Description="4.1.7 Properties, Display Name")]
		public void ChangeName()
		{
			var name = "Test 417 " + DataGen.String(4);
			const String resetName = "Custom Search";

			Store.LoginAsUser(Users.Admin);
			var page = new ProjectsCustomSearch();
			var component = page.ProjectsComponent;
			page.NavigateTo();

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(
				properties => { properties.TxtTitle.Value = name; });
			Assert.IsTrue(new Container(By.XPath("//div[@data-display-name= '" + name + "']")).Exists);
			var tempComponent = new CustomSearchComponent(name);
			tempComponent.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(
				properties => { properties.TxtTitle.Value = resetName; });
			Assert.IsTrue(new Container(By.XPath("//div[@data-display-name= '" + resetName + "']")).Exists);
		}

		[Test(Description = "4.1.7 Properties, Display Single Search")]
		public void ChangeSearch()
		{
			const String searchToUse = "Person Display Properties";
			const String searchToResetTo = "Test - _TestProjType Custom Attribute Primitives";

			Store.LoginAsUser(Users.Admin);
			var page = new ProjectsCustomSearch();
			var component = page.ProjectsComponent;
			page.NavigateTo();

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>( properties => {
				var popup = (CustomSearchComponentPropertiesPopup) properties;
				popup.RdoSingleSearch.Click();
				popup.BtnSelectSingleSearch.Click();
				var singleChooser = new ChooserPopup("Custom Search");
				singleChooser.SwitchTo();
				singleChooser.SelectValue(searchToUse, "Name");
				singleChooser.BtnOk.Click();
				singleChooser.SwitchBackToParent(WaitForPopupToClose.Yes);
				Wait.Until(d => popup.TxtSingleSearchName.Value == searchToUse);
			});
			Wait.Until(d => Link(By.LinkText("First Name")).Exists);
			Assert.IsTrue(Link(By.LinkText("Last Name")).Exists);
			Assert.IsTrue(Link(By.LinkText("Employer")).Exists);
			Assert.IsTrue(Link(By.LinkText("Title")).Exists);

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>( properties => {
				var popup = (CustomSearchComponentPropertiesPopup) properties;
				popup.RdoSingleSearch.Click();
				popup.BtnSelectSingleSearch.Click();
				var singleChooser = new ChooserPopup("Custom Search");
				singleChooser.SwitchTo();
				singleChooser.SelectValue(searchToResetTo, "Name");
				singleChooser.BtnOk.Click();
				singleChooser.SwitchBackToParent(WaitForPopupToClose.Yes);
				Wait.Until(d => popup.TxtSingleSearchName.Value == searchToResetTo);
			});

			Wait.Until(d => Link(By.LinkText("ID")).Exists);
			Assert.IsTrue(Link(By.LinkText("Name")).Exists);
			Assert.IsTrue(Link(By.LinkText("Boolean")).Exists);
			Assert.IsTrue(Link(By.LinkText("String")).Exists);

		}

		[Test(Description = "4.1.7 Properties, Display List of Searches")]
		public void ListOfSearches()
		{
			const String searchToUse = "Person Display Properties";
			const String searchToResetTo = "Test - _TestProjType Custom Attribute Primitives";

			Store.LoginAsUser(Users.Admin);
			var page = new ProjectsCustomSearch();
			var component = page.ProjectsComponent;
			page.NavigateTo();

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>( properties => {
				var popup = (CustomSearchComponentPropertiesPopup) properties;
				popup.RdoListOfSearches.Click();
				popup.BtnAddSearchToList.Click();
				var listChooser = new ChooserPopup("Custom Searches", true);
				listChooser.SwitchTo();
				listChooser.SelectValue(searchToUse, "Name");
				listChooser.BtnOk.Click();
				listChooser.SwitchBackToParent(WaitForPopupToClose.Yes);
				Wait.Until(d => popup.SelCustomSearchList.Contains(searchToUse));
			});
			Wait.Until(d => Link(By.LinkText(searchToUse)).Exists);

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				var popup = (CustomSearchComponentPropertiesPopup) properties;
				popup.RdoSingleSearch.Click();
				popup.BtnSelectSingleSearch.Click();
				var singleChooser = new ChooserPopup("Custom Search");
				singleChooser.SwitchTo();
				singleChooser.SelectValue(searchToResetTo, "Name");
				singleChooser.BtnOk.Click();
				singleChooser.SwitchBackToParent(WaitForPopupToClose.Yes);
				Wait.Until(d => popup.TxtSingleSearchName.Value == searchToResetTo);
			});
			Wait.Until(d => Link(By.LinkText("ID")).Exists);
			Assert.IsTrue(Link(By.LinkText("Name")).Exists);
			Assert.IsTrue(Link(By.LinkText("Boolean")).Exists);
			Assert.IsTrue(Link(By.LinkText("String")).Exists);
		}

		[Test(Description = "4.1.7 Properties, Display List of Searches By Tag")]
		public void ListOfSearchesByTag()
		{
			const String searchToResetTo = "Test - _TestProjType Custom Attribute Primitives";

			Store.LoginAsUser(Users.Admin);
			var page = new ProjectsCustomSearch();
			var component = page.ProjectsComponent;
			page.NavigateTo();

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				var popup = (CustomSearchComponentPropertiesPopup) properties;
				popup.RdoSearchWithTag.Click();
				popup.SelTag.SelectOption("Display Fields Filter");
			});
			Wait.Until(d => Link(By.LinkText("Company/Organization Display Properties")).Exists);
			Assert.IsTrue(Link(By.LinkText("Document Display Properties")).Exists);
			Assert.IsTrue(Link(By.LinkText("Open Projects By Company")).Exists);
			Assert.IsTrue(Link(By.LinkText("Organization Managers")).Exists);

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				var popup = (CustomSearchComponentPropertiesPopup) properties;
				popup.RdoSingleSearch.Click();
				popup.BtnSelectSingleSearch.Click();
				var singleChooser = new ChooserPopup("Custom Search");
				singleChooser.SwitchTo();
				singleChooser.SelectValue(searchToResetTo, "Name");
				singleChooser.BtnOk.Click();
				singleChooser.SwitchBackToParent(WaitForPopupToClose.Yes);
				Wait.Until(d => popup.TxtSingleSearchName.Value == searchToResetTo);
			});
			Wait.Until(d => Link(By.LinkText("ID")).Exists);
			Assert.IsTrue(Link(By.LinkText("Name")).Exists);
			Assert.IsTrue(Link(By.LinkText("Boolean")).Exists);
			Assert.IsTrue(Link(By.LinkText("String")).Exists);
		}

		[Test(Description = "4.1.7 Show export button")]
		public void ShowExportButton()
		{
			Store.LoginAsUser(Users.Admin);
			var page = new ProjectsCustomSearch();
			var component = page.ProjectsComponent;
			page.NavigateTo();

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).ChkShowExportButton.Checked = false; 
			});
			Wait.Until(d => !component.BtnExport.Exists);
			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).ChkShowExportButton.Checked = true; 
			});
			Wait.Until(d => component.BtnExport.Exists);
		}

		[Test(Description = "4.1.7 Show email button")]
		public void ShowEmailButton()
		{
			Store.LoginAsUser(Users.Admin);
			var page = new ProjectsCustomSearch();
			var component = page.ProjectsComponent;
			page.NavigateTo();

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).ChkShowEmailButton.Checked = true;
			});
			Wait.Until(d => component.BtnEmail.Exists);

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).ChkShowEmailButton.Checked = false; 
			});
			Wait.Until(d => !component.BtnEmail.Exists);
		}

		[Test(Description = "4.1.7 Show filter bar")]
		public void ShowFilterBar()
		{
			Store.LoginAsUser(Users.Admin);
			var page = new ProjectsCustomSearch();
			page.NavigateTo();
			var component = page.ProjectsComponent;
			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).ChkShowFilterBar.Checked = false; 
			});
			Wait.Until(d => !component.BtnGo.Displayed);
			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).ChkShowFilterBar.Checked = true; 
			});
			Wait.Until(d => component.BtnGo.Displayed);
		}

		[Test(Description = "4.1.7 Show details link")]
		public void ShowDetailsLink()
		{
			Store.LoginAsUser(Users.Admin);
			var page = new ProjectsCustomSearch();
			page.NavigateTo();
			var component = page.ProjectsComponent;
			var link = new Image(By.XPath("//img[@title='Details']"));
			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).ChkShowDetailsLink.Checked = false;
			});
			Wait.Until(d => !link.Exists);
			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).ChkShowDetailsLink.Checked = true;
			});
			Wait.Until(d => link.Exists);
		}

		[Test(Description = "4.1.7 Show ID as link")]
		public void ShowIdAsLink()
		{
			const String studyId = "X-00000001";
			Store.LoginAsUser(Users.Admin);
			var page = new ProjectsCustomSearch();
			page.NavigateTo();
			var component = page.ProjectsComponent;
			var link = new Link(By.LinkText(studyId));
			var span = new Container(By.XPath("//span[text()='" + studyId + "']"));
			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).ChkShowInlineDetailsLink.Checked = false;
			});
			component.SetCriteria("ID", studyId);
			component.BtnGo.Click();
			Assert.IsFalse(link.Exists);
			Assert.IsTrue(span.Exists);
			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).ChkShowInlineDetailsLink.Checked = true;
			});
			component.SetCriteria("ID", studyId);
			component.BtnGo.Click();
			Assert.IsTrue(link.Exists);
			Assert.IsFalse(span.Exists);
		}

		[Test(Description = "4.1.7 Show paging bar")]
		public void ShowPagingBar()
		{
			Store.LoginAsUser(Users.Admin);
			var page = new ProjectsCustomSearch();
			page.NavigateTo();
			var component = page.ProjectsComponent;
			Wait.Until(d => component.TxtRowsPerPage.Enabled);
			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				var popup = (CustomSearchComponentPropertiesPopup) properties;
				popup.ChkAlwaysShowPagingBar.Checked = false;
				popup.TxtResultsPerPage.Value = "100";
			});
			Wait.Until(d => component.BtnGo.Enabled && !component.BtnNextPage.Displayed);
			Assert.IsFalse(component.BtnPrevPage.Displayed);
			Assert.IsFalse(component.TxtPage.Displayed);
			Assert.IsFalse(component.TxtRowsPerPage.Displayed);
			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				var popup = (CustomSearchComponentPropertiesPopup) properties;
				popup.ChkAlwaysShowPagingBar.Checked = true;
				popup.TxtResultsPerPage.Value = "10";
			});
			Wait.Until(d => component.BtnGo.Enabled && component.BtnNextPage.Displayed);
			Assert.IsTrue(component.TxtPage.Displayed);
			Assert.IsTrue(component.TxtRowsPerPage.Displayed);
		}

		[Test(Description = "4.1.7 Search Mode")]
		public void SearchMode()
		{
			const String studyId = "X-00000001";
			Store.LoginAsUser(Users.Admin);
			var page = new ProjectsCustomSearch();
			page.NavigateTo();
			var component = page.ProjectsComponent;
			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				var popup = (CustomSearchComponentPropertiesPopup) properties;
				popup.ChkSearchMode.Checked = true;
				popup.ChkShowInlineDetailsLink.Checked = true;
			});
			component.Clear();
			Wait.Until(d => component.BtnGo.Enabled);
			Assert.AreEqual(0, component.GetNumberOfRowsDisplayed());
			component.SetCriteria("ID", studyId);
			component.BtnGo.Click();
			Wait.Until(d => component.BtnGo.Enabled);
			Assert.AreEqual(1, component.GetNumberOfRowsDisplayed());
			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).ChkSearchMode.Checked = false;
			});
		}

		[Test(Description = "4.1.7 Show filter bar")]
		public void ShowImage()
		{
			Store.LoginAsUser(Users.Admin);
			var page = new ProjectsCustomSearch();
			page.NavigateTo();
			var component = page.ProjectsComponent;
			var image = new Image(By.XPath("//img[contains(@src, 'project_type_default.gif')]"));
			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).ChkShowImage.Checked = true;
			});
			Assert.IsTrue(image.Exists);
			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).ChkShowImage.Checked = false;
			});
			Assert.IsFalse(image.Exists);
		}

		[Test(Description = "4.1.7 Standard paging mode")]
		public void StandardPagingMode()
		{
			Store.LoginAsUser(Users.Admin);
			var page = new ProjectsCustomSearch();
			page.NavigateTo();
			var component = page.ProjectsComponent;
			
			// component should start in standard paging mode, but set it just to be sure
			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => 
				((CustomSearchComponentPropertiesPopup) properties).SelPagingMode.SelectOption("standard")
			);
			Assert.IsTrue(component.BtnNextPage.Displayed);
			Assert.IsTrue(component.BtnPrevPage.Displayed);
			Assert.IsTrue(component.TxtPage.Displayed);
			Assert.IsTrue(component.TxtRowsPerPage.Displayed);
		}

		[Test(Description = "4.1.7 Optimized paging mode, behaves the same as standard because of the small amount of data")]
		public void OptimizedPagingMode()
		{
			Store.LoginAsUser(Users.Admin);
			var page = new ProjectsCustomSearch();
			page.NavigateTo();
			var component = page.ProjectsComponent;

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties =>
				((CustomSearchComponentPropertiesPopup) properties).SelPagingMode.SelectOption("optimized")
			);
			Assert.IsTrue(component.BtnNextPage.Displayed);
			Assert.IsTrue(component.BtnPrevPage.Displayed);
			Assert.IsTrue(component.TxtPage.Displayed);
			Assert.IsTrue(component.TxtRowsPerPage.Displayed);
			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties =>
				((CustomSearchComponentPropertiesPopup) properties).SelPagingMode.SelectOption("standard")
			);
		}

		[Test(Description = "4.1.7 Minimal paging mode")]
		public void MinimalPagingMode()
		{
			Store.LoginAsUser(Users.Admin);
			var page = new ProjectsCustomSearch();
			page.NavigateTo();
			var component = page.ProjectsComponent;

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties =>
				((CustomSearchComponentPropertiesPopup) properties).SelPagingMode.SelectOption("minimal")
			);
			Assert.IsTrue(component.BtnNextPage.Displayed);
			Assert.IsTrue(component.BtnPrevPage.Displayed);
			Assert.IsFalse(component.TxtPage.Displayed);
			Assert.IsFalse(component.TxtRowsPerPage.Displayed);
			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties =>
				((CustomSearchComponentPropertiesPopup) properties).SelPagingMode.SelectOption("standard")
			);
		}

		[Test(Description = "4.1.7 Results per page")]
		public void ResultsPerPage()
		{
			Store.LoginAsUser(Users.Admin);
			var page = new ProjectsCustomSearch();
			page.NavigateTo();
			var component = page.ProjectsComponent;

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).TxtResultsPerPage.Value = "20";
			});
			Assert.AreEqual(20, component.GetNumberOfRowsDisplayed());
			component.TxtRowsPerPage.Value = "5";
			component.BtnGo.Click();
			Assert.AreEqual(5, component.GetNumberOfRowsDisplayed());

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).TxtResultsPerPage.Value = "10";
			});
		}

		[Test(Description = "4.1.7 Display 'Change Search Parameters'")]
		public void ChangeSearchParametersButton()
		{
			const String searchA = "Person Display Properties";
			const String searchB = "Parameter Testing";

			Store.LoginAsUser(Users.Admin);
			var page = new CustomSearchTesting();
			page.NavigateTo();

			var linkA = new Link(By.LinkText(searchA));
			var linkB = new Link(By.LinkText(searchB));
			
			linkA.Click();
			var results = new CustomSearchResultsPopup();
			results.SwitchTo();
			Assert.IsFalse(results.BtnChangeParameters.Displayed);
			results.Close();
			results.SwitchBackToParent();

			linkB.Click();
			results.SwitchTo();
			Assert.IsTrue(results.BtnChangeParameters.Displayed);
			results.Close();
			results.SwitchBackToParent();
		}

		[Test(Description = "4.1.7 List Mode Run Button")]
		public void ListModeRunButton()
		{
			Store.LoginAsUser(Users.Admin);
			var page = new CustomSearchTesting();
			page.NavigateTo();
			var component = page.ListOfSearches;
			var imgRun = new Image(By.XPath("//img[contains(@src,'run_tx.gif')]"));

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).ChkListOptionsRunButton.Checked = false;
			});
			Assert.IsFalse(imgRun.Exists);

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).ChkListOptionsRunButton.Checked = true;
			});
			Assert.IsTrue(imgRun.Exists);
		}

		[Test(Description = "4.1.7 List Mode Result Type")]
		public void ListModeResultType()
		{
			Store.LoginAsUser(Users.Admin);
			var page = new CustomSearchTesting();
			page.NavigateTo();
			var component = page.ListOfSearches;
			var lnkResultType = new Link(By.LinkText("Result Type"));

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).ChkListOptionsResultType.Checked = false;
			});
			Assert.IsFalse(lnkResultType.Exists);

			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties => {
				((CustomSearchComponentPropertiesPopup) properties).ChkListOptionsResultType.Checked = true;
			});
			Assert.IsTrue(lnkResultType.Exists);
		}

		[Test(Description = "4.1.8 Contextual Custom Search Functions")]
		public void ContextualCustomSearchFunctions()
		{
			var workspace = new DefaultTemplate("X-00000001");
			Store.LoginAsUser(Users.Admin);
			workspace.NavigateTo();
			workspace.LnkContextualCustomSearchTab.Click();
			Wait.Until(d => workspace.ContextualCustomSearch.DivComponentArea.Exists);
			Wait.Until(d => Link(By.LinkText("Another Clone")).Exists);
			Assert.IsTrue(Link(By.LinkText("Example Project")).Exists);
			Assert.IsTrue(Link(By.LinkText("Yet Another Clone")).Exists);
		}

		[Test(Description = "4.1.8 Contextual Custom Search Can Be Created")]
		public void ContextualCustomSearchCreation()
		{
			var templatesPage = new TemplatesTab("_VerySmallProjType");
			Store.LoginAsUser(Users.Admin);
			templatesPage.NavigateTo();
			templatesPage.OpenTemplateComponentsPopup("Default");
			var templatePopup = new EditPageLayoutPopup();
			templatePopup.SwitchTo();

			// create a custom search component 
			var selComponent = new Select(By.Name("Column0"));
			selComponent.SelectOption("Custom Search");
			Button(By.XPath("//select[@name='Column0']/../../TD[2]/a/img")).Click();

			// open the custom search component properties
			var btnEdit = new Button(By.XPath("//a[text()='Custom Search']/../../td[2]/a"));
			btnEdit.Click();
			Link(By.XPath("//td[text()='Properties...']")).Click();
			var properties = new CustomSearchComponentPropertiesPopup();
			properties.SwitchTo();

			// open the new custom search editor
			properties.BtnNewSingleSearch.Click();
			var searchEditor = new SavedSearchEditorPopup();
			searchEditor.SwitchTo();
			// actually just see if the attribute button is displayed, which indicates the
			// editor is in context mode
			Assert.IsTrue(searchEditor.BtnAttributeCriterion.Displayed);
			
			// close some popups
			searchEditor.Close();
			searchEditor.SwitchBackToParent();
			properties.Close();

			// remove the component
			properties.SwitchBackToParent();
			btnEdit.Click();
			Link(By.LinkText("Delete")).Click();
			Wait.Until(d => new Link(By.LinkText("Custom Search")).Exists == false);
			templatePopup.Close();
		}

		/// <summary>
		/// This is a re-usable method to verify that fields that are expected to be filterable
		/// and sortable behave as expected.  This is tightly coupled to _TestProjType and it's
		/// SmartForm from the test store.
		/// </summary>
		/// <param name="TestFieldRef">Name of a field exposed on the second SmartForm page object, like 'TxtInteger'</param>
		/// <param name="DataField">Name of the same field as exposed on the custom search component, like 'Integer'</param>
		/// <param name="LowValue">A string that when sorted by the DRSV will have a value lower than the others</param>
		/// <param name="MidValue">A string that when sorted is between the low and high values</param>
		/// <param name="HighValue">A string that when sorted by the DRSV will have a value higher than the others</param>
		/// <param name="columnIdx">Starting at 0 on the left, the index of the column the values will be found in</param>
		public static void TestFilterAndSortAttribute(String TestFieldRef, String DataField, String LowValue, String MidValue, String HighValue, UInt16 columnIdx) {
			const String nameField = "Name";
			var suffix = DataGen.String(4);
			var dataA = new Dictionary<String, String> {
				{nameField, "AAA_" + suffix},
				{DataField, LowValue}
			};
			var dataB = new Dictionary<String, String> {
				{nameField, "BBB_" + suffix},
				{DataField, MidValue}
			};
			var dataC = new Dictionary<String, String> {
				{nameField, "CCC_" + suffix},
				{DataField, HighValue}
			};
			var allData = new [] { dataA, dataB, dataC };

			Store.LoginAsUser(Users.Admin);
			var projCreatorPage = new ProjectCreator();
			foreach (var data in allData) {
				projCreatorPage.NavigateTo();
				Thread.Sleep(1000);
				projCreatorPage.NavigateTo(); // clear created successfully message
				Wait.Until(d => projCreatorPage.SelProjectType.Contains("TestProjType"));
				projCreatorPage.SelProjectType.SelectOption("TestProjType");
				projCreatorPage.BtnCreate.Click();
				var sfPage1 = new SmartFormPageBaseAttributes();
				sfPage1.TxtName.Value = data[nameField];
				sfPage1.BtnContinue.Click();
				var sfPage2 = new SmartFormPageTestDeAttributes();
				((TextBox) sfPage2.GetType().GetField(TestFieldRef).GetValue(sfPage2)).Value = data[DataField];
				sfPage2.BtnContinue.Click();
			}

			// When we click the column header, we don't know which way it will sort it because
			// that depends on the previous state of the component.  So we sort, then see which is
			// at the top.  We expect the next sort to reverse that order.
			var page = new ProjectsCustomSearch();
			page.NavigateTo();
			var component = page.ProjectsComponent;
			component.SetCriteria("Name", "%" + suffix);
			component.SortColumn(1);
			var top = component.GetValueAt(columnIdx, 0);
			var nextTop = top == allData[0][DataField] ? allData[2][DataField] : allData[0][DataField];
			component.SortColumn(1);
			Assert.AreEqual(nextTop, component.GetValueAt(columnIdx, 0));
			component.Clear();

			// verify that we can filter to each item
			component.LnkAdvanced.Click();
			component.SetCriteria(nameField, "%" + suffix);
			component.SetCriteria(DataField, LowValue, 1);
			Wait.Until(d => LowValue == component.GetValueAt(columnIdx, 0));
			component.SetCriteria(DataField, MidValue, 1);
			Wait.Until(d => MidValue == component.GetValueAt(columnIdx, 0));
			component.SetCriteria(DataField, HighValue, 1);
			Wait.Until(d => HighValue == component.GetValueAt(columnIdx, 0));
			component.Clear();
		}


		[Test(Description = "4.1.9 String are filterable, sortable")]
		public void StringsFilterAndSort()
		{
			TestFilterAndSortAttribute("TxtString", "String", "Ape", "Bat", "Cat", 3);
		}

		[Test(Description = "4.1.10 Integers are filterable, sortable")]
		public void IntegerFilterAndSort()
		{
			TestFilterAndSortAttribute("TxtInteger", "Integer", "-1", "0", "1", 4);
		}

		[Test(Description = "4.1.11 Floats are filterable, sortable")]
		public void FloatFilterAndSort()
		{
			TestFilterAndSortAttribute("TxtFloat", "Float", "-1", "0", "1", 6);
		}

		[Test(Description = "4.1.12 Doubles are filterable, sortable")]
		public void DoubleFilterAndSort()
		{
			TestFilterAndSortAttribute("TxtDouble", "Double", "-1", "0", "1", 7);
		}

		[Test(Description = "4.1.13 Currency are filterable, sortable")]
		public void CurrencyFilterAndSort()
		{
			TestFilterAndSortAttribute("TxtCurrency", "Currency", "$0.00", "$1.00", "$2.00", 5);
		}

		[Test(Description = "4.1.15 Currency are filterable, sortable")]
		public void DateFilterAndSort()
		{
			TestFilterAndSortAttribute("TxtDate", "Date", "2/23/2012 12:00 AM", "2/24/2012 12:00 AM", "2/25/2012 12:00 AM", 8);
		}

		[Test(Description = "4.1.16 Entity references are NOT filterable, NOT sortable")]
		public void EntityReferencesDontFilterOrSort()
		{
			var testPage = new CustomSearchTesting();
			Store.LoginAsUser(Users.Admin);
			testPage.NavigateTo();
			var prefix = testPage.EntitiesAndExportableMethods.XpathPrefix;
			var lnkNameSort = new Link(By.XPath(prefix + "//a[contains(text(),'Name')]"));
			var lnkPersonSort = new Link(By.XPath(prefix + "//a[contains(text(),'Person')]"));
			var selFilter = new Select(By.XPath(prefix + "//select[contains(@id,'_queryField1')]"));
			Wait.Until(d => lnkNameSort.Exists);
			Assert.False(lnkPersonSort.Exists);
			Assert.False(selFilter.Contains("Person"));
		}

		[Test(Description = "4.1.17 Entity references are NOT filterable, NOT sortable")]
		public void EntitySetReferencesDontFilterOrSort()
		{
			var testPage = new CustomSearchTesting();
			Store.LoginAsUser(Users.Admin);
			testPage.NavigateTo();
			var prefix = testPage.Sets.XpathPrefix;
			var lnkNameSort = new Link(By.XPath(prefix + "//a[contains(text(),'Name')]"));
			var lnkPersonSort = new Link(By.XPath(prefix + "//a[contains(text(),'Person')]"));
			var selFilter = new Select(By.XPath(prefix + "//select[contains(@id,'_queryField1')]"));
			Wait.Until(d => lnkNameSort.Exists);
			Assert.False(lnkPersonSort.Exists);
			Assert.False(selFilter.Contains("Person"));
			Assert.True(selFilter.Contains("Set of String"));
		}

		[Test(Description = "4.1.18 Entity references are NOT filterable, NOT sortable")]
		public void MethodsDontFilterOrSort()
		{
			var testPage = new CustomSearchTesting();
			Store.LoginAsUser(Users.Admin);
			testPage.NavigateTo();
			var prefix = testPage.UserInterfaceMethods.XpathPrefix;
			var lnkNameSort = new Link(By.XPath(prefix + "//a[contains(text(),'Name')]"));
			var iconSort = new Link(By.XPath(prefix + "//a[contains(text(),'getSmartFormIconLink()')]"));
			var selFilter = new Select(By.XPath(prefix + "//select[contains(@id,'_queryField1')]"));
			Wait.Until(d => lnkNameSort.Exists);
			Assert.False(iconSort.Exists);
			Assert.False(selFilter.Contains("getSmartFormIconLink()"));
		}

		[Test(Description= "Test filtering on set attribute")]
		public void FilterOnSetAttribute()
		{
			var name = "TestProj_" + DataGen.String(4);
			var data = new List<String> { "test_" + DataGen.String(4), "test_" + DataGen.String(4) };

			var projCreatorPage = new ProjectCreator();
			var sfPage1 = new SmartFormPageBaseAttributes();
			var sfPage2 = new SmartFormPageTestDeAttributes();
			var testPage = new CustomSearchTesting();

			Store.LoginAsUser(Users.Admin);
			
			projCreatorPage.NavigateTo();
			projCreatorPage.NavigateTo(); // clear created successfully message
			projCreatorPage.SelProjectType.SelectOption("TestProjType");
			projCreatorPage.BtnCreate.Click();
			sfPage1.TxtName.Value = name;
			sfPage1.BtnContinue.Click();
			foreach (var val in data) {
				var value = val; // avoid exposing foreach value in closure
				sfPage2.SetTestDe.Add<AddTestDePopup>(popup => popup.TxtString.Value = value);			
			}
			Assert.AreEqual(data[0], sfPage2.SetTestDe.GetValueAt(1, 0));
			Assert.AreEqual(data[1], sfPage2.SetTestDe.GetValueAt(1, 1));
			sfPage2.BtnContinue.Click();

			testPage.NavigateTo();
			testPage.Sets.SetCriteria("Set of String", data[0]);
			Wait.Until(d => name == testPage.Sets.GetValueAt(1, 0));
			testPage.Sets.SetCriteria("Set of String", data[1]);
			Wait.Until(d => name == testPage.Sets.GetValueAt(1, 0));
		}

		[Test(Description = "4.1.19 Enter filter, click link, go back, filter persists?")]
		public void FilterPersistance()
		{
			// For some reason Selenium works for this test if you return with the back key, but
			// not if you return with .NavigateTo().

			var testPage = new ProjectsCustomSearch();
			Store.LoginAsUser(Users.Admin);
			testPage.NavigateTo();
			var component = testPage.ProjectsComponent;
			var prefix = component.XpathPrefix;
			var selFilter = new Select(By.XPath(prefix + "//select[contains(@id,'_queryField1')]"));
			var txtFilter = new TextBox(By.XPath(prefix + "//input[contains(@id,'_queryCriteria1')]"));
			var lnkHome = new Link(By.LinkText("Home"));
			component.SetCriteria("Name", "%0001");
			
			// navigate away and return via back button
			lnkHome.Click();
			Wait.Until(d => new Link(By.LinkText("Project Creator")).Exists);
			CCPage.NavigateBack();
			Assert.AreEqual("Name", selFilter.SelectedOption);
			Assert.AreEqual("%0001", txtFilter.Value);
		}

		[Test(Description = "4.1.22 Filters of the pattern '%22' work correctly")]
		public void FiltersDoNotEncode()
		{
			var testPage = new ProjectsCustomSearch();
			Store.LoginAsUser(Users.Admin);
			testPage.NavigateTo();
			var component = testPage.ProjectsComponent;
			component.SetCriteria("Name", "%22");
			Assert.AreEqual("Search for '%22' on ID matches this, not a 'space'", component.GetValueAt(1, 0));
		}

		[Test(Description = "4.1.23 Works correctly if placed on the workspace of a project with '#' or '%' in the ID")]
		public void WorksInWorkspacesWithSpecialCharactersInId()
		{
			var workspace = new DefaultTemplate("X-00000013");
			Store.LoginAsUser(Users.Admin);
			workspace.NavigateTo();
			workspace.LnkContextualCustomSearchTab.Click();
			Wait.Until(d => "This project's name includes a '#' in it" == workspace.ContextualCustomSearch.GetValueAt(1, 0));
		}

		[Test(Description = "4.1.24 Do not show generic message for badly formed dates")]
		public void HandleBadlyFormedDates()
		{
			var testPage = new ProjectsCustomSearch();
			Store.LoginAsUser(Users.Admin);
			testPage.NavigateTo();
			var component = testPage.ProjectsComponent;
			component.SetCriteria("Date", "this day sucks");
			var spanError = new Container(By.CssSelector(".Error"));
			Wait.Until(d => spanError.Exists);
			Assert.AreEqual("Invalid Date/Time. Please use the format \"mm/dd/yyyy\" or \"mm/dd/yyyy hh:mm (AM/PM)\"", spanError.Text);
		}

		[Test(Description="4.1.5 Export to CSV")]
		public void ExportToCsv()
		{
			var testPage = new ProjectsCustomSearch();
			var component = testPage.ProjectsComponent;
			var exportPopup = new CustomSearchExportPopup();

			Store.LoginAsUser(Users.Admin);
			testPage.NavigateTo();
			component.RunOnPropertiesPopup<CustomSearchComponentPropertiesPopup>(properties =>
				((CustomSearchComponentPropertiesPopup) properties).ChkShowExportButton.Checked = true
			);
			component.BtnExport.Click();
			exportPopup.SwitchTo();
			Wait.Until(d => exportPopup.LnkDownload.Displayed);
			exportPopup.Close();
			exportPopup.SwitchBackToParent();
		}

		[Test(Description = "Test with _identity_ field works")]
		public void TestsWithIdentityFieldWorks()
		{
			Store.LoginAsUser(Users.Admin);
			var page = new CustomSearchTesting();
			page.NavigateTo();
			var link = new Link(By.LinkText("Projects Owned by Current User"));
			link.Click();
			var results = new CustomSearchResultsPopup();
			results.SwitchTo();
			Wait.Until(d => Link(By.LinkText("Another Clone")).Exists);
			Wait.Until(d => Link(By.LinkText("X-00000027")).Exists);
			results.Close();
			results.SwitchBackToParent();
		}
    }
}
