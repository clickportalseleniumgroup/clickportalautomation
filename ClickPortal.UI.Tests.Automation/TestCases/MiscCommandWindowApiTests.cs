﻿using System;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages.BasePages;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using NUnit.Framework;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
	[TestFixture]
	public class MiscCommandWindowApiTests : BaseTest
	{

		[Test(Description = "Copy a Document between two projects")]
		public void EntityUtils_ExecuteSqlWithResults()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			const String script =
				"var sql = 'select oid, extent, date from __TestDE_CustomAttributesManager';\n" +
				"var results = EntityUtils.ExecuteSQLWithResults(sql);\n" +
				"Assert.True(results.indexOf('<field fieldName=\"[date]\" fieldValue=') != -1);\n" +
				"? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}
	}
}
