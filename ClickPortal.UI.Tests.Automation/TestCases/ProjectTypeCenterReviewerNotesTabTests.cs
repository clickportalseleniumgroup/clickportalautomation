﻿using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages.BasePages.ProjectTypeCenter;
using CCWebUIAuto.PrimitiveElements;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
    [TestFixture]
    public class ProjectTypeCenterReviewerNotesTabTests : BaseTest
    {
        public const string TestProjType = "_VerySmallProjType";

        [Test(Description="1.10.1 Manage Note Types")]
        public void ManageReviewerNoteTypes()
        {
            var noteName = "testNote_" + DataGen.String(4);
            var modNoteName = "testNote_" + DataGen.String(4);
            var reviewerNotesTab = new ReviewerNotesTab(TestProjType);
            Store.LoginAsUser(Users.Admin);
            reviewerNotesTab.NavigateTo();
            reviewerNotesTab.CreateNoteType(noteName, true, "1");
            Assert.IsTrue(reviewerNotesTab.NoteTypeExists(noteName));
            reviewerNotesTab.ModifyNoteType(noteName, modNoteName);
            Assert.IsFalse(reviewerNotesTab.NoteTypeExists(noteName));
            Assert.IsTrue(reviewerNotesTab.NoteTypeExists(modNoteName));
            reviewerNotesTab.DeleteNoteType(modNoteName);
            Assert.IsFalse(reviewerNotesTab.NoteTypeExists(noteName));
            Assert.IsFalse(reviewerNotesTab.NoteTypeExists(modNoteName));
        }

        [Test(Description = "1.10.3 Manage Response Types")]
        public void ManageResponseTypes()
        {
            var responseName = "testResponse_" + DataGen.String(4);
            var modResponseName = "testResponse_" + DataGen.String(4);
			var reviewerNotesTab = new ReviewerNotesTab(TestProjType);
            Store.LoginAsUser(Users.Admin);
            reviewerNotesTab.NavigateTo();
            reviewerNotesTab.CreateResponseType(responseName, "1");
            Assert.IsTrue(reviewerNotesTab.NoteResponseTypeExists(responseName));
            reviewerNotesTab.ModifyResponseType(responseName, modResponseName);
            Assert.IsFalse(reviewerNotesTab.NoteResponseTypeExists(responseName));
            Assert.IsTrue(reviewerNotesTab.NoteResponseTypeExists(modResponseName));
            reviewerNotesTab.DeleteResponseType(modResponseName);
            Assert.IsFalse(reviewerNotesTab.NoteResponseTypeExists(responseName));
            Assert.IsFalse(reviewerNotesTab.NoteResponseTypeExists(modResponseName));
        }

        [Test(Description="1.10.5 Create, edit, and respond to reviewer note, in SmartForm")]
        public void CanManageNotesInSmartForm()
        {
            var noteText = "note_text_" + DataGen.String(8);
            var id = "Test_1_10_5_" + DataGen.String(4);
            
			Store.LoginAsUser(Users.TestWrite);
            var projCreatorPage = new ProjectCreator();
            projCreatorPage.NavigateTo();
            projCreatorPage.SelProjectType.SelectOption("TestProjType");
            projCreatorPage.BtnCreate.Click();
			var form = new Pages.TestStore.TestProjType.SmartFormPageBaseAttributes();
            form.TxtId.Value = id;
            form.TxtName.Value = id;
            form.BtnContinue.Click();
			
            var workspace = new Pages.TestStore.TestProjType.DefaultTemplate(id);
            workspace.NavigateTo();
            workspace.LnkEditProject.Click();
            form.BtnExpandNotes.Click();
            form.AddReviewerNote("Basic Note", noteText);
			Assert.IsTrue(new Link(By.LinkText("Basic Note")).Exists);
			Assert.IsTrue(new Link(By.XPath("//div[text()='" + noteText + "']")).Exists);

			var modifiedTextA = "note_text_" + DataGen.String(8);
			form.EditReviewerNoteWithPopup(0, "Response Required Note A", modifiedTextA);
			Assert.IsTrue(new Link(By.LinkText("Response Required Note A")).Exists);
			Assert.IsTrue(new Link(By.XPath("//div[text()='" + modifiedTextA + "']")).Exists);
			
			var modifiedTextB = "note_text_" + DataGen.String(8);
			form.EditReviewerNoteInline(0, "Response Required Note B", modifiedTextB);
			Assert.IsTrue(new Link(By.LinkText("Response Required Note B")).Exists);
			Assert.IsTrue(new Link(By.XPath("//div[text()='" + modifiedTextB + "']")).Exists);

			Store.LoginAsUser(Users.TestRespond);
			workspace.NavigateTo();
			workspace.LnkViewProject.Click();
			var responseA = "note_response_" + DataGen.String(8);
			form.RespondToNoteWithPopup(0, "A Response", responseA);
			Assert.IsTrue(new Link(By.XPath("//div[text()='" + responseA + "']")).Exists);
        }
    }
}
