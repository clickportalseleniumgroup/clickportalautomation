﻿using System;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages.BasePages;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using NUnit.Framework;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
	[TestFixture]
	class EntitySynchTests : BaseTest
	{
		[Test(Description = "Copy primitives between two projects")]
		public void CopyPrimitives()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			const String script = @"
				EntitySynch.useBetaVersion = false;
				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				var target = EntityCloner.quickClone(source);
				target.customAttributes.testDE.customAttributes.boolean = false;
				target.customAttributes.testDE.customAttributes.currency = source.customAttributes.testDE.customAttributes.currency + 2.2;
				target.customAttributes.testDE.customAttributes.date = new Date();
				target.customAttributes.testDE.customAttributes.double = source.customAttributes.testDE.customAttributes.double + 2.2;
				target.customAttributes.testDE.customAttributes.float = source.customAttributes.testDE.customAttributes.float + 2.2;
				target.customAttributes.testDE.customAttributes.integer = source.customAttributes.testDE.customAttributes.integer + 2;
				target.customAttributes.testDE.customAttributes.String = source.customAttributes.testDE.customAttributes.String + 'modified';
				Assert.False(source.customAttributes.testDE.customAttributes.boolean ==
							 target.customAttributes.testDE.customAttributes.boolean);
				Assert.False(source.customAttributes.testDE.customAttributes.currency ==
							 target.customAttributes.testDE.customAttributes.currency);
				Assert.False(source.customAttributes.testDE.customAttributes.date.getTime() ==
							 target.customAttributes.testDE.customAttributes.date.getTime());
				Assert.False(source.customAttributes.testDE.customAttributes.double ==
							 target.customAttributes.testDE.customAttributes.double);
				Assert.False(source.customAttributes.testDE.customAttributes.float ==
							 target.customAttributes.testDE.customAttributes.float);
				Assert.False(source.customAttributes.testDE.customAttributes.integer ==
							 target.customAttributes.testDE.customAttributes.integer);
				Assert.False(source.customAttributes.testDE.customAttributes.String ==
							 target.customAttributes.testDE.customAttributes.String);

				var synch = EntitySynch.createRequest(source, target);
				synch.defineSetKeysForType('_TestDE', 'customAttributes.String');
				synch.addAttributes('_TestProjType.name');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.boolean');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.currency');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.date');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.double');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.float');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.integer');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.String');
				synch.copy();
				Assert.True(source.customAttributes.testDE.customAttributes.boolean ==
							target.customAttributes.testDE.customAttributes.boolean);
				Assert.True(source.customAttributes.testDE.customAttributes.currency ==
							target.customAttributes.testDE.customAttributes.currency);
				Assert.True(source.customAttributes.testDE.customAttributes.date.getTime() ==
							target.customAttributes.testDE.customAttributes.date.getTime());
				Assert.True(source.customAttributes.testDE.customAttributes.double ==
							target.customAttributes.testDE.customAttributes.double);
				Assert.True(source.customAttributes.testDE.customAttributes.float ==
							target.customAttributes.testDE.customAttributes.float);
				Assert.True(source.customAttributes.testDE.customAttributes.integer ==
							target.customAttributes.testDE.customAttributes.integer);
				Assert.True(source.customAttributes.testDE.customAttributes.String ==
							target.customAttributes.testDE.customAttributes.String);
				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}

        [Test(Description = "Copy primitives between two projects - use beta version")]
        public void CopyPrimitivesBetaVersion()
        {
            Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
            const String script = @"
				EntitySynch.useBetaVersion = true;
				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				var target = EntityCloner.quickClone(source);
				target.customAttributes.testDE.customAttributes.boolean = false;
				target.customAttributes.testDE.customAttributes.currency = source.customAttributes.testDE.customAttributes.currency + 2.2;
				target.customAttributes.testDE.customAttributes.date = new Date();
				target.customAttributes.testDE.customAttributes.double = source.customAttributes.testDE.customAttributes.double + 2.2;
				target.customAttributes.testDE.customAttributes.float = source.customAttributes.testDE.customAttributes.float + 2.2;
				target.customAttributes.testDE.customAttributes.integer = source.customAttributes.testDE.customAttributes.integer + 2;
				target.customAttributes.testDE.customAttributes.String = source.customAttributes.testDE.customAttributes.String + 'modified';
				Assert.False(source.customAttributes.testDE.customAttributes.boolean ==
							 target.customAttributes.testDE.customAttributes.boolean);
				Assert.False(source.customAttributes.testDE.customAttributes.currency ==
							 target.customAttributes.testDE.customAttributes.currency);
				Assert.False(source.customAttributes.testDE.customAttributes.date.getTime() ==
							 target.customAttributes.testDE.customAttributes.date.getTime());
				Assert.False(source.customAttributes.testDE.customAttributes.double ==
							 target.customAttributes.testDE.customAttributes.double);
				Assert.False(source.customAttributes.testDE.customAttributes.float ==
							 target.customAttributes.testDE.customAttributes.float);
				Assert.False(source.customAttributes.testDE.customAttributes.integer ==
							 target.customAttributes.testDE.customAttributes.integer);
				Assert.False(source.customAttributes.testDE.customAttributes.String ==
							 target.customAttributes.testDE.customAttributes.String);

				var synch = EntitySynch.createRequest(source, target);
				synch.defineSetKeysForType('_TestDE', 'customAttributes.String');
				synch.addAttributes('_TestProjType.name');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.boolean');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.currency');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.date');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.double');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.float');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.integer');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.String');
				synch.copy();
				Assert.True(source.customAttributes.testDE.customAttributes.boolean ==
							target.customAttributes.testDE.customAttributes.boolean);
				Assert.True(source.customAttributes.testDE.customAttributes.currency ==
							target.customAttributes.testDE.customAttributes.currency);
				Assert.True(source.customAttributes.testDE.customAttributes.date.getTime() ==
							target.customAttributes.testDE.customAttributes.date.getTime());
				Assert.True(source.customAttributes.testDE.customAttributes.double ==
							target.customAttributes.testDE.customAttributes.double);
				Assert.True(source.customAttributes.testDE.customAttributes.float ==
							target.customAttributes.testDE.customAttributes.float);
				Assert.True(source.customAttributes.testDE.customAttributes.integer ==
							target.customAttributes.testDE.customAttributes.integer);
				Assert.True(source.customAttributes.testDE.customAttributes.String ==
							target.customAttributes.testDE.customAttributes.String);
				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
        }

		[Test(Description = "Copy a Document between two projects")]
		public void CopyDocument()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			const String script = @"
				EntitySynch.useBetaVersion = false;
				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				var q = getResultSet('Document').query('name != #n');
				q.bind('n', source.customAttributes.testDE.customAttributes.entityDocument.name);
				var doc = q.elements.item(1);
				var target = EntityCloner.quickClone(source);
				target.customAttributes.testDE.customAttributes.entityDocument = doc;
				Assert.False(source.customAttributes.testDE.customAttributes.entityDocument.targetURL ==
							 target.customAttributes.testDE.customAttributes.entityDocument.targetURL);

				var synch = EntitySynch.createRequest(source, target);
				synch.defineSetKeysForType('_TestDE', 'customAttributes.String');
				synch.addAttributes('_TestProjType.name');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.entityDocument');

				synch.copy();
				Assert.True(source.customAttributes.testDE.customAttributes.entityDocument.targetURL ==
							target.customAttributes.testDE.customAttributes.entityDocument.targetURL);
				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}

		[Test(Description = "Copy a Document between two projects")]
		public void CopyDocumentBetaVersion()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			const String script = @"
				EntitySynch.useBetaVersion = true;
				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				var q = getResultSet('Document').query('name != #n');
				q.bind('n', source.customAttributes.testDE.customAttributes.entityDocument.name);
				var doc = q.elements.item(1);
				var target = EntityCloner.quickClone(source);
				target.customAttributes.testDE.customAttributes.entityDocument = doc;
				Assert.False(source.customAttributes.testDE.customAttributes.entityDocument.targetURL ==
							 target.customAttributes.testDE.customAttributes.entityDocument.targetURL);

				var synch = EntitySynch.createRequest(source, target);
				synch.defineSetKeysForType('_TestDE', 'customAttributes.String');
				synch.addAttributes('_TestProjType.name');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.entityDocument');

				synch.copy();
				Assert.True(source.customAttributes.testDE.customAttributes.entityDocument.targetURL ==
							target.customAttributes.testDE.customAttributes.entityDocument.targetURL);
				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}

		[Test(Description = "Ensure the document doesn't copy if there is no change")]
		public void	EnsureDocumentDoesntCopyInappropriatly()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			const String script = @"
				EntitySynch.useBetaVersion = false;
				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				
				var target = EntityCloner.quickClone(source);
				var doc = target.customAttributes.testDE.customAttributes.entityDocument;

				var synch = EntitySynch.createRequest(source, target);
				synch.defineSetKeysForType('_TestDE', 'customAttributes.String');
				synch.addAttributes('_TestProjType.name');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.entityDocument');

				synch.copy();
				Assert.True(doc == target.customAttributes.testDE.customAttributes.entityDocument);
				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}

		[Test(Description = "Ensure the document doesn't copy if there is no change")]
		public void EnsureDocumentDoesntCopyInappropriatlyBetaVersion()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			const String script = @"
				EntitySynch.useBetaVersion = true;
				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				
				var target = EntityCloner.quickClone(source);
				var doc = target.customAttributes.testDE.customAttributes.entityDocument;

				var synch = EntitySynch.createRequest(source, target);
				synch.defineSetKeysForType('_TestDE', 'customAttributes.String');
				synch.addAttributes('_TestProjType.name');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.entityDocument');

				synch.copy();
				Assert.True(doc == target.customAttributes.testDE.customAttributes.entityDocument);
				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}

		[Test(Description = "Copy references between two projects")]
		public void CopyReferences()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			var personAstr = DataGen.AllLowerCaseString(8);
			var personBstr = DataGen.AllLowerCaseString(8);			
			var script = @"
				EntitySynch.useBetaVersion = false;
				var personA = Person.createEntity();
				personA.firstName = '" + personAstr + @"';
				personA.lastName = '" + personAstr + @"';
				personA.ID = Person.GetID('Person');

				var personB = Person.createEntity();
				personB.firstName = '" + personBstr + @"';
				personB.lastName = '" + personBstr + @"';
				personB.ID = Person.GetID('Person');

				var projSet = ApplicationEntity.getResultSet('_TestProjType').query('Name like \'%filler%\'').elements;
				var projA = projSet.item(1);
				var projB = projSet.item(2);

				var cdtSet = ApplicationEntity.getResultSet('_TestSelection').elements;
				var cdtA = cdtSet.item(1);
				var cdtB = cdtSet.item(2);

				var docA = ApplicationEntity.getResultSet('Document').elements.item(1);
				docA.Name = 'docA';
				var docBQuery = ApplicationEntity.getResultSet('Document').query('targetURL != #x');
				docBQuery.bind('x', docA.targetURL);
				var docB = docBQuery.elements.item(1);
				docB.Name = 'docB';

				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				var target = EntityCloner.quickClone(source);
				source.customAttributes.testDE.customAttributes.entityPerson = personA;
				target.customAttributes.testDE.customAttributes.entityPerson = personB;
				source.customAttributes.testDE.customAttributes.entityProject = projA;
				target.customAttributes.testDE.customAttributes.entityProject = projB;
				source.customAttributes.testDE.customAttributes.entitySelection = cdtA;
				target.customAttributes.testDE.customAttributes.entitySelection = cdtB;
				source.customAttributes.testDE.customAttributes.entityDocument = docA;
				target.customAttributes.testDE.customAttributes.entityDocument = docB;

				Assert.False(source.customAttributes.testDE.customAttributes.entityPerson ==
							 target.customAttributes.testDE.customAttributes.entityPerson);
				Assert.False(source.customAttributes.testDE.customAttributes.entityProject ==
							 target.customAttributes.testDE.customAttributes.entityProject);
				Assert.False(source.customAttributes.testDE.customAttributes.entitySelection ==
							 target.customAttributes.testDE.customAttributes.entitySelection);
				Assert.False(source.customAttributes.testDE.customAttributes.entityDocument.Name ==
							 target.customAttributes.testDE.customAttributes.entityDocument.Name);

				var synch = EntitySynch.createRequest(source, target);
				synch.defineSetKeysForType('_TestDE', 'customAttributes.String');
				synch.addAttributes('_TestProjType.name');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.entityPerson');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.entityProject');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.entitySelection');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.entityDocument');
				synch.copy();

				Assert.True(source.customAttributes.testDE.customAttributes.entityPerson ==
							target.customAttributes.testDE.customAttributes.entityPerson);
				Assert.True(source.customAttributes.testDE.customAttributes.entityProject ==
							target.customAttributes.testDE.customAttributes.entityProject);
				Assert.True(source.customAttributes.testDE.customAttributes.entitySelection ==
							target.customAttributes.testDE.customAttributes.entitySelection);
				Assert.True(source.customAttributes.testDE.customAttributes.entityDocument !=
							target.customAttributes.testDE.customAttributes.entityDocument);
				Assert.True(source.customAttributes.testDE.customAttributes.entityDocument.Name ==
							target.customAttributes.testDE.customAttributes.entityDocument.Name);
				
				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}

        [Test(Description = "Copy references between two projects - use beta version")]
        public void CopyReferencesBetaVersion()
        {
            Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
            var personAstr = DataGen.AllLowerCaseString(8);
            var personBstr = DataGen.AllLowerCaseString(8);
            var script = @"
				EntitySynch.useBetaVersion = true;
				var personA = Person.createEntity();
				personA.firstName = '" + personAstr + @"';
				personA.lastName = '" + personAstr + @"';
				personA.ID = Person.GetID('Person');

				var personB = Person.createEntity();
				personB.firstName = '" + personBstr + @"';
				personB.lastName = '" + personBstr + @"';
				personB.ID = Person.GetID('Person');

				var projSet = ApplicationEntity.getResultSet('_TestProjType').query('Name like \'%filler%\'').elements;
				var projA = projSet.item(1);
				var projB = projSet.item(2);

				var cdtSet = ApplicationEntity.getResultSet('_TestSelection').elements;
				var cdtA = cdtSet.item(1);
				var cdtB = cdtSet.item(2);

				var docSet = ApplicationEntity.getResultSet('Document').elements;
				var docA = docSet.item(1);
				docA.Name = 'docA';
				var docB = docSet.item(2);
				docB.Name = 'docB';

				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				var target = EntityCloner.quickClone(source);
				source.customAttributes.testDE.customAttributes.entityPerson = personA;
				target.customAttributes.testDE.customAttributes.entityPerson = personB;
				source.customAttributes.testDE.customAttributes.entityProject = projA;
				target.customAttributes.testDE.customAttributes.entityProject = projB;
				source.customAttributes.testDE.customAttributes.entitySelection = cdtA;
				target.customAttributes.testDE.customAttributes.entitySelection = cdtB;
				source.customAttributes.testDE.customAttributes.entityDocument = docA;
				target.customAttributes.testDE.customAttributes.entityDocument = docB;

				Assert.False(source.customAttributes.testDE.customAttributes.entityPerson ==
							 target.customAttributes.testDE.customAttributes.entityPerson);
				Assert.False(source.customAttributes.testDE.customAttributes.entityProject ==
							 target.customAttributes.testDE.customAttributes.entityProject);
				Assert.False(source.customAttributes.testDE.customAttributes.entitySelection ==
							 target.customAttributes.testDE.customAttributes.entitySelection);
				Assert.False(source.customAttributes.testDE.customAttributes.entityDocument.Name ==
							 target.customAttributes.testDE.customAttributes.entityDocument.Name);

				var synch = EntitySynch.createRequest(source, target);
				synch.defineSetKeysForType('_TestDE', 'customAttributes.String');
				synch.addAttributes('_TestProjType.name');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.entityPerson');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.entityProject');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.entitySelection');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.entityDocument');
				synch.copy();

				Assert.True(source.customAttributes.testDE.customAttributes.entityPerson ==
							target.customAttributes.testDE.customAttributes.entityPerson);
				Assert.True(source.customAttributes.testDE.customAttributes.entityProject ==
							target.customAttributes.testDE.customAttributes.entityProject);
				Assert.True(source.customAttributes.testDE.customAttributes.entitySelection ==
							target.customAttributes.testDE.customAttributes.entitySelection);
				Assert.True(source.customAttributes.testDE.customAttributes.entityDocument !=
							target.customAttributes.testDE.customAttributes.entityDocument);
				Assert.True(source.customAttributes.testDE.customAttributes.entityDocument.Name ==
							target.customAttributes.testDE.customAttributes.entityDocument.Name);
				
				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
        }

		[Test(Description = "Copy new data entry CDTs in a set between two projects, also remove a non-matching CDT from target set.")]
		public void CopyDecdtInSet()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			const String script = @"
				EntitySynch.useBetaVersion = false;
				var cdtA = _TestDE.createEntity();
				cdtA.setQualifiedAttribute('customAttributes.String', 'cdtA');
				
				var cdtB = _TestDE.createEntity();
				cdtB.setQualifiedAttribute('customAttributes.String', 'cdtB');

				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				var target = EntityCloner.quickClone(source);
				source.customAttributes.testDE.customAttributes.setTestDE = _TestDE.createEntitySet();
				target.customAttributes.testDE.customAttributes.setTestDE = _TestDE.createEntitySet();
				source.customAttributes.testDE.customAttributes.setTestDE.addElement(cdtA);
				target.customAttributes.testDE.customAttributes.setTestDE.addElement(cdtB);
				
				var synch = EntitySynch.createRequest(source, target);
				synch.defineSetKeysForType('_TestDE', 'customAttributes.String');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.setTestDE');
				synch.addAttributes('_TestDE.customAttributes.String');
				synch.copy();

				var sourceCdt = source.customAttributes.testDE.customAttributes.setTestDE.elements.item(1);
				var targetCdt = target.customAttributes.testDE.customAttributes.setTestDE.elements.item(1);

				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.count() == 1);
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.count() == 1);
				Assert.True(sourceCdt.customAttributes.String == 'cdtA');
				Assert.True(targetCdt.customAttributes.String == 'cdtA');
				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE !=
							target.customAttributes.testDE.customAttributes.setTestDE);

				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}

        [Test(Description = "Copy new data entry CDTs in a set between two projects, also remove a non-matching CDT from target set. - use beta version")]
        public void CopyDecdtInSetBetaVersion()
        {
            Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
            const String script = @"
				EntitySynch.useBetaVersion = true;
				var cdtA = _TestDE.createEntity();
				cdtA.setQualifiedAttribute('customAttributes.String', 'cdtA');
				
				var cdtB = _TestDE.createEntity();
				cdtB.setQualifiedAttribute('customAttributes.String', 'cdtB');

				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				var target = EntityCloner.quickClone(source);
				source.customAttributes.testDE.customAttributes.setTestDE = _TestDE.createEntitySet();
				target.customAttributes.testDE.customAttributes.setTestDE = _TestDE.createEntitySet();
				source.customAttributes.testDE.customAttributes.setTestDE.addElement(cdtA);
				target.customAttributes.testDE.customAttributes.setTestDE.addElement(cdtB);
				
				var synch = EntitySynch.createRequest(source, target);
				synch.defineSetKeysForType('_TestDE', 'customAttributes.String');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.setTestDE');
				synch.addAttributes('_TestDE.customAttributes.String');
				synch.copy();

				var sourceCdt = source.customAttributes.testDE.customAttributes.setTestDE.elements.item(1);
				var targetCdt = target.customAttributes.testDE.customAttributes.setTestDE.elements.item(1);

				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.count() == 1);
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.count() == 1);
				Assert.True(sourceCdt.customAttributes.String == 'cdtA');
				Assert.True(targetCdt.customAttributes.String == 'cdtA');
				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE !=
							target.customAttributes.testDE.customAttributes.setTestDE);

				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
        }

		[Test(Description = "Copies new data entry CTS into a targert set if there are cycles - use beta version")]
		public void CopyNewDataEntryCdtIntoTargetSetWithCycleBetaVersion()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			const String script = @"
				EntitySynch.useBetaVersion = true;
				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				var target = EntityCloner.quickClone(source);

				var sourceSet = source.customAttributes.testDE.customAttributes.setTestDE;
				var targetSet = target.customAttributes.testDE.customAttributes.setTestDE;
	
				var newEntity = _TestDE.createEntity();
				newEntity.setQualifiedAttribute('customAttributes.String', 'newly inserted');
				newEntity.setQualifiedAttribute('customAttributes.entityTestDE', source.customAttributes.testDE);
				sourceSet.addElement(newEntity);
		
				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.count() == 3, 'CopyNewDataEntryCDTIntoTargetSetWithCycle() source count before');
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.count() == 2, 'CopyNewDataEntryCDTIntoTargetSetWithCycle() target count before');
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.query('customAttributes.String=\'newly inserted\'').count() == 0, 'CopyNewDataEntryCDTIntoTargetSetWithCycle() target contents before');
				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.query('customAttributes.String=\'newly inserted\'').elements.item(1).customAttributes.entityTestDE == source.customAttributes.testDE, 'CopyNewDataEntryCDTIntoTargetSetWithCycle() source cycle before');
	
				var synch = EntitySynch.createRequest(source, target);
				synch.defineSetKeysForType('_TestDE', 'customAttributes.String');
				synch.addAttributes('_TestDE.customAttributes.entityTestDE');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.setTestDE');
				synch.addAttributes('_TestDE.customAttributes.String');
				synch.copy();
	
				var sourceTestDE = source.customAttributes.testDE;
				var sourceCycle = source.customAttributes.testDE.customAttributes.setTestDE.query('customAttributes.String=\'newly inserted\'').elements.item(1).customAttributes.entityTestDE;
				var targetTestDE = target.customAttributes.testDE;
				var targetCycle = target.customAttributes.testDE.customAttributes.setTestDE.query('customAttributes.String=\'newly inserted\'').elements.item(1).customAttributes.entityTestDE;
		
				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.count() == 3, 'CopyNewDataEntryCDTIntoTargetSetWithCycle() source count after');
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.count() == 3, 'CopyNewDataEntryCDTIntoTargetSetWithCycle() target count after');
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.query('customAttributes.String=\'newly inserted\'').elements.item(1) != 
							source.customAttributes.testDE.customAttributes.setTestDE.query('customAttributes.String=\'newly inserted\'').elements.item(1), 'CopyNewDataEntryCDTIntoTargetSetWithCycle() clone of contents after');
				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.query('customAttributes.String=\'newly inserted\'').elements.item(1).customAttributes.entityTestDE == source.customAttributes.testDE, 'CopyNewDataEntryCDTIntoTargetSetWithCycle() source cycle after');
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.query('customAttributes.String=\'newly inserted\'').elements.item(1).customAttributes.entityTestDE == target.customAttributes.testDE, 'CopyNewDataEntryCDTIntoTargetSetWithCycle() target cycle after');

				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}

		[Test(Description = "Copies new data entry CTS into a targert set if there are cycles")]
		public void SynchDocumentSetByRemovingExcess()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			const String script = @"
				EntitySynch.useBetaVersion = false;
				var docSet = ApplicationEntity.getResultSet('Document').elements;
				var docA = docSet.item(1);
				docA.Name = 'docA';
				var docB = docSet.item(2);
				docB.Name = 'docB';
				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				source.customAttributes.testDE.customAttributes.setDocument = Document.createEntitySet();
				source.customAttributes.testDE.customAttributes.setDocument.addElement(docA);
				
				var target = EntityCloner.quickClone(source);
				target.customAttributes.testDE.customAttributes.setDocument.addElement(docB);
				
				var synch = EntitySynch.createRequest(source, target);
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.setDocument');
				synch.copy();

				var sourceSet = source.customAttributes.testDE.customAttributes.setDocument.elements;
				var targetSet = target.customAttributes.testDE.customAttributes.setDocument.elements;

				Assert.True(sourceSet.count() == 1);
				Assert.True(targetSet.count() == 1);
				Assert.True(sourceSet.item(1).Name == docA.Name);
				Assert.True(targetSet.item(1).Name == docA.Name);

				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));		
		}

        [Test(Description = "Copies new data entry CTS into a targert set if there are cycles - use beta version")]
        public void SynchDocumentSetByRemovingExcessBetaVersion()
        {
            Store.LoginAsUser(Users.Admin);
            var cmd = new CommandWindow();
            const String script = @"
				EntitySynch.useBetaVersion = true;
				var docSet = ApplicationEntity.getResultSet('Document').elements;
				var docA = docSet.item(1);
				docA.Name = 'docA';
				var docB = docSet.item(2);
				docB.Name = 'docB';
				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				source.customAttributes.testDE.customAttributes.setDocument = Document.createEntitySet();
				source.customAttributes.testDE.customAttributes.setDocument.addElement(docA);
				
				var target = EntityCloner.quickClone(source);
				target.customAttributes.testDE.customAttributes.setDocument.addElement(docB);
				
				var synch = EntitySynch.createRequest(source, target);
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.setDocument');
				synch.copy();

				var sourceSet = source.customAttributes.testDE.customAttributes.setDocument.elements;
				var targetSet = target.customAttributes.testDE.customAttributes.setDocument.elements;

				Assert.True(sourceSet.count() == 1);
				Assert.True(targetSet.count() == 1);
				Assert.True(sourceSet.item(1).Name == docA.Name);
				Assert.True(targetSet.item(1).Name == docA.Name);

				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
        }

		[Test(Description = "Unregister Data Entry CDT removed from set")]
		public void UnregisterDataEntryCdtFromSet()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			const String script = @"
				EntitySynch.useBetaVersion = false;
				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				source.customAttributes.testDE.customAttributes.setTestDE = _TestDE.createEntitySet();
				var target = EntityCloner.quickClone(source);
				var srcCDT = _TestDE.createEntity();
				var tgtCDT = _TestDE.createEntity();
				source.customAttributes.testDE.customAttributes.setTestDE.addElement(srcCDT);
				target.customAttributes.testDE.customAttributes.setTestDE.addElement(tgtCDT);
				srcCDT.setQualifiedAttribute('customAttributes.String', 'I am from source');
				tgtCDT.setQualifiedAttribute('customAttributes.String', 'I am from target');
				srcCDT.setQualifiedAttribute('customAttributes.integer', 0);
				tgtCDT.setQualifiedAttribute('customAttributes.integer', 1);
				srcCDT.owningEntity = source;
				tgtCDT.owningEntity = target;
				Assert.True(getResultSet('_TestDE').query('customAttributes.String = \'I am from target\'').count() == 1);

				var synch = EntitySynch.createRequest(source, target);
				synch.defineSetKeysForType('_TestDE', 'customAttributes.integer');
				synch.addAttributes('_TestDE.customAttributes.String');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.setTestDE');
				synch.copy();
				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.count() == 1);
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.count() == 1);
				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.elements.item(1) != target.customAttributes.testDE.customAttributes.setTestDE.elements.item(1));
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.elements.item(1).customAttributes.String == 'I am from source');
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.elements.item(1).owningEntity == target);
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.elements.item(1) != tgtCDT);
				Assert.True(tgtCDT.owningEntity == null);
				Assert.True(getResultSet('_TestDE').query('customAttributes.String = \'I am from target\'').count() == 0);
				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}

        [Test(Description = "Unregister Data Entry CDT removed from set - use beta version")]
        public void UnregisterDataEntryCdtFromSetBetaVersion()
        {
            Store.LoginAsUser(Users.Admin);
            var cmd = new CommandWindow();
            const String script = @"
				EntitySynch.useBetaVersion = true;
				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				source.customAttributes.testDE.customAttributes.setTestDE = _TestDE.createEntitySet();
				var target = EntityCloner.quickClone(source);
				var srcCDT = _TestDE.createEntity();
				var tgtCDT = _TestDE.createEntity();
				source.customAttributes.testDE.customAttributes.setTestDE.addElement(srcCDT);
				target.customAttributes.testDE.customAttributes.setTestDE.addElement(tgtCDT);
				srcCDT.setQualifiedAttribute('customAttributes.String', 'I am from source');
				tgtCDT.setQualifiedAttribute('customAttributes.String', 'I am from target');
				srcCDT.setQualifiedAttribute('customAttributes.integer', 0);
				tgtCDT.setQualifiedAttribute('customAttributes.integer', 1);
				srcCDT.owningEntity = source;
				tgtCDT.owningEntity = target;
				Assert.True(getResultSet('_TestDE').query('customAttributes.String = \'I am from target\'').count() == 1);

				var synch = EntitySynch.createRequest(source, target);
				synch.defineSetKeysForType('_TestDE', 'customAttributes.integer');
				synch.addAttributes('_TestDE.customAttributes.String');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.setTestDE');
				synch.copy();
				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.count() == 1);
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.count() == 1);
				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.elements.item(1) != target.customAttributes.testDE.customAttributes.setTestDE.elements.item(1));
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.elements.item(1).customAttributes.String == 'I am from source');
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.elements.item(1).owningEntity == target);
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.elements.item(1) != tgtCDT);
				Assert.True(tgtCDT.owningEntity == null);
				Assert.True(getResultSet('_TestDE').query('customAttributes.String = \'I am from target\'').count() == 0);
				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
        }

		[Test(Description="Used metaCloneId as default set key for CDT sets")]
		public void UsesMetaCloneIdAsDefaultSetKey()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			const String script = @"
				EntitySynch.useBetaVersion = false;
				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				source.customAttributes.testDE.customAttributes.setTestDE = _TestDE.createEntitySet();
				
				var srcCDTA = _TestDE.createEntity();
				srcCDTA.setQualifiedAttribute('customAttributes.String', 'modify me');
				srcCDTA.owningEntity = source;
				source.customAttributes.testDE.customAttributes.setTestDE.addElement(srcCDTA);

				var srcCDTB = _TestDE.createEntity();
				srcCDTB.setQualifiedAttribute('customAttributes.String', 'delete me');
				srcCDTB.owningEntity = source;
				source.customAttributes.testDE.customAttributes.setTestDE.addElement(srcCDTB);

				var target = EntityCloner.quickClone(source);
				srcCDTA.setQualifiedAttribute('customAttributes.String', 'I am from source');
				source.customAttributes.testDE.customAttributes.setTestDE.removeElement(srcCDTB);
				srcCDTB.unregisterEntity();

				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.count() == 1);
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.count() == 2);
				
				var synch = EntitySynch.createRequest(source, target);
				synch.addAttributes('_TestDE.customAttributes.String');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.setTestDE');
				synch.copy();

				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.count() == 1);
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.count() == 1);
				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.elements.item(1) != 
							target.customAttributes.testDE.customAttributes.setTestDE.elements.item(1));
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.elements.item(1).customAttributes.String == 'I am from source');

				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}

		[Test(Description = "Sets metaCloneId on new data entry CDT set elements")]
		public void SetsMetaCloneIdOnNewCdtSetElements()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			const String script = @"
				EntitySynch.useBetaVersion = false;
				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				source.customAttributes.testDE.customAttributes.setTestDE = _TestDE.createEntitySet();
				
				var target = EntityCloner.quickClone(source);

				var srcCDTA = _TestDE.createEntity();
				srcCDTA.setQualifiedAttribute('customAttributes.String', 'I am from source');
				srcCDTA.owningEntity = source;
				source.customAttributes.testDE.customAttributes.setTestDE.addElement(srcCDTA);

				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.count() == 1);
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.count() == 0);
				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.elements.item(1).metaCloneId == null);
				
				var synch = EntitySynch.createRequest(source, target);
				synch.addAttributes('_TestDE.customAttributes.String');
				synch.addAttributes('_TestProjType.customAttributes.testDE.customAttributes.setTestDE');
				synch.copy();

				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.count() == 1);
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.count() == 1);
				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.elements.item(1) != 
							target.customAttributes.testDE.customAttributes.setTestDE.elements.item(1));
				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.elements.item(1).metaCloneId == 
							target.customAttributes.testDE.customAttributes.setTestDE.elements.item(1).metaCloneId);
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.elements.item(1).customAttributes.String == 'I am from source');

				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}

		[Test(Description = "Assigns metaCloneId values to two entity graphs")]
		public void LinkTwoGraphs()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			const String script = @"
			var docSet = ApplicationEntity.getResultSet('Document').elements;
			var docA = docSet.item(1);
			docA.Name = 'docA';
			var docB = docSet.item(1);
			docB.Name = 'docB';

			var deCdtAA = _TestDE.createEntity();
			deCdtAA.setQualifiedAttribute('customAttributes.String', 'cdtA');
			var deCdtAB = _TestDE.createEntity();
			deCdtAB.setQualifiedAttribute('customAttributes.String', 'cdtB');
			var deCdtBA = _TestDE.createEntity();
			deCdtBA.setQualifiedAttribute('customAttributes.String', 'cdtA');
			var deCdtBB = _TestDE.createEntity();
			deCdtBB.setQualifiedAttribute('customAttributes.String', 'cdtB');

			var source = _TestProjType.CreateEntity();
			source.ID = _TestProjType.getID();
			source.setQualifiedAttribute('customAttributes.testDE.customAttributes.entityDocument', docA);
			source.setQualifiedAttribute('customAttributes.testDE.customAttributes.setTestDE', _TestDE.createEntitySet());
			source.customAttributes.testDE.customAttributes.setTestDE.addElement(deCdtAA);
			source.customAttributes.testDE.customAttributes.setTestDE.addElement(deCdtAB);

			var target = _TestProjType.CreateEntity();
			target.ID = _TestProjType.getID();
			target.setQualifiedAttribute('customAttributes.testDE.customAttributes.entityDocument', docB);
			target.setQualifiedAttribute('customAttributes.testDE.customAttributes.setTestDE', _TestDE.createEntitySet());
			target.customAttributes.testDE.customAttributes.setTestDE.addElement(deCdtBA);
			target.customAttributes.testDE.customAttributes.setTestDE.addElement(deCdtBB);


			EntityGraphSynch.LinkEntityGraphs(source, target, 'Document.name', '_TestDE.customAttributes.String');
			Assert.True(source.metaCloneId == target.metaCloneId);
			Assert.True(deCdtAA.metaCloneId == deCdtBA.metaCloneId);
			Assert.True(deCdtAB.metaCloneId == deCdtBB.metaCloneId);
			Assert.True(docA.metaCloneId == docB.metaCloneId);

			? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}
	}
}
