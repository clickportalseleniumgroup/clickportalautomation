﻿using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.BasePages.DataTypeCenter;
using CCWebUIAuto.PrimitiveElements;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
    [TestFixture]
    public class SelectionCdt : BaseTest
    {
        public const string TestDataType = "_TestSelection";

        [Test(Description = "2.2.0 Create a Selection CDT")]
        public void CreateSelectionCdt()
        {
            var newCustomDataTypeName = "Test_2_2_0_" + DataGen.String(4);
            var customDataTypeCenter = new DataTypeCenter();
            var newCustomDataTypeForm = new PropertiesTab("_" + TestDataType);

            Store.LoginAsUser(Users.Admin);
            customDataTypeCenter.NavigateTo();
            customDataTypeCenter.BtnNew.Click();
            newCustomDataTypeForm.TxtDisplayName.Value = newCustomDataTypeName;
            newCustomDataTypeForm.RdoSelection.Selected = true;
            newCustomDataTypeForm.BtnOk.Click();
            customDataTypeCenter.NavigateTo();
            Assert.IsTrue(customDataTypeCenter.Table.Text.Contains(newCustomDataTypeName));
        }

        [Test(Description = "2.2.1 Add Attributes -- Boolean")]
        public void AddAttribute_Boolean()
        {
			var propertiesTab = new PropertiesTab(TestDataType);
            const string prefix = "boolean_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            propertiesTab.NavigateTo();
            propertiesTab.CreateNewCustomAttribute(
                CustomAttributeDataType.Boolean,
                displayName, 
                internalName, 
                description, 
                booleanDisplay: "On/Off");
            PopUpWindow.SwitchTo("Data Type (TestSelection)");
            Assert.IsTrue(propertiesTab.CustomAttributeExists(internalName));
        }

        [Test(Description = "2.2.2 Add Attribute -- Currency")]
        public void AddAttribute_Currency()
        {
            var propertiesTab = new PropertiesTab(TestDataType);
            const string prefix = "currency_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            propertiesTab.NavigateTo();
            propertiesTab.CreateNewCustomAttribute( 
                CustomAttributeDataType.Currency,
                displayName,
                internalName,
                description,
                currencyType: "Chilean Peso/CL [1146] ($)");
            PopUpWindow.SwitchTo("Data Type (TestSelection)");
            Assert.IsTrue(propertiesTab.CustomAttributeExists(internalName));
        }

        [Test(Description = "2.2.3 Add Attributes -- Date")]
        public void AddAttribute_Date()
        {
            var propertiesTab = new PropertiesTab(TestDataType);
            const string prefix = "date_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            propertiesTab.NavigateTo();
            propertiesTab.CreateNewCustomAttribute( 
                CustomAttributeDataType.Date,
                displayName,
                internalName,
                description,
                defaultValue: "1/1/01", 
                displayFormat: "Verbose" );
            PopUpWindow.SwitchTo("Data Type (TestSelection)");
            Assert.IsTrue(propertiesTab.CustomAttributeExists(internalName));
        }

        [Test(Description = "2.2.4 Add Attributes -- Document Content")]
        public void AddAttribute_DocumentContent()
        {
            var propertiesTab = new PropertiesTab(TestDataType);
            const string prefix = "docCon_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            propertiesTab.NavigateTo();
            propertiesTab.CreateNewCustomAttribute( 
                CustomAttributeDataType.DocumentContent,
                displayName,
                internalName,
                description,
                readOnlyString: "readOnlyDocString");
            PopUpWindow.SwitchTo("Data Type (TestSelection)");
            Assert.IsTrue(propertiesTab.CustomAttributeExists(internalName));
        }

        [Test(Description = "2.2.5 Add Attributes -- Double" )]
        public void AddAttribute_Double()
        {
            var propertiesTab = new PropertiesTab(TestDataType);
            const string prefix = "double_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);       
            propertiesTab.NavigateTo();
            propertiesTab.CreateNewCustomAttribute( 
                CustomAttributeDataType.Double,
                displayName,
                internalName,
                description,
                defaultValue: "1.23456789");
            PopUpWindow.SwitchTo("Data Type (TestSelection)");
            Assert.IsTrue(propertiesTab.CustomAttributeExists(internalName));
        }

        [Test(Description = "2.2.6 Add Attributes -- Entity of Type")]
        public void AddAttribute_EntityType()
        {
            var propertiesTab = new PropertiesTab(TestDataType);
            const string prefix = "entity_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);    
            propertiesTab.NavigateTo();
            // TODO need to put in default value, but need to select EType
            propertiesTab.CreateNewCustomAttribute( 
                CustomAttributeDataType.EntityOfType,
                displayName,
                internalName,
                description,
                readOnlyString: "PersonEntityType_ReadOnlyString", 
                entityType: "Person");
            PopUpWindow.SwitchTo("Data Type (TestSelection)");
            Assert.IsTrue(propertiesTab.CustomAttributeExists(internalName));
        }

        [Test(Description = "2.2.7 Add Attributes -- Entity of Float")]
        public void AddAttribute_Float()
        {
            var propertiesTab = new PropertiesTab(TestDataType);
            const string prefix = "float_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            propertiesTab.NavigateTo();
            propertiesTab.CreateNewCustomAttribute( 
                CustomAttributeDataType.Float,
                displayName,
                internalName,
                description,
                defaultValue: "1.234567");
            PopUpWindow.SwitchTo("Data Type (TestSelection)");
            Assert.IsTrue(propertiesTab.CustomAttributeExists(internalName));
        }

        [Test(Description = "2.2.8 Add Attributes -- Integer")]
        public void AddAttribute_Integer()
        {
            var propertiesTab = new PropertiesTab(TestDataType);
            const string prefix = "integer_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            propertiesTab.NavigateTo();
            propertiesTab.CreateNewCustomAttribute( 
                CustomAttributeDataType.Integer,
                displayName,
                internalName,
                description,
                defaultValue: "1");
            PopUpWindow.SwitchTo("Data Type (TestSelection)");
            Assert.IsTrue(propertiesTab.CustomAttributeExists(internalName));
        }

        [Test(Description = "2.2.9 Add Attributes -- SetOfType ")]
        public void AddAttribute_SetOfType()
        {
            var propertiesTab = new PropertiesTab(TestDataType);
            const string prefix = "eset_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            propertiesTab.NavigateTo();
            propertiesTab.CreateNewCustomAttribute( 
                CustomAttributeDataType.SetOfType,
                displayName,
                internalName,
                description,
                setOfType: "Project", 
                readOnlyString: "SetTypesReadOnlyString");
            PopUpWindow.SwitchTo("Data Type (TestSelection)");
            Assert.IsTrue(propertiesTab.CustomAttributeExists(internalName));
        }

        [Test(Description = "2.3.0 Add Attributes -- String  ")]
        public void AddAttribute_String()
        {
            var propertiesTab = new PropertiesTab(TestDataType);
            const string prefix = "string_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            propertiesTab.NavigateTo();
            propertiesTab.CreateNewCustomAttribute( 
                CustomAttributeDataType.String,
                displayName,
                internalName,
                description,
                defaultValue: "stringDefaultValue");
            PopUpWindow.SwitchTo("Data Type (TestSelection)");
            Assert.IsTrue(propertiesTab.CustomAttributeExists(internalName));
        }

        [Test(Description = "2.3.0 Add Attributes -- String Multiline ")]
        public void AddAttribute_multiString()
        {
            var propertiesTab = new PropertiesTab(TestDataType);
            const string prefix = "multiString_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);         
            propertiesTab.NavigateTo();
            propertiesTab.CreateNewCustomAttribute( 
                CustomAttributeDataType.String,
                displayName,
                internalName,
                description,
                defaultValue: "multiStringDefaultValue", 
                multiLineString: true);
            PopUpWindow.SwitchTo("Data Type (TestSelection)");
            Assert.IsTrue(propertiesTab.CustomAttributeExists(internalName));
        }

        [Test(Description = "2.3.0 Add Attributes -- String Multiline ")]
        public void AddAttribute_enumeratedStringList()
        {
            var propertiesTab = new PropertiesTab(TestDataType);
            const string prefix = "enum_";
            var displayName = prefix + DataGen.String(4);
            var internalName = prefix + DataGen.String(4);
            var description = prefix + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            propertiesTab.NavigateTo();
            propertiesTab.CreateNewCustomAttribute(
                CustomAttributeDataType.Enumeration,
                displayName,
                internalName,
                description,
                enumValues: new [] {"enumA", "enumB"});
            PopUpWindow.SwitchTo("Data Type (TestSelection)");
            Assert.IsTrue(propertiesTab.CustomAttributeExists(internalName));
        }

        [Test(Description = "2.3.3 -- Change Display Name ")]
        public void ModifyDisplayName()
        {
            var origDisplayName = "Test_2_2_" + DataGen.String(4);
            var modDisplayName = "Test_2_2_" + DataGen.String(4);
            var dataTypeCenter = new DataTypeCenter();
            var propertiesTab = new PropertiesTab("_" + origDisplayName);
            
            Store.LoginAsUser(Users.Admin);

            dataTypeCenter.NavigateTo();
            dataTypeCenter.BtnNew.Click();
            propertiesTab.TxtDisplayName.Value = origDisplayName;
            propertiesTab.RdoSelection.Selected = true;
            propertiesTab.BtnOk.Click();
            dataTypeCenter.NavigateTo();
            Assert.IsTrue(dataTypeCenter.Table.Text.Contains(origDisplayName));

            propertiesTab.NavigateTo();
			propertiesTab.TxtDisplayName.Value = modDisplayName;
            propertiesTab.BtnOk.Click();
            dataTypeCenter.NavigateTo();
            ClickPortalUI.Wait.Until(d => dataTypeCenter.Table.Text.Contains(modDisplayName));
            Assert.IsTrue(dataTypeCenter.Table.Text.Contains(modDisplayName));
        }

        [Test(Description = "2.3.4 -- Delete Custom Attribute ")]
        public void DeleteCustomAttribute()
        {
            var propertiesTab = new PropertiesTab(TestDataType);
            Store.LoginAsUser(Users.Admin);
            propertiesTab.NavigateTo();
            propertiesTab.CreateNewCustomAttribute( CustomAttributeDataType.String, "delMe", "delMe");
            PopUpWindow.SwitchTo("Data Type (TestSelection)");
            propertiesTab.DeleteCustomAttribute("delMe");
            Assert.IsFalse(propertiesTab.CustomAttributeExists("delMe"));
        }

        [Test(Description = "2.3.5 -- CreateInstanceBinder View")]
        public void CreateView()
        {
            var viewName = "Test_2_3_5_" + DataGen.String(4);
            var viewsTab = new ViewsTab(TestDataType);
            Store.LoginAsUser(Users.Admin);
            viewsTab.NavigateTo();
            viewsTab.CreateNewView(viewName);
            Assert.IsTrue(viewsTab.ViewExists(viewName));
        }

        [Test(Description = "2.3.6 -- Edit Entity Type View Name")]
        public void EditView()
        {
            var origName = "Test_2_3_6_" + DataGen.String(4);
            var modName = "Test_2_3_6_" + DataGen.String(4);
            var viewsTab = new ViewsTab(TestDataType);
            Store.LoginAsUser(Users.Admin);
            viewsTab.NavigateTo();
            viewsTab.CreateNewView(origName);
            viewsTab.ModifyViewName(origName, modName);
            Assert.IsTrue(viewsTab.ViewExists(modName));
        }

        [Test(Description = "2.3.7 -- Edit View Properties")]
        [Ignore("Under Construction")]
        public void EditStandardViewProperties()
        {
            var viewsTab = new ViewsTab(TestDataType);
            Store.LoginAsUser(Users.Admin);
            viewsTab.NavigateTo();
        }

        /// <summary>
        /// Creates a view, then deletes it
        /// </summary>
        [Test(Description = "2.3.8 -- Delete View")]
        public void DeleteView()
        {
            var viewName = "Test-2_3_8_" + DataGen.String(4);
            var viewsTab = new ViewsTab(TestDataType);
            Store.LoginAsUser(Users.Admin);   
            viewsTab.NavigateTo();
            viewsTab.CreateNewView(viewName);
            Assert.IsTrue(viewsTab.ViewExists(viewName));
            viewsTab.DeleteView(viewName);
            Assert.IsFalse(viewsTab.ViewExists(viewName));
        }

        /// <summary>
        /// Creates a chooser with one display (with sorting / filtering allowed) and a filter field.
        /// </summary>
		[Test(Description = "2.3.9 - Create chooser, 2.4.0 - Modify chooser, 2.4.1 - Set default chooser, 2.4.2 - Delete chooser")]
        public void ManageChoosers()
        {
            var myDataTypeChoosersTab = new ChoosersTab(TestDataType);
	        var chooserName = "2_2_Chooser_" + DataGen.String(4);
            Store.LoginAsUser(Users.Admin);
            
			myDataTypeChoosersTab.NavigateTo();
			myDataTypeChoosersTab.CreateChooser(chooserName, "string", "ID");
			Assert.IsTrue(myDataTypeChoosersTab.VerifyChooserExists(chooserName));

			var modChooserName = "2_2_Chooser_" + DataGen.String(4);
			var viewNameLink = new Link(By.LinkText(chooserName));
			viewNameLink.Click();
			var popup = new EntityChooserPopup();
			popup.SwitchTo();
			popup.SetChooserName(modChooserName);
			popup.BtnOk.Click();
			popup.SwitchBackToParent();
			Assert.IsFalse(myDataTypeChoosersTab.VerifyChooserExists(chooserName));
			Assert.IsTrue(myDataTypeChoosersTab.VerifyChooserExists(modChooserName));

			myDataTypeChoosersTab.SetChooserAsDefault(modChooserName);
	        Assert.IsTrue(myDataTypeChoosersTab.VerifyChooserIsDefault(modChooserName));

			myDataTypeChoosersTab.DeleteChooser(modChooserName);
			Assert.IsFalse(myDataTypeChoosersTab.VerifyChooserExists(modChooserName));
 
		}

        [Test(Description = "2.4.3 -- Add Entity")]
        public void A_AddEntity() // must run before custom attribute tests
        {
            var entityValue = "Test_2_4_3_" + DataGen.String(4);
            var myDataTypeDataTab = new DataTab(TestDataType);
            Store.LoginAsUser(Users.Admin);
            myDataTypeDataTab.NavigateTo();
            Assert.IsFalse(myDataTypeDataTab.VerifyEntityDataExists(entityValue));
            myDataTypeDataTab.CreateEntityData(entityValue);
            Assert.IsTrue(myDataTypeDataTab.VerifyEntityDataExists(entityValue));
        }

        [Test(Description = "2.4.4 -- Edit Entity Name")]
        public void A_EditEntityName() // must run before custom attribute tests
        {
            var entityName = "Test_2_4_4_" + DataGen.String(4);
            var modEntityName = "Test_2_4_4_" + DataGen.String(4);
            var myDataTypeDataTab = new DataTab(TestDataType);
            Store.LoginAsUser(Users.Admin);
            myDataTypeDataTab.NavigateTo();
            myDataTypeDataTab.CreateEntityData(entityName);
            Assert.IsTrue(myDataTypeDataTab.VerifyEntityDataExists(entityName));
            myDataTypeDataTab.ModifyEntityName(entityName, modEntityName);
            Assert.IsTrue(myDataTypeDataTab.VerifyEntityDataExists(modEntityName));
            Assert.IsFalse(myDataTypeDataTab.VerifyEntityDataExists(entityName));
        }

        [Test(Description = "2.4.5 -- Remove Entity")]
        public void A_RemoveEntity() // must run before custom attribute tests
        {
            var entityName = "test-2.2-delMe_" + DataGen.String(4);
            var myDataTypeDataTab = new DataTab(TestDataType);
            Store.LoginAsUser(Users.Admin);
            myDataTypeDataTab.NavigateTo();
            myDataTypeDataTab.CreateEntityData(entityName);
            Assert.IsTrue(myDataTypeDataTab.VerifyEntityDataExists(entityName));
            myDataTypeDataTab.DeleteEntity(entityName);
            Assert.IsFalse(myDataTypeDataTab.VerifyEntityDataExists(entityName));
        }

        [Test(Description = "2.4.6 -- SetDisplayValue")]
        public void SetDisplayAttribute()
        {
            var propertiesTab = new PropertiesTab(TestDataType);
            Store.LoginAsUser(Users.Admin);
            propertiesTab.NavigateTo();
            propertiesTab.SetDisplayValue("string");
            Assert.IsTrue(propertiesTab.VerifyDisplayValue("string"));
            propertiesTab.SetDisplayValue("ID");
            Assert.IsTrue(propertiesTab.VerifyDisplayValue("ID"));
        }
    }
}