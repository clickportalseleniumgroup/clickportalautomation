﻿using System;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages.BasePages;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using NUnit.Framework;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
	[TestFixture]
	class EntityClonerTests : BaseTest
	{
		[Test(Description = "Copy primitives")]
		public void CopyPrimitives()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			const String script = @"
				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				source.customAttributes.testDE.customAttributes.boolean = false;
				source.customAttributes.testDE.customAttributes.currency = source.customAttributes.testDE.customAttributes.currency + 2.2;
				source.customAttributes.testDE.customAttributes.date = new Date();
				source.customAttributes.testDE.customAttributes.double = source.customAttributes.testDE.customAttributes.double + 2.2;
				source.customAttributes.testDE.customAttributes.float = source.customAttributes.testDE.customAttributes.float + 2.2;
				source.customAttributes.testDE.customAttributes.integer = source.customAttributes.testDE.customAttributes.integer + 2;
				source.customAttributes.testDE.customAttributes.string = source.customAttributes.testDE.customAttributes.string + 'modified';
				var target = EntityCloner.quickClone(source);
				Assert.True(source.customAttributes.testDE.customAttributes.boolean ==
							target.customAttributes.testDE.customAttributes.boolean);
				Assert.True(source.customAttributes.testDE.customAttributes.currency ==
							target.customAttributes.testDE.customAttributes.currency);
				Assert.True(source.customAttributes.testDE.customAttributes.date.getTime() ==
							target.customAttributes.testDE.customAttributes.date.getTime());
				Assert.True(source.customAttributes.testDE.customAttributes.double ==
							target.customAttributes.testDE.customAttributes.double);
				Assert.True(source.customAttributes.testDE.customAttributes.float ==
							target.customAttributes.testDE.customAttributes.float);
				Assert.True(source.customAttributes.testDE.customAttributes.integer ==
							target.customAttributes.testDE.customAttributes.integer);
				Assert.True(source.customAttributes.testDE.customAttributes.string ==
							target.customAttributes.testDE.customAttributes.string);
				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}

		[Test(Description = "Copy references")]
		public void CopyReferences()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			var personAstr = DataGen.AllLowerCaseString(8);
			var script = @"
				var personA = Person.createEntity();
				personA.firstName = '" + personAstr + @"';
				personA.lastName = '" + personAstr + @"';
				personA.ID = Person.GetID('Person');

				var projSet = ApplicationEntity.getResultSet('_TestProjType').query('Name like \'%filler%\'').elements;
				var projA = projSet.item(1);

				var cdtSet = ApplicationEntity.getResultSet('_TestSelection').elements;
				var cdtA = cdtSet.item(1);

				var docSet = ApplicationEntity.getResultSet('Document').elements;
				var docA = docSet.item(1);
				docA.Name = 'docA';

				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				source.customAttributes.testDE.customAttributes.entityPerson = personA;
				source.customAttributes.testDE.customAttributes.entityProject = projA;
				source.customAttributes.testDE.customAttributes.entitySelection = cdtA;
				source.customAttributes.testDE.customAttributes.entityDocument = docA;
				var target = EntityCloner.quickClone(source);

				Assert.True(source.customAttributes.testDE.customAttributes.entityPerson ==
							target.customAttributes.testDE.customAttributes.entityPerson);
				Assert.True(source.customAttributes.testDE.customAttributes.entityProject ==
							target.customAttributes.testDE.customAttributes.entityProject);
				Assert.True(source.customAttributes.testDE.customAttributes.entitySelection ==
							target.customAttributes.testDE.customAttributes.entitySelection);
				Assert.True(source.customAttributes.testDE.customAttributes.entityDocument !=
							target.customAttributes.testDE.customAttributes.entityDocument);
				Assert.True(source.customAttributes.testDE.customAttributes.entityDocument.Name ==
							target.customAttributes.testDE.customAttributes.entityDocument.Name);
				
				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}

		[Test(Description = "Copy data entry CDTs in a set")]
		public void CopydeCdtAInSet()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			const string script = @"
				var cdtA = _TestDE.createEntity();
				cdtA.setQualifiedAttribute('customAttributes.string', 'cdtA');

				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				source.customAttributes.testDE.customAttributes.setTestDE = _TestDE.createEntitySet();
				source.customAttributes.testDE.customAttributes.setTestDE.addElement(cdtA);
				var target = EntityCloner.quickClone(source);

				var sourceCdt = source.customAttributes.testDE.customAttributes.setTestDE.elements.item(1);
				var targetCdt = target.customAttributes.testDE.customAttributes.setTestDE.elements.item(1);

				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE.count() == 1);
				Assert.True(target.customAttributes.testDE.customAttributes.setTestDE.count() == 1);
				Assert.True(sourceCdt.customAttributes.string == 'cdtA');
				Assert.True(targetCdt.customAttributes.string == 'cdtA');
				Assert.True(source.customAttributes.testDE.customAttributes.setTestDE !=
							target.customAttributes.testDE.customAttributes.setTestDE);

				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}

		[Test(Description="Verify newly created entities have no metaCloneId, and that EntityCloner assigns it correctly")]
		public void AssignMetaCloneId()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			var personAstr = DataGen.AllLowerCaseString(8);
			var script = @"
				var personA = Person.createEntity();
				personA.firstName = '" + personAstr + @"';
				personA.lastName = '" + personAstr + @"';
				personA.ID = Person.GetID('Person');

				var docSet = ApplicationEntity.getResultSet('Document').elements;
				var docA = docSet.item(1);
				docA.Name = 'docA';

				var cdtSet = ApplicationEntity.getResultSet('_TestSelection').elements;
				var selCdt = cdtSet.item(1);

				var deCdtA = _TestDE.createEntity();
				deCdtA.setQualifiedAttribute('customAttributes.string', 'cdtA');
				var deCdtB = _TestDE.createEntity();
				deCdtB.setQualifiedAttribute('customAttributes.string', 'cdtB');

				var source = _TestProjType.CreateEntity();
				source.ID = _TestProjType.getID();
				
				source.setQualifiedAttribute('customAttributes.testDE.customAttributes.entityPerson', personA);
				source.setQualifiedAttribute('customAttributes.testDE.customAttributes.entitySelection', selCdt);
				source.setQualifiedAttribute('customAttributes.testDE.customAttributes.entityDocument', docA);
				source.setQualifiedAttribute('customAttributes.testDE.customAttributes.setTestDE', _TestDE.createEntitySet());
				source.customAttributes.testDE.customAttributes.setTestDE.addElement(deCdtA);
				source.customAttributes.testDE.customAttributes.setTestDE.addElement(deCdtB);

				Assert.True(source.metaCloneId == null);
				var target = EntityCloner.quickClone(source);

				var sourceCdtA = source.customAttributes.testDE.customAttributes.setTestDE.query('customAttributes.string =\'cdtA\'').elements.item(1);
				var targetCdtA = target.customAttributes.testDE.customAttributes.setTestDE.query('customAttributes.string =\'cdtA\'').elements.item(1);
				var sourceCdtB = source.customAttributes.testDE.customAttributes.setTestDE.query('customAttributes.string =\'cdtB\'').elements.item(1);
				var targetCdtB = target.customAttributes.testDE.customAttributes.setTestDE.query('customAttributes.string =\'cdtB\'').elements.item(1);

				Assert.True(source != target);
				Assert.True(source.metaCloneId != null);
				Assert.True(target.metaCloneId != null);
				Assert.True(source.metaCloneId == target.metaCloneId);
				Assert.True(source.customAttributes.testDE != target.customAttributes.testDE);
				Assert.True(source.customAttributes.testDE.metaCloneId != null);
				Assert.True(target.customAttributes.testDE.metaCloneId != null);
				Assert.True(source.customAttributes.testDE.metaCloneId == target.customAttributes.testDE.metaCloneId);
				Assert.True(source.customAttributes.testDE.customAttributes.entityDocument != 
							target.customAttributes.testDE.customAttributes.entityDocument);
				Assert.True(source.customAttributes.testDE.customAttributes.entityDocument.metaCloneId != null);
				Assert.True(target.customAttributes.testDE.customAttributes.entityDocument.metaCloneId != null);
				Assert.True(source.customAttributes.testDE.customAttributes.entityDocument.metaCloneId == 
							target.customAttributes.testDE.customAttributes.entityDocument.metaCloneId);
				Assert.True(sourceCdtA != targetCdtA);
				Assert.True(sourceCdtA.metaCloneId != null);
				Assert.True(targetCdtA.metaCloneId != null);
				Assert.True(sourceCdtA.metaCloneId == targetCdtA.metaCloneId);
				Assert.True(sourceCdtB != targetCdtB);
				Assert.True(sourceCdtB.metaCloneId != null);
				Assert.True(targetCdtB.metaCloneId != null);
				Assert.True(sourceCdtB.metaCloneId == targetCdtB.metaCloneId);

				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}

		[Test(Description = "SetClonableProjectTypes")]
		public void SetClonableProjectTypes()
		{
			Store.LoginAsUser(Users.Admin);
			var cmd = new CommandWindow();
			const String script = @"
				var projSet = ApplicationEntity.getResultSet('_VerySmallProjType').elements;
				var projA = projSet.item(1);

				var source = ApplicationEntity.getResultSet('_TestProjType').query('ID like \'%0001\'').elements.item(1);
				source.customAttributes.testDE.customAttributes.entityProject = projA;

				var ec = EntityCloner.createRequest(source, source, 'xxx');
				ec.setClonableProjectTypes('_VerySmallProjType');
				ec.startRequestNow();
				var target = ec.rootEntity;

				Assert.True(source.customAttributes.testDE.customAttributes.entityProject !=
							target.customAttributes.testDE.customAttributes.entityProject);
				Assert.True(source.customAttributes.testDE.customAttributes.entityProject.name ==
							target.customAttributes.testDE.customAttributes.entityProject.name);
				
				sch.setAbort();
				? 'done';";
			Assert.AreEqual("done", cmd.Run(script));
		}
	}
}