﻿using System;
using System.Collections.Generic;
using System.Threading;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.BasePages.ProjectTypeCenter;
using CCWebUIAuto.Pages.Components;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using ClickPortal.UI.Tests.Automation.Pages.TestStore.TestProjType;
using ClickPortal.UI.Tests.Automation.TestStore;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
	class SiteSearchComponentTests : BaseTest
	{
		[Test(Description = "Define Indexed Attributes")]
		public void DefineIndexedAttributes()
		{
			const String property = "testDE.string";
			
			// add an attribute but don't click OK
			Store.LoginAsUser(Users.Admin);
			var searchConfigPage = new SearchIndexingTab("_TestProjType");
			searchConfigPage.NavigateTo();
			searchConfigPage.ChkEnableSearchIndexing.Checked = true;
			searchConfigPage.AddProperty(property);
			Wait.Until(d => searchConfigPage.SelIndexedAttributes.Contains(property));

			// verify the change didn't persist because we didn't click OK
			var switchOffPage = new Projects();
			switchOffPage.NavigateTo();
			searchConfigPage.NavigateTo();
			Assert.False(searchConfigPage.SelIndexedAttributes.Contains(property));

			// add an attribute and click OK
			searchConfigPage.ChkEnableSearchIndexing.Checked = true;
			Wait.Until(d => searchConfigPage.BtnAddAttribute.Enabled);
			searchConfigPage.AddProperty(property);
			Wait.Until(d => searchConfigPage.SelIndexedAttributes.Contains(property));
			searchConfigPage.BtnOk.Click();

			// verify the change persisted
			switchOffPage.NavigateTo();
			searchConfigPage.NavigateTo();
			Wait.Until(d => searchConfigPage.SelIndexedAttributes.Contains(property));

			// cleanup
			searchConfigPage.SelIndexedAttributes.SelectOption(property);
			searchConfigPage.BtnRemoveAttribute.Click();
			Wait.Until(d => !searchConfigPage.SelIndexedAttributes.Contains(property));
			searchConfigPage.ChkEnableSearchIndexing.Checked = false;
			searchConfigPage.BtnOk.Click();
		}

		[Test(Description="Generate a Search Proxy Doc for a project instance")]
		public void GenerateIndexProxyDoc()
		{
			const String projType = "_TestProjType";
			var id = DataGen.String(8);
			String[] attrsToIndex = { "name" };

			Store.LoginAsUser(Users.Admin);
			configureProjectTypeSearchSettings("_TestProjType", attrsToIndex);
			createTestProjTypeInstance(id);
			indexInstance(projType, id);

			try {
				var searchConfigPage = new SearchIndexingTab(projType);
				searchConfigPage.NavigateTo();
				var mainWindow = searchConfigPage.GetTitle();
				searchConfigPage.TxtSearchProxyFor.Value = id;
				searchConfigPage.BtnViewSearchProxyDoc.Click();
				Wait.Until(d => d.WindowHandles.Count == 2);
				SwitchToMostRecentWindow();
				var contents = CCWebUIAuto.Web.Driver.PageSource;
				Assert.IsTrue(contents.Contains(id));
				PopUpWindow.SwitchTo(mainWindow);
			} finally {
				clearProjectTypeSearchSettings(projType, attrsToIndex);
			}
		}

		[Test(Description="Can add the component to a page (non-template)")]
		public void AddComponentToPage()
		{
			var homePage = new Home();
			Store.LoginAsUser(Users.Admin);
			homePage.NavigateTo();
			createAndConfigureSiteSearchComponent(homePage, 2);
			var component = new SiteSearchComponent("Site Search"); // default display name
			Wait.Until(d => component.DivComponentArea.Exists);
			removeSearchComponent(homePage, 0, 2);
			Wait.Until(d => !component.DivComponentArea.Exists);
		}

		public void indexInstance(String type, String id)
		{
			var searchConfigPage = new SearchIndexingTab(type);
			searchConfigPage.NavigateTo();
			searchConfigPage.TxtSearchProxyFor.Value = id;
			searchConfigPage.BtnUpdateSearchProxyDoc.Click();
			AcceptAlert("search proxy document updated successfully");
		}

		public void createTestProjTypeInstance(String id)
		{
			var projCreatorPage = new ProjectCreator();
			projCreatorPage.NavigateTo();
			Thread.Sleep(1000);
			projCreatorPage.NavigateTo(); // avoid success message
			projCreatorPage.SelProjectType.SelectOption("TestProjType");
			projCreatorPage.BtnCreate.Click();
			var sfPage1 = new SmartFormPageBaseAttributes();
			sfPage1.TxtId.Value = id;
			sfPage1.TxtName.Value = id;
			sfPage1.BtnContinue.Click();
		}

		public void configureProjectTypeSearchSettings(String type, IEnumerable<String> IndexedAttributes)
		{
			var searchConfigPage = new SearchIndexingTab(type);
			searchConfigPage.NavigateTo();
			searchConfigPage.ChkEnableSearchIndexing.Checked = true;
			foreach (var attr in IndexedAttributes) {
				var attrName = attr; // new instance for closure
				searchConfigPage.AddProperty(attr);
				Wait.Until(d => searchConfigPage.SelIndexedAttributes.Contains(attrName));
			}
			searchConfigPage.BtnOk.Click();
		}

		public void createAndConfigureSiteSearchComponent(LayoutInitialBasedPage page, Int32 areaId, Action<RoomComponentPropertiesPopup> action = null)
		{
			var editLayoutPopup = new EditPageLayoutPopup();
			page.NavigateTo();
			page.BtnPageComponents.Click();
			editLayoutPopup.SwitchTo();
			editLayoutPopup.AddComponent(ComponentType.ContentSearch, areaId);
			editLayoutPopup.BtnOk.Click();
			editLayoutPopup.SwitchBackToParent();
			var component = new SiteSearchComponent("Site Search"); // default display name
			Wait.Until(d => component.DivComponentArea.Exists);
			if (action != null) {
				component.RunOnPropertiesPopup<SiteSearchComponentPropertiesPopup>(action);
			}
		}

		public void removeSearchComponent(LayoutInitialBasedPage page, Int32 componentIdx, Int32 areaId)
		{
			var editLayoutPopup = new EditPageLayoutPopup();
			page.NavigateTo();
			page.BtnPageComponents.Click();
			editLayoutPopup.SwitchTo();
			editLayoutPopup.RemoveComponent(componentIdx, areaId);
			editLayoutPopup.BtnOk.Click();
			editLayoutPopup.SwitchBackToParent();
		}

		public void clearProjectTypeSearchSettings(String type, IEnumerable<String> AttributesToRemove)
		{
			var searchConfigPage = new SearchIndexingTab(type);
			searchConfigPage.NavigateTo();
			Wait.Until(d => searchConfigPage.ChkEnableSearchIndexing.Checked);

			foreach (var attrName in AttributesToRemove) {
				var attr = attrName; // new instance for closure
				searchConfigPage.SelIndexedAttributes.SelectOption(attr);
				searchConfigPage.BtnRemoveAttribute.Click();
				Wait.Until(d => !searchConfigPage.SelIndexedAttributes.Contains(attr));
			}
			
			searchConfigPage.ChkEnableSearchIndexing.Checked = false;
			searchConfigPage.BtnOk.Click();
		}

		[Test(Description="Simple Match")]
		public void SimpleMatch()
		{
			var id = DataGen.String(8);
			const String ProjType = "_TestProjType";
			String[] attrsToIndex = { "name" };
			var homePage = new Home();

			Store.LoginAsUser(Users.Admin);
			configureProjectTypeSearchSettings(ProjType, attrsToIndex);
			createTestProjTypeInstance(id);
			indexInstance(ProjType, id);
			createAndConfigureSiteSearchComponent(homePage, 2, properties => {
				var popup = (SiteSearchComponentPropertiesPopup) properties;
				popup.SelResultsStyle.SelectOption("Inline");
			});
			
			try {
				homePage.NavigateTo();
				var component = new SiteSearchComponent("Site Search");
				component.TxtSearch.Value = id;
				component.BtnGo.Click();
				Wait.Until(d => Link(By.LinkText(id + ": " + id)).Exists);
			} finally {
				removeSearchComponent(homePage, 0, 2);
				clearProjectTypeSearchSettings(ProjType, attrsToIndex);
			}
		}

		[Test(Description = "Match only when all words present")]
		public void MatchOnlyWhenAllWordsPresent()
		{
			var idA = DataGen.String(8);
			var idB = DataGen.String(8);
			var id = idA + " " + idB;
			const String ProjType = "_TestProjType";
			String[] attrsToIndex = { "name" };
			var homePage = new Home();

			Store.LoginAsUser(Users.Admin);
			configureProjectTypeSearchSettings(ProjType, attrsToIndex);
			createTestProjTypeInstance(id);
			indexInstance(ProjType, id);
			createAndConfigureSiteSearchComponent(homePage, 2, properties => {
				var popup = (SiteSearchComponentPropertiesPopup) properties;
				popup.SelResultsStyle.SelectOption("Inline");
			});

			try {
				homePage.NavigateTo();
				var component = new SiteSearchComponent("Site Search");
				component.TxtSearch.Value = id;
				component.BtnGo.Click();
				Wait.Until(d => Link(By.LinkText(id + ": " + id)).Exists);

				component.TxtSearch.Value = idA + " " + DataGen.String(8);
				component.BtnGo.Click();
				Assert.False(Link(By.LinkText(id + ": " + id)).Exists);

				component.TxtSearch.Value = idB + " " + DataGen.String(8);
				component.BtnGo.Click();
				Assert.False(Link(By.LinkText(id + ": " + id)).Exists);
			} finally {
				removeSearchComponent(homePage, 0, 2);
				clearProjectTypeSearchSettings(ProjType, attrsToIndex);
			}
		}

		[Test(Description = "Or operator works")]
		public void OrOperatorWorks()
		{
			var idA = DataGen.String(8);
			var idB = DataGen.String(8);
			var id = idA + " " + idB;
			const String ProjType = "_TestProjType";
			String[] attrsToIndex = { "name" };
			var homePage = new Home();

			Store.LoginAsUser(Users.Admin);
			configureProjectTypeSearchSettings(ProjType, attrsToIndex);
			createTestProjTypeInstance(id);
			indexInstance(ProjType, id);
			createAndConfigureSiteSearchComponent(homePage, 2, properties => {
				var popup = (SiteSearchComponentPropertiesPopup) properties;
				popup.SelResultsStyle.SelectOption("Inline");
			});

			try {
				homePage.NavigateTo();
				var component = new SiteSearchComponent("Site Search");

				component.TxtSearch.Value = idA + " OR " + DataGen.String(8);
				component.BtnGo.Click();
				Assert.True(Link(By.LinkText(id + ": " + id)).Exists);

				component.TxtSearch.Value = idB + " OR " + DataGen.String(8);
				component.BtnGo.Click();
				Assert.True(Link(By.LinkText(id + ": " + id)).Exists);
			} finally {
				removeSearchComponent(homePage, 0, 2);
				clearProjectTypeSearchSettings(ProjType, attrsToIndex);
			}
		}

		[Test(Description = "And operator works")]
		public void AndOperatorWorks()
		{
			var idX = DataGen.String(4);
			var idA = DataGen.String(8);
			var idB = DataGen.String(8);
			var idC = DataGen.String(8);
			var projIdA = idA + " " + idX + " " + idB;
			var projIdB = idA + " " + idX + " " + idC;
			const String ProjType = "_TestProjType";
			String[] attrsToIndex = { "name" };
			var homePage = new Home();

			Store.LoginAsUser(Users.Admin);
			configureProjectTypeSearchSettings(ProjType, attrsToIndex);
			createTestProjTypeInstance(projIdA);
			createTestProjTypeInstance(projIdB);
			indexInstance(ProjType, projIdA);
			indexInstance(ProjType, projIdB);
			createAndConfigureSiteSearchComponent(homePage, 2, properties => {
				var popup = (SiteSearchComponentPropertiesPopup) properties;
				popup.SelResultsStyle.SelectOption("Inline");
			});

			try {
				homePage.NavigateTo();
				var component = new SiteSearchComponent("Site Search");

				component.TxtSearch.Value = idA + " AND " + idB;
				component.BtnGo.Click();
                Wait.Until(h => Link(By.PartialLinkText(projIdA)).Exists);
				Assert.True(Link(By.LinkText(projIdA + ": " + projIdA)).Exists);
				Assert.False(Link(By.LinkText(projIdB + ": " + projIdB)).Exists);

				component.TxtSearch.Value = idA + " AND " + idC;
				component.BtnGo.Click();
				Assert.False(Link(By.LinkText(projIdA + ": " + projIdA)).Exists);
				Assert.True(Link(By.LinkText(projIdB + ": " + projIdB)).Exists);
			} finally {
				removeSearchComponent(homePage, 0, 2);
				clearProjectTypeSearchSettings(ProjType, attrsToIndex);
			}
		}

		[Test(Description = "Not operator works")]
		public void NotperatorWorks()
		{
			var idX = DataGen.String(4);
			var idA = DataGen.String(8);
			var idB = DataGen.String(8);
			var idC = DataGen.String(8);
			var projIdA = idA + " " + idX + " " + idB;
			var projIdB = idA + " " + idX + " " + idC;
			const String ProjType = "_TestProjType";
			String[] attrsToIndex = { "name" };
			var homePage = new Home();

			Store.LoginAsUser(Users.Admin);
			configureProjectTypeSearchSettings(ProjType, attrsToIndex);
			createTestProjTypeInstance(projIdA);
			createTestProjTypeInstance(projIdB);
			indexInstance(ProjType, projIdA);
			indexInstance(ProjType, projIdB);
			createAndConfigureSiteSearchComponent(homePage, 2, properties => {
				var popup = (SiteSearchComponentPropertiesPopup) properties;
				popup.SelResultsStyle.SelectOption("Inline");
			});

			try {
				homePage.NavigateTo();
				var component = new SiteSearchComponent("Site Search");

				component.TxtSearch.Value = idA + " NOT " + idC;
				component.BtnGo.Click();
				Assert.True(Link(By.LinkText(projIdA + ": " + projIdA)).Exists);
				Assert.False(Link(By.LinkText(projIdB + ": " + projIdB)).Exists);

				component.TxtSearch.Value = idA + " NOT " + idB;
				component.BtnGo.Click();
				Assert.False(Link(By.LinkText(projIdA + ": " + projIdA)).Exists);
				Assert.True(Link(By.LinkText(projIdB + ": " + projIdB)).Exists);
			} finally {
				removeSearchComponent(homePage, 0, 2);
				clearProjectTypeSearchSettings(ProjType, attrsToIndex);
			}
		}

		[Test(Description = "Does not match partial words")]
		public void DoesNotMatchPartialWords()
		{
			var idA = DataGen.String(8);
			var idB = DataGen.String(8);
			var id = idA + idB;
			const String ProjType = "_TestProjType";
			String[] attrsToIndex = { "name" };
			var homePage = new Home();

			Store.LoginAsUser(Users.Admin);
			configureProjectTypeSearchSettings(ProjType, attrsToIndex);
			createTestProjTypeInstance(id);
			indexInstance(ProjType, id);
			createAndConfigureSiteSearchComponent(homePage, 2, properties => {
				var popup = (SiteSearchComponentPropertiesPopup) properties;
				popup.SelResultsStyle.SelectOption("Inline");
			});

			try {
				homePage.NavigateTo();
				var component = new SiteSearchComponent("Site Search");
				component.TxtSearch.Value = id;
				component.BtnGo.Click();
				Wait.Until(d => Link(By.LinkText(id + ": " + id)).Exists);

				component.TxtSearch.Value = idA;
				component.BtnGo.Click();
				Assert.False(Link(By.LinkText(id + ": " + id)).Exists);

				component.TxtSearch.Value = idB;
				component.BtnGo.Click();
				Assert.False(Link(By.LinkText(id + ": " + id)).Exists);
			} finally {
				removeSearchComponent(homePage, 0, 2);
				clearProjectTypeSearchSettings(ProjType, attrsToIndex);
			}
		}

		[Test(Description="Auto-disable if missing attribute exception occurs while indexing a document")]
		public void AutoDisableDueToMissingAttribute()
		{
			const String projType = "_TestProjType";
			var testCustomAttrName = DataGen.String(8);
			String[] attrsToIndex = { "name", testCustomAttrName };
			var propertiesTab = new PropertiesTab(projType);
			var searchConfigPage = new SearchIndexingTab(projType);
			
			Store.LoginAsUser(Users.Admin);

			// create an attribute to delete later
			propertiesTab.NavigateTo();
			propertiesTab.BtnNewCustomAttribute.AsyncClick();
			PopUpWindow.SwitchTo("Field Definition");
			var createPopup = new CustomAttributeDefinitionPopup();
			createPopup.SelDataType.SelectOption("Boolean");
			createPopup.TxtDisplayName.Value = testCustomAttrName;
			createPopup.TxtInternalName.Value = testCustomAttrName;
			createPopup.BtnOk.Click();
			PopUpWindow.SwitchTo("Project Type (TestProjType)");

			// add the new attribute to the search index
			configureProjectTypeSearchSettings(projType, attrsToIndex);
			
			// remove the attribute while it is configured to be indexed
			propertiesTab.NavigateTo();
			propertiesTab.CheckRow(testCustomAttrName);
			propertiesTab.BtnDeleteCustomAttribute.Click();

			try {
				// attempt to update the index for a page, verify that 
				// indexing is automatically disabled 
				indexInstance(projType, "X-00000001");
				searchConfigPage.NavigateTo();
				Wait.Until(d => searchConfigPage.ChkEnableSearchIndexing.Checked == false);
			} finally {
				// turn indexing back on so we can clean up
				searchConfigPage.NavigateTo();
				searchConfigPage.ChkEnableSearchIndexing.Checked = true;
				searchConfigPage.BtnOk.Click();
				Thread.Sleep(1000);
				searchConfigPage.NavigateTo();
				clearProjectTypeSearchSettings(projType, attrsToIndex);
			}
		}

		[Test(Description = "Handle Null in Search Attribute Path")]
		public void HandleNullInSearchAttributePath()
		{
			var id = DataGen.String(8);
			const String ProjType = "_TestProjType";
			String[] attrsToIndex = { "projects.name", "name" };
			var homePage = new Home();
			var cmd = new CommandWindow();

			Store.LoginAsUser(Users.Admin);
			configureProjectTypeSearchSettings(ProjType, attrsToIndex);
			createTestProjTypeInstance(id);
			cmd.Run("var proj = getResultSet('_TestProjType').query(\"ID='" + id + "'\").elements.item(1);\n" +
					"proj.projects = null;\n");
			indexInstance(ProjType, id);
			createAndConfigureSiteSearchComponent(homePage, 2, properties => {
				var popup = (SiteSearchComponentPropertiesPopup) properties;
				popup.SelResultsStyle.SelectOption("Inline");
			});

			try {
				homePage.NavigateTo();
				var component = new SiteSearchComponent("Site Search");

				component.TxtSearch.Value = id;
				component.BtnGo.Click();
				Wait.Until(d => Link(By.LinkText(id + ": " + id)).Exists);
			} finally {
				removeSearchComponent(homePage, 0, 2);
				clearProjectTypeSearchSettings(ProjType, attrsToIndex);
			}
		}

		[Test(Description = "Handle Null in Search Attribute Path")]
		public void HandleNullOwnerForSearchResult()
		{
			var id = DataGen.String(8);
			const String ProjType = "_TestProjType";
			String[] attrsToIndex = { "name" };
			var homePage = new Home();
			var cmd = new CommandWindow();

			Store.LoginAsUser(Users.Admin);
			configureProjectTypeSearchSettings(ProjType, attrsToIndex);
			createTestProjTypeInstance(id);
			cmd.Run("var proj = getResultSet('_TestProjType').query(\"ID='" + id + "'\").elements.item(1);\n" +
					"proj.owner = null;\n");
			indexInstance(ProjType, id);
			createAndConfigureSiteSearchComponent(homePage, 2, properties => {
				var popup = (SiteSearchComponentPropertiesPopup) properties;
				popup.SelResultsStyle.SelectOption("Inline");
			});

			try {
				homePage.NavigateTo();
				var component = new SiteSearchComponent("Site Search");

				component.TxtSearch.Value = id;
				component.BtnGo.Click();
				Wait.Until(d => Link(By.LinkText(id + ": " + id)).Exists);
			} finally {
				removeSearchComponent(homePage, 0, 2);
				clearProjectTypeSearchSettings(ProjType, attrsToIndex);
			}
		}
	}
}
