﻿using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.BasePages.ProjectTypeCenter;
using CCWebUIAuto.Pages.Components;
using CCWebUIAuto.PrimitiveElements;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using ClickPortal.UI.Tests.Automation.Pages.TestStore.TestProjType;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
	[TestFixture]
	class ReviewerNotesComponentTests : BaseTest
	{
		[Test(Description="Change the name of the reviewer notes component")]
		public void ChangeName()
		{
			var testName = DataGen.String(8);
			var templatesTab = new TemplatesTab("_TestProjType");
			var layoutPopup = new EditPageLayoutPopup();
			var workspace = new DefaultTemplate("X-00000001");

			Store.LoginAsUser(Users.Admin);
			templatesTab.NavigateTo();
			templatesTab.OpenTemplateComponentsPopup("Default");
			layoutPopup.SwitchTo();
			layoutPopup.RunOnPropertiesPopup<ReviewerNotesComponentPropertiesPopup>(0, 2, 1, properties => {
				var popup = (ReviewerNotesComponentPropertiesPopup) properties;
				popup.TxtTitle.Value = testName;
			});
			layoutPopup.BtnOk.Click();
			layoutPopup.SwitchBackToParent();

			workspace.NavigateTo();
			workspace.LnkReviewerNotesTab.Click();
			Wait.Until(d => new Container(By.XPath("//span[text()='" + testName + "']")).Exists);

			templatesTab.NavigateTo();
			templatesTab.OpenTemplateComponentsPopup("Default");
			layoutPopup.SwitchTo();
			layoutPopup.RunOnPropertiesPopup<ReviewerNotesComponentPropertiesPopup>(0, 2, 1, properties => {
				var popup = (ReviewerNotesComponentPropertiesPopup) properties;
				popup.TxtTitle.Value = "Reviewer Notes";
			});
			layoutPopup.BtnOk.Click();
			layoutPopup.SwitchBackToParent();
		}

		[Test(Description="4.3.2 Edit Inline, 4.3.3 Response Inline")]
		public void EditInline()
		{
			var name = "TestProj_" + DataGen.String(4);
			var noteTextA = DataGen.String(8);
			var noteTextB = DataGen.String(8);
			var responseText = DataGen.String(8);
			var workspace = new DefaultTemplate(name);
			var component = workspace.ReviewerNotes;
			var projCreatorPage = new ProjectCreator();
			var sfPage1 = new SmartFormPageBaseAttributes();
			var sfPage2 = new SmartFormPageTestDeAttributes();

			Store.LoginAsUser(Users.Admin);
			projCreatorPage.NavigateTo();
			projCreatorPage.NavigateTo(); // clear created successfully message
			projCreatorPage.SelProjectType.SelectOption("TestProjType");
			projCreatorPage.BtnCreate.Click();
			sfPage1.TxtId.Value = name;
			sfPage1.TxtName.Value = name;
			sfPage1.BtnContinue.Click();
			sfPage2.AddReviewerNote("Response Required Note A", noteTextA);

			workspace.NavigateTo();
			workspace.LnkReviewerNotesTab.Click();
			var divExpectedTextA = new Container(By.XPath("//div[contains(text(), '" + noteTextA + "')]"));
			Wait.Until(d => divExpectedTextA.Displayed);
			component.EditNoteInline(0, "Response Required Note B", noteTextB);
			var divExpectedTextB = new Container(By.XPath("//div[contains(text(), '" + noteTextB + "')]"));
			Wait.Until(d => divExpectedTextB.Displayed);

			Store.Logout();
			Store.LoginAsUser(Users.TestRespond);
			workspace.NavigateTo();
			workspace.LnkReviewerNotesTab.Click();
			Wait.Until(d => divExpectedTextB.Displayed);
			component.RespondInline(0, "B Response", responseText);
			var divExpectedResponse = new Container(By.XPath("//div[contains(text(), '" + responseText + "')]"));
			Wait.Until(d => divExpectedResponse.Displayed);
		}

		[Test(Description = "4.3.4 Edit in Popup")]
		public void EditInPopup()
		{
			var name = "TestProj_" + DataGen.String(4);
			var noteTextA = DataGen.String(8);
			var noteTextB = DataGen.String(8);
			var responseText = DataGen.String(8);
			var workspace = new DefaultTemplate(name);
			var component = workspace.ReviewerNotes;
			var projCreatorPage = new ProjectCreator();
			var sfPage1 = new SmartFormPageBaseAttributes();
			var sfPage2 = new SmartFormPageTestDeAttributes();

			Store.LoginAsUser(Users.Admin);
			projCreatorPage.NavigateTo();
			projCreatorPage.NavigateTo(); // clear created successfully message
			projCreatorPage.SelProjectType.SelectOption("TestProjType");
			projCreatorPage.BtnCreate.Click();
			sfPage1.TxtId.Value = name;
			sfPage1.TxtName.Value = name;
			sfPage1.BtnContinue.Click();
			sfPage2.AddReviewerNote("Response Required Note A", noteTextA);

			workspace.NavigateTo();
			workspace.LnkReviewerNotesTab.Click();
			var divExpectedTextA = new Container(By.XPath("//div[contains(text(), '" + noteTextA + "')]"));
			Wait.Until(d => divExpectedTextA.Displayed);
			component.EditNoteInPopup(0, "Response Required Note B", noteTextB);
			var divExpectedTextB = new Container(By.XPath("//div[contains(text(), '" + noteTextB + "')]"));
			Wait.Until(d => divExpectedTextB.Displayed);

			Store.Logout();
			Store.LoginAsUser(Users.TestRespond);
			workspace.NavigateTo();
			workspace.LnkReviewerNotesTab.Click();
			Wait.Until(d => divExpectedTextB.Displayed);
			component.RespondInPopup(0, "B Response", responseText);
			var divExpectedResponse = new Container(By.XPath("//div[contains(text(), '" + responseText + "')]"));
			Wait.Until(d => divExpectedResponse.Displayed);
		}

		[Test(Description="4.3.9 Works correctly if placed on the workspace of a project with # or % in the ID")]
		public void WorksOnWorkspaceWithSpecialCharacterInId()
		{
			var workspace = new DefaultTemplate("X-00000013");
			var sfPage1 = new SmartFormPageBaseAttributes();
			var noteText = DataGen.String(8);
			var divExpectedTextA = new Container(By.XPath("//div[contains(text(), '" + noteText + "')]"));

			Store.LoginAsUser(Users.Admin);
			workspace.NavigateTo();
			workspace.LnkEditProject.Click();
			sfPage1.AddReviewerNote("Basic Note", noteText);
			sfPage1.LnkSave.Click();
			sfPage1.LnkExit.Click();
			workspace.LnkReviewerNotesTab.Click();
			Wait.Until(d => divExpectedTextA.Displayed);
		}
	}
}
