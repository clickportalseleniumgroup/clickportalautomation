﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.BasePages.ProjectTypeCenter;
using CCWebUIAuto.PrimitiveElements;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
	class ConditionalDisplayRules : BaseTest
	{
		/// <summary>
		/// Utility function that creates a new rule. Used by the tests in this class
		/// </summary>
		private void CreateRule(String attrToSelect)
		{
			//Switch to Rules admin page
			var rulesAdminPage = new RulesAdminPagecs();
			rulesAdminPage.SwitchTo();
			rulesAdminPage.BtnNew.Click();

			//Create a new rule
			var ruleEditorPage = new RulesEditorPage();
			ruleEditorPage.SwitchTo();
			var ruleName = "Rule_" + DataGen.AllLowerCaseString(4);
			ruleEditorPage.TxtName.Value = ruleName;
			ruleEditorPage.LnkLogic.Click();

			//Add the rule logic
			var ruleLogicPage = new RuleLogicPage();
			ruleLogicPage.SwitchTo();
			ruleLogicPage.ImgPlus.Click();
			if (attrToSelect == "Name")
			{
				ruleLogicPage.CntName.Click();
			}
			else
			{
				ruleLogicPage.CntID.Click();
			}
			ruleLogicPage.BtnAddCriteria.Click();
			var value = DataGen.AllLowerCaseString(4);
			ruleLogicPage.TxtFilterValue.Value = value;
			ruleLogicPage.BtnAddCriterion.Click();
			Wait.Until(d => ruleLogicPage.SelCriteria.Contains(attrToSelect + " like " + value));
			ruleLogicPage.BtnSave.Click();
			ruleLogicPage.SwitchBackToParent(WaitForPopupToClose.Yes);
			
			//Wait until the parent page refreshes
			Wait.Until(d => (new CCElement(By.XPath("//div[@id='displayRuleLogicText']//td[text()='" + attrToSelect + " like " + value + "']"))).Exists);
			ruleEditorPage.BtnOk.Click();
			ruleEditorPage.SwitchBackToParent(WaitForPopupToClose.Yes);
			Wait.Until(d => Link(By.XPath("//table[@id='webrRSV__ID_0']//a[text()='" + ruleName + "']")).Exists);

			rulesAdminPage.BtnOk.Click();
			rulesAdminPage.SwitchBackToParent(WaitForPopupToClose.Yes);
		}

		[Test(Description = "Create a new rule from the project type views tab")]
		public void CreateRuleFromViewsTab()
		{
			//Navigate to the project type views tab
			var viewsTab = new ViewsTab("_TestProjType");
			Store.LoginAsUser(Users.Admin);
			viewsTab.NavigateTo();

			//Click the conditional display rules button
			viewsTab.LnkConditionalDisplay.Click();

			//Create a new rule
			CreateRule("Name");
			
			Wait.Until(d => (viewsTab.LnkConditionalDisplay.BaseElement.GetAttributeValue("class") == "iconEnabled"));
		}

		[Test(Description = "Create a new rule from the activities tab")]
		public void CreateRuleFromActivitiesQuickLinks()
		{
			//Navigate to the project type views tab
			var activitiesTab = new ActivitiesTab("_TestProjType");
			Store.LoginAsUser(Users.Admin);
			activitiesTab.NavigateTo();

			//Click the conditional display rules button
			activitiesTab.LnkConditionalDisplay.Click();

			//Create a new rule
			CreateRule("Name");

			Wait.Until(d => (activitiesTab.LnkConditionalDisplay.BaseElement.GetAttributeValue("class") == "iconEnabled"));
		}

		[Test(Description = "Create a new rule from the data type center views tab")]
		public void CreateRuleFromCdtViewsTab()
		{
			//Navigate to the CDT views tab
			var viewsTab = new CCWebUIAuto.Pages.BasePages.DataTypeCenter.ViewsTab("_TestSelection");
			Store.LoginAsUser(Users.Admin);
			viewsTab.NavigateTo();

			//Click the conditional display rules button
			viewsTab.LnkConditionalDisplay.Click();

			//Create a new rule
			CreateRule("ID");

			Wait.Until(d => (viewsTab.LnkConditionalDisplay.BaseElement.GetAttributeValue("class") == "iconEnabled"));
		}
	}
}
