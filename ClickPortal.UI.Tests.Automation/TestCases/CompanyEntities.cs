﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
    public class CompanyEntities
    {

    }
}

//_include("standard.sah"); 

//_click(_link("Login"));
//_setValue(_textbox("username"), "beau");
//_setValue(_password("password"), "1234");
//_click(_submit("Login"));
//_click(_link("Projects[0]"));
//_wait(1000);

//reportProgress("2.4.1 Create");
//// verify the test doesn't already exist
//_click(_link("Fast Find"));
//_setSelected(_select("selSearchType"), "Organizations");
//_setValue(_textbox("txtCompanyName"), "testCompany2.4");
//_click(_button("Find Now"));
//_wait(1000);
//_assertNotExists(_link("testCompany2.4"));

//// create the test entity
//_click(_link("Organization"));
//_popup("New Organization")._setValue(_textbox("Company.name"), "testCompany2.4");
//_popup("New Organization")._click(_button("OK"));
//_wait(1000);

//// verify Fast Find locates it
//_click(_link("Fast Find"));
//_setSelected(_select("selSearchType"), "Organizations");
//_setValue(_textbox("txtCompanyName"), "testCompany2.4");
//_click(_button("Find Now"));
//_wait(1000);
//_assertExists(_link("testCompany2.4"));

//reportProgress("2.4.2 Delete -- TEST MANUALLY");
///*
//// this code should work, but the click on the delete button never happens?  I have no explanation
//_click(_image("tool_delete.gif"));
//_wait(1000);
//_expectConfirm("You are deleting an Organization with 0 employee(s) and 0 project(s).  Are you sure that you want to permanently delete testCompany2.4?", true);
//_expectPrompt("Please confirm the deletion by entering the value \"testCompany2.4\" below (without the quotes).", "testCompany2.4")
//_wait(1000);
//_click(_link("Projects[0]"));
//_wait(1000);
//_click(_link("Fast Find"));
//_setSelected(_select("selSearchType"), "Organizations");
//_setValue(_textbox("txtCompanyName"), "testCompany2.4");
//_click(_button("Find Now"));
//_wait(1000);
//_assertNotExists(_link("testCompany2.4"));
//*/

//reportProgress("2.4.3 Create Custom Attributes");
//_click(_link("Site Administration"));
//_click(_link("Project Configuration"));
//_click(_link("Custom Data Types"));
//_click(_link("Organization"));
//_assertNotExists(_link("Test 2_4 Add new attribute"));
//_click(_button("New"));
//_setSelected(_select("dataType"), "Double");
//_setValue(_textbox("caption"), "Test 2_4 Add new attribute");
//_setValue(_textbox("name"), "_test214");
//_click(_button("OK[1]"));
//_wait(1000);
//_assertExists(_link("Test 2_4 Add new attribute"));
//_click(_button("OK"));
//_wait(1000);

//reportProgress("2.4.4 Remove Custom Attributes");
//_click(_checkbox("Delete"));
//_click(_button("Delete"));
//_wait(1000);
//_assertNotExists(_link("Test 2_4 Add new attribute"));

//reportProgress("2.4.5 Create View");
//_click(_link("Views"));
//_assertNotExists(_link("Test 2_4 Create View"));
//_click(_button("New"));
//_wait(2000);
//_popup("Entity View Editor")._setValue(_textbox("EntityView.name"), "Test 2_4 Create View");
//_popup("Entity View Editor")._click(_button("OK"));
//_assertExists(_link("Test 2_4 Create View"));

//reportProgress("2.4.10 Create view validation script");
//_click(_image("Run Custom Script[3]"));
//_wait(1000);
//_popup("Workflow Script Editor")._setValue(_textarea("WorkflowScript.script"), "// company view validation script");
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(1000);
//_click(_image("Run Custom Script[3]"));
//_wait(1000);
//_popup("Workflow Script Editor")._assertEqual("// company view validation script", _textarea("WorkflowScript.script").value);
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(1000);

//reportProgress("2.4.6 Edit View");
//_click(_link("Test 2_4 Create View"));
//_wait(1000);
//_popup("Entity View Editor")._setValue(_textbox("EntityView.name"), "Test 2_4 Create View Edited");
//_popup("Entity View Editor")._click(_button("OK"));
//_wait(1000);
//_assertNotExists(_link("Test 2_4 Create View"));
//_assertExists(_link("Test 2_4 Create View Edited"));

//reportProgress("2.4.8 Delete View");
//_click(_checkbox("webrRSV__SelectedItem_0[3]"));
//_click(_button("Delete"));
//_expectConfirm("Are you sure you want to delete the selected item(s)?", true);
//_wait(1000);
//_assertNotExists(_link("Test 2_4 Create View"));
//_assertNotExists(_link("Test 2_4 Create View Edited"));

//reportProgress("2.4.9 Create/Edit/Delete Choosers");
//_click(_link("Choosers"));
//_assertNotExists(_link("2_4_Chooser"));
//_click(_button("New"));
//_wait(1000);
//_popup("Chooser Editor")._setValue(_textbox("EntityChooser.name"), "2_4_Chooser");
//_popup("Chooser Editor")._click(_button("New"));
//_wait(1000);
//_popup("Chooser Field")._click(_button("..."));
//_wait(1000);
//_popup("Select Property")._click(_span("name"));
//_popup("Select Property")._click(_button("OK"));
//_wait(1000);
//_popup("Chooser Field")._click(_checkbox("EntityChooserField.allowSorting"));
//_popup("Chooser Field")._click(_checkbox("EntityChooserField.allowFiltering"));
//_popup("Chooser Field")._click(_button("OK"));
//_wait(1000);
//_popup("Chooser Editor")._click(_button("New[1]"));
//_wait(1000);
//_popup("Chooser Field")._click(_button("..."));
//_wait(1000);
//_popup("Select Property")._click(_span("name"));
//_popup("Select Property")._click(_button("OK"));
//_wait(1000);
//_popup("Chooser Field")._click(_button("OK"));
//_wait(1000);
//_popup("Chooser Editor")._click(_button("OK"));
//_wait(1000);
//_assertExists(_link("2_4_Chooser"));

//// edit
//_click(_link("2_4_Chooser"));
//_popup("Chooser Editor")._setValue(_textbox("EntityChooser.name"), "2_4_Chooser_Modified");
//_popup("Chooser Editor")._click(_button("OK"));
//_assertNotExists(_link("2_4_Chooser"));
//_assertExists(_link("2_4_Chooser_Modified"));

//// set default
//_click(_checkbox("webrRSV__SelectedItem_0"));
//_click(_button("Set Default"));
//_assertExists(_cell("The default chooser for Organization is now 2_4_Chooser_Modified."));

//// delete
//_click(_checkbox("webrRSV__SelectedItem_0"));
//_click(_button("Delete"));
//_expectConfirm("Are you sure you want to delete the selected item(s)?", true);
//_assertNotExists(_link("2_4_Chooser"));
//_assertNotExists(_link("2_4_Chooser_Modified"));

//reportProgress("2.4.11 Create activity");
//_click(_link("Activities"));
//_assertNotExists(_link("Act_Test_2_4"), "activity link found");
//_click(_button("New"));
//_setValue(_textbox("EntityTypeDisplayName"), "Act_Test_2_4");
//_assertTrue("Act_Test_2_4" == _getValue(_textbox("EntityTypeInternalNameSuffix")), "incorrect internal name suffix");
//_assertTrue("_Company_Act_Test_2_4" == _getValue(_hidden("EntityTypeInternalName")), "incorrect internal name");
//_setValue(_textbox("EntityTypeInternalNameSuffix"), "Act_Test_2_4_SuffixMod");
//_assertTrue("_Company_Act_Test_2_4_SuffixMod" == _getValue(_hidden("EntityTypeInternalName")), "incorrect internal name");
//_assertTrue("Act_Test_2_4" == _getValue(_textbox("EntityTypeDisplayName")), "incorrect display name");
//_setValue(_textbox("nameCompleted"), "Act_Test_2_4 Ran");
//_click(_button("OK"));
//_wait(1000);
//_click(_link("Organization (Activities)"));
//_assertExists(_link("Act_Test_2_4"), "activity link not found");

//reportProgress("2.4.12 Edit activity");
//_click(_link("Act_Test_2_4"));
//_setValue(_textbox("EntityTypeDisplayName"), "Act_Test_2_4_Mod");
//_click(_checkbox("showActivityForm"));
//_click(_button("OK"));
//_click(_link("Organization (Activities)"));
//_assertNotExists(_link("Act_Test_2_4"), "activity link not found");
//_assertExists(_link("Act_Test_2_4_Mod"), "activity link not found");

//reportProgress("2.4.14 Add activity pre-script");
//_click(_image("Run Pre-Script"));
//_wait(1000);
//_popup("Workflow Script Editor")._setValue(_textarea("WorkflowScript.script"), "// company activity pre-script");
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(1000);
//_click(_image("Run Pre-Script"));
//_wait(1000);
//_popup("Workflow Script Editor")._assertEqual("// company activity pre-script", _textarea("WorkflowScript.script").value);
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(1000);

//reportProgress("2.4.15 Add activity notification");
//_click(_image("Notify Users"));
//_wait(1000);
//_popup("Activity Notifications Editor")._assertNotExists(_link("Act_Test_2_4_Mod Activity_Notification"));
//_popup("Activity Notifications Editor")._click(_image("Add"));
//_wait(1000);
//_popup("notificationSettings")._click(_button("Add Property..."));
//_wait(1000);
//_click(_span("owner"));
//_click(_button("OK"));
//_wait(1000);
//_popup("notificationSettings")._setValue(_textbox("EmailNotificationSetting.subject"), "Act_Test_2_4_Mod Activity_Notification");
//_popup("notificationSettings")._click(_button("OK"));
//_wait(1000);
//_popup("Activity Notifications Editor")._assertExists(_link("Act_Test_2_4_Mod Activity_Notification"));
//_popup("Activity Notifications Editor")._click(_button("OK"));

//reportProgress("2.4.16 Add activity post-script");
//_click(_image("Run Post-Script"));
//_wait(1000);
//_popup("Workflow Script Editor")._setValue(_textarea("WorkflowScript.script"), "// company activity post-script");
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(1000);
//_click(_image("Run Post-Script"));
//_wait(1000);
//_popup("Workflow Script Editor")._assertEqual("// company activity post-script", _textarea("WorkflowScript.script").value);
//_popup("Workflow Script Editor")._click(_button("OK"));
//_wait(1000);

//reportProgress("2.4.17 Add activity view");
//_click(_image(/Edit Activity Form.*/));
//_wait(1000);
//_popup("Entity View Editor")._setValue(_textbox("EntityView.name"), "Test 2.3 View");
//_popup("Entity View Editor")._click(_button("OK"));
//_wait(1000);
//_click(_image(/Edit Activity Form.*/));
//_wait(1000);
//_popup("Entity View Editor")._assertEqual("Test 2.3 View", _textbox("EntityView.name").value);
//_wait(1000);
//_popup("Entity View Editor")._click(_button("Cancel"));

//reportProgress("2.4.20 Delete activity");
//_assertExists(_link("Act_Test_2_4_Mod"));
//_click(_checkbox("webrRSV__SelectedItem_0[0]"));
//_click(_button("Delete"));
//_expectConfirm("Are you sure you want to delete the selected item(s)?", true);
//_wait(1000);
//_assertNotExists(_link("Act_Test_2_4"));
//_assertNotExists(_link("Act_Test_2_4_Mod"));

//reportProgress("2.4.7 Checkin/checkout View -- TEST MANUALLY");
//reportProgress("2.4.13 Checkin/checkout activity  -- TEST MANUALLY");
//reportProgress("2.4.18 Renders activity view -- TEST MANUALLY");
//reportProgress("2.4.19 Execute activity -- TEST MANUALLY");
//reportProgress("2.4.21 Configure site policy  -- TEST MANUALLY");
