﻿using System;
using CCWebUIAuto;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.BasePages.ProjectTypeCenter;
using CCWebUIAuto.PrimitiveElements;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using ClickPortal.UI.Tests.Automation.Pages.TestStore.TestProjType;
using ClickPortal.UI.Tests.Automation.TestStore.TestProjType.ActivityPopups;
using NUnit.Framework;
using OpenQA.Selenium;


namespace ClickPortal.UI.Tests.Automation.TestCases
{
    [TestFixture]
    public class ProjectTypeActivity : BaseTest
    {
        public const string TestProjType = "_TestProjType";

        /// <summary>
        /// Enter only display name with pattern 'ValidName', internal name becomes '_ProjType_ValidName'
        /// </summary>
        [Test]
        public void CreateProjectActivityWithSimpleName()
        {
            var myProjectTypeActivitiesTab = new ActivitiesTab("_VerySmallProjType");
            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();
            myProjectTypeActivitiesTab.CreateNewActivity("ValidName");
            Assert.IsTrue(myProjectTypeActivitiesTab.VerifyActivityExists("ValidName"));
            Assert.IsTrue(myProjectTypeActivitiesTab.VerifyActivityInternalName("_VerySmallProjType_ValidName"));
        }

        /// <summary>
        /// Enter only display name with pattern 'Simple Activity With Spaces', internal name becomes '_VerySmallProjType_SimpleActivityWithSpaces'
        /// </summary>
        [Test]
        public void CreateProjectActivtyWithSpaces()
        {
            var myProjectTypeActivitiesTab = new ActivitiesTab("_VerySmallProjType");
            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();
            myProjectTypeActivitiesTab.CreateNewActivity("Simple Activity With Spaces");
            Assert.IsTrue(myProjectTypeActivitiesTab.VerifyActivityExists("Simple Activity With Spaces"));
            Assert.IsTrue(
                myProjectTypeActivitiesTab.VerifyActivityInternalName("_VerySmallProjType_SimpleActivityWithSpaces"));
        }

        /// <summary>
        /// Enter a display name with pattern 'Has Custom Internal Name', customize internal name as '_VerySmallProjType_IsCustomInternalName'
        /// </summary>
        [Test]
        public void CreateProjectActivityWithCustomInternalName()
        {
            var myProjectTypeActivitiesTab = new ActivitiesTab("_VerySmallProjType");
            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();
            myProjectTypeActivitiesTab.CreateNewActivity("Has Custom Internal Name", "IsCustomInternalName");
            Assert.IsTrue(myProjectTypeActivitiesTab.VerifyActivityExists("Has Custom Internal Name"));
            Assert.IsTrue(
                myProjectTypeActivitiesTab.VerifyActivityInternalName("_VerySmallProjType_IsCustomInternalName"));
        }

        [Test]
        public void CreateProjectActivityTypeWithBadDisplayName()
        {
			var myProjectTypeActivitiesTab = new ActivitiesTab(TestProjType);
            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();
			myProjectTypeActivitiesTab.BtnNew.Click();
			var newActivityPage = new NewActivityPage();
			newActivityPage.TxtDisplayName.Value = "badname %*()";
			newActivityPage.BtnOk.Click();
			var alert = Web.Driver.SwitchTo().Alert();
			Assert.IsTrue(alert.Text.Contains("\" \\ / [ ] < > : ; | = , .+ * ? # % ' &"));
			alert.Accept();
        }

        /// <summary>
        /// Customize internal name with invalid characters like '%*()', input is sanitized when building internal name
        /// </summary>
        [Test]
        public void CreateProjectActivityWithBadInternalName()
        {
            var myProjectTypeActivitiesTab = new ActivitiesTab("_VerySmallProjType");
            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();
            myProjectTypeActivitiesTab.CreateNewActivity("CustomizedActivity1",
                "*() CustomInternalName !@% has @#$ been cleansed", true);
            Assert.IsTrue(myProjectTypeActivitiesTab.VerifyActivityExists("CustomizedActivity1"));
            Assert.IsTrue(
                myProjectTypeActivitiesTab.VerifyActivityInternalName(
                    "_VerySmallProjType_CustomInternalNameHasBeenCleansed"));
        }

        /// <summary>
        /// Display name changes are echoed in internal name with automatic 'camel-casing' and removal of white space
        /// </summary>
        [Test]
        [Ignore("Problem reading display name text fields -- selenium / framework bug?")]
        public void SimpleInputCascade()
        {
            var myProjectTypeActivitiesTab = new ActivitiesTab("_VerySmallProjType");
            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();
            myProjectTypeActivitiesTab.BtnNew.Click();
			var newActivityPage = new NewActivityPage();
			newActivityPage.TxtDisplayName.Value = "Simple cascade";
            PopUpWindow.SwitchTo(myProjectTypeActivitiesTab.GetTitle());
			Assert.IsTrue(newActivityPage.VerifyDisplayName("Simple cascade"));
			Assert.IsTrue(newActivityPage.VerifyCustomizeInternalName("SimpleCascade"));
            Assert.IsTrue(newActivityPage.VerifyInternalName("VerySmallProjType_SimpleCascade"));
        }

        /// <summary>
        /// Change the display name and the internal root should change with it
        /// </summary>
        [Test]
        [Ignore("Problem reading display name text fields -- selenium / framework bug?")]
        public void SimpleInputCascade_ChangeDisplayName()
        {
            var myProjectTypeActivitiesTab = new ActivitiesTab("_VerySmallProjType");
            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();
			myProjectTypeActivitiesTab.BtnNew.Click();
			var newActivityPage = new NewActivityPage();
			newActivityPage.TxtDisplayName.Value = "Simple Cascade";
			newActivityPage.TxtDisplayName.Value = "Modified Simple Cascade";
			Assert.IsTrue(newActivityPage.VerifyDisplayName("Modified Simple Cascade"));
			Assert.IsTrue(newActivityPage.VerifyCustomizeInternalName("ModifiedSimpleCascade"));
            Assert.IsTrue(newActivityPage.VerifyInternalName("VerySmallProjType_ModifiedSimpleCascade"));
        }

        /// <summary>
        /// Change the internal root and nothing happens to the display name
        /// </summary>
        [Test]
        [Ignore("Problem reading display name text fields -- selenium / framework bug?")]
        public void SimpleInputCascade_ChangeInternalRoot()
        {
            var myProjectTypeActivitiesTab = new ActivitiesTab("_VerySmallProjType");
            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();
			myProjectTypeActivitiesTab.BtnNew.Click();
			var newActivityPage = new NewActivityPage();
			newActivityPage.TxtDisplayName.Value = "SimpleCascade";
			newActivityPage.TxtInternalName.Value = "SimpleCascadeABC";
			Assert.IsTrue(newActivityPage.VerifyDisplayName("SimpleCascade"));
			Assert.IsTrue(newActivityPage.VerifyCustomizeInternalName("SimpleCascadeABC"));
			Assert.IsTrue(newActivityPage.VerifyInternalName("VerySmallProjType_SimpleCascadeABC"));
        }

        /// <summary>
        /// Change the display name after renaming the internal name, and it no longer modifies the internal root
        /// </summary>
        [Test]
        [Ignore("Problem reading display name text fields -- selenium / framework bug?")]
        public void SimpleInputCascade_ChangeDisplayNameAfterRenamingInternal()
        {
            var myProjectTypeActivitiesTab = new ActivitiesTab("_VerySmallProjType");
            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();
			myProjectTypeActivitiesTab.BtnNew.Click();
			var newActivityPage = new NewActivityPage();
			newActivityPage.TxtDisplayName.Value = "SimpleCascade";
			newActivityPage.TxtInternalName.Value = "SimpleCascadeABC";
			newActivityPage.TxtDisplayName.Value = "XYZ Simple cascade";
			Assert.IsTrue(newActivityPage.VerifyDisplayName("XYZ Simple cascade"));
			Assert.IsTrue(newActivityPage.VerifyCustomizeInternalName("SimpleCascadeABC"));
			Assert.IsTrue(newActivityPage.VerifyInternalName("VerySmallProjType_SimpleCascadeABC"));
        }

        /// <summary>
        /// Attempts to manage activity groups when no activities exist.
        /// </summary>
        [Test]
        public void CreateActivityGroupWhileNoActivitiesExist()
        {
            var testProjType = "ActivityTest" + DataGen.String(4);
            var projectTypeCenter = new ProjectTypeCenterPage();  
            Store.LoginAsUser(Users.Admin);
            projectTypeCenter.NavigateTo();
            projectTypeCenter.CreateNewProjectType(testProjType, "test");
			var subtab = new ActivitiesTabManageGroupsSubTab("_" + testProjType);
			subtab.NavigateTo();
			subtab.VerifyNoActivityWarning();
        }

        /// <summary>
        /// Create a group of activities for a project type
        /// </summary>
        [Test]
        public void CreateActivityGroup()
        {
            var myProjectTypeActivitiesTab = new ActivitiesTab("_VerySmallProjType");
			var group = "TestGroup_" + DataGen.String(4);
			var actA = "TestActivity_" + DataGen.String(4);
			var actB = "TestActivity_" + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();
			myProjectTypeActivitiesTab.CreateNewActivity(actA);
			myProjectTypeActivitiesTab.CreateNewActivity(actB);

			var subTab = new ActivitiesTabManageGroupsSubTab("_VerySmallProjType");
			subTab.NavigateTo();
			subTab.CreateActivityGroup(group, new[] { actA, actB });
			Assert.IsTrue(subTab.VerifyActivityGroupExists(group));
			Assert.IsTrue(new Link(By.LinkText(actA)).Exists);
			Assert.IsTrue(new Link(By.LinkText(actB)).Exists);
        }

        [Test]
        [Ignore("Modifying text field for popup that is opened again does not work for some reason.")]
        public void A2_EditActivityGroupName()
        {  
            var myProjectTypeActivitiesTab = new ActivitiesTab("_VerySmallProjType");

            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();
            myProjectTypeActivitiesTab.CreateNewActivity("TestActivity3");

			var subtab = new ActivitiesTabManageGroupsSubTab(TestProjType);
			subtab.NavigateTo();
			subtab.CreateActivityGroup("GroupTest2", new[] { "TestActivity3" });
			subtab.ModifyActivityGroupName("GroupTest2", "GroupTest2_Modified");
			Assert.IsTrue(subtab.VerifyActivityGroupExists("GroupTest2_Modified"));
        }

        [Test]
        [Description("Create a prescript for activity")]
        public void AddPreScriptForActivity()
        {
            
            var myProjectTypeActivitiesTab = new ActivitiesTab("_TestProjType");

            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();

            myProjectTypeActivitiesTab.AddPreScriptPostProcessing("Comment", "// validation script");
            Assert.IsTrue(myProjectTypeActivitiesTab.VerifyPreScriptExists("Comment", "// validation script"), "Validation script does not exist for activity when expected.");
        }

        [Test]
        [Description("Create a change property for an activity and verify it.")]
        public void AddChangePropertyForActivity()
        {
            var myProjectTypeActivitiesTab = new ActivitiesTab("_TestProjType");
            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();
            myProjectTypeActivitiesTab.AddChangeProperty("Comment","gBoolean", "True");

            // force a refresh, navigate or click on Activities Tab again
            Wait.Until(d => {
				myProjectTypeActivitiesTab.NavigateTo();
				return myProjectTypeActivitiesTab.VerifyProperty("Comment", "gBoolean = True");
			});
        }

        [Test]
        public void AddActivityNotification()
        {
            var myProjectTypeActivitiesTab = new ActivitiesTab("_TestProjType");
            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();
            myProjectTypeActivitiesTab.AddNotification("Comment","owner","Comment_Activity_Notification");
			var imageLink = new Button(By.XPath("//a[text()='Comment']/../../td[7]/table/tbody/tr/td/a[3]/img[1]"));
			imageLink.Click();
			var popup = new ActivityNotificationPopup();
			popup.SwitchTo();
            Assert.IsTrue(popup.VerifyNotificationExists("Comment_Activity_Notification"), "Activity notification 'Comment-Activity_Notification' does not exist.");
			popup.BtnOk.Click();
			popup.SwitchBackToParent();
        }

        [Test]
        public void AddPostScriptForActivity()
        {    
            var myProjectTypeActivitiesTab = new ActivitiesTab("_TestProjType");
            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();
            myProjectTypeActivitiesTab.AddPostScriptPostProcessing("Comment", "// post script");
            Assert.IsTrue(myProjectTypeActivitiesTab.VerifyPostScriptExists("Comment", "// post script"), "'// post script'does not exist for post script activity when expected.");
        }

        [Test]
        public void AddCustomizeActivityScript()
        {
            var myProjectTypeActivitiesTab = new ActivitiesTab("_VerySmallProjType");
            Store.LoginAsUser(Users.Admin);
            myProjectTypeActivitiesTab.NavigateTo();
            myProjectTypeActivitiesTab.AddCustomizeActivityScript("// customize script");
            Assert.IsTrue(myProjectTypeActivitiesTab.VerifyCustomizeActivityDetailsScriptExists("// customize script"), "'// customize script' -- does not exist for customized activity details when expected.");
        }

        [Test(Description="Verify Global Activity Post Processing Script Runs")]
		public void GlobalActivityPostProcessingScriptRuns()
		{
			var errorText = DataGen.AllUpperCaseString(8);
			var activitiesTab = new ActivitiesTab("_TestProjType");
			Store.LoginAsUser(Users.Admin);
			activitiesTab.NavigateTo();
			activitiesTab.AddGlobalPostProcessingScript("throw new Error(-1, '" + errorText + "');");

			const String projId = "X-00000001";
			var workspace = new DefaultTemplate(projId);
			workspace.NavigateTo();
			workspace.LnkExecuteEasyActivityDropdown.Click();
			workspace.EasyActivitiesDropdownLinks.LnkComment.Click();

			var popup = new CommentPopup(projId);
			popup.SwitchTo();
			try {
				popup.TxtComment.Value = "dummy comment";
				popup.BtnOk.Click();
				//Wait.Until(d => new Container(By.XPath("//span[contains(text(),'" + errorText + "')]")).Exists);
				Wait.Until(d => new Container(By.XPath("//span[contains(.,'" + errorText + "')]")).Exists);
			} finally {
				popup.Close();
				popup.SwitchBackToParent();
				activitiesTab.NavigateTo();
				activitiesTab.AddGlobalPostProcessingScript("// empty");
			}
		}
    }
}