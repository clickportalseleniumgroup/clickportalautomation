﻿using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages.ScheduledBackgroundOperations;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
	[TestFixture]
	class ScheduledBackgroundOperations : BaseTest
	{
		[Test(Description = "Verify that we can create and delete a scheduled task")]
		public void CreateAndDeleteScheduledTask()
		{
			var opName = DataGen.String(8);
			var activityNodeName = DataGen.String(8);
			var schedBkgPage = new Main();
			var details = new Details();
			var schedulePopup = new ScheduleDetailsPopup();
			var activityPopup = new ActivityNodePopup();

			Store.LoginAsUser(Users.Admin);
			schedBkgPage.NavigateTo();
			schedBkgPage.BtnNew.Click();

			schedulePopup.SwitchTo();
			schedulePopup.TxtName.Value = opName;
			schedulePopup.TxtUserName.Value = "administrator";
			schedulePopup.TxtStartDate.Value = "05/31/2020 1:30 PM";
			schedulePopup.BtnOk.Click();
			schedulePopup.SwitchBackToParent();

			schedBkgPage.OpenBackgroundOperationDetails(opName);

			details.SelActivityFromThisType.SelectOption("TestProjType");
			details.BtnAdd.Click();
			activityPopup.SwitchTo();
			activityPopup.TxtName.Value = activityNodeName;
			activityPopup.SelCustomSearch.SelectOption("Test - _TestProjType Custom Attribute Primitives");
			activityPopup.SelActivityType.SelectOption("Toggle State");
			activityPopup.BtnOk.Click();
			activityPopup.SwitchBackToParent();

			Wait.Until(d => Link(By.LinkText(activityNodeName)).Exists);
			details.BtnOk.Click();
			schedBkgPage.DeleteBackgroundOperation(opName);
			AcceptAlert();
			Wait.Until(d => !Link(By.LinkText(activityNodeName)).Exists);
		}
	}
}
