﻿using System;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.BasePages.DataTypeCenter;
using CCWebUIAuto.PrimitiveElements;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using ClickPortal.UI.Tests.Automation.Pages.TestStore.TestProjType;
using ClickPortal.UI.Tests.Automation.TestStore.CdtForms;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
	class PersonManagement : BaseTest
	{
		/// <summary>
		/// Utility method that defines the person quick creation view
		/// </summary>
		private void SetPersonQuickCreationView()
		{
			//Navigate to the views tab in the person data type
			var personViewsTab = new ViewsTab("Person");
			Store.LoginAsUser(Users.Admin);
			personViewsTab.NavigateTo();

			//Click on the Edit Standard View Properties button and set the quick creation view
			personViewsTab.BtnEditStandardViewProperties.Click();
			var editViewPropertiesPage = new EditStandardViewProperties();
			editViewPropertiesPage.SwitchTo();
			editViewPropertiesPage.SelQuickCreationView.SelectOption("Quick Create View");
			editViewPropertiesPage.BtnOk.Click();
			editViewPropertiesPage.SwitchBackToParent(WaitForPopupToClose.Yes);
			
			//Wait until parent page refreshes
			const string xpathQuery = "//a[text()='Quick Create View']/../..//img[contains(@src, 'Check.gif')]";
			Wait.Until(d => (new Image(By.XPath(xpathQuery))).Exists);
		}

		[Test(Description = "Selects an existing person from the chooser")]
		public void SelectExistingPerson()
		{
			//Navigate to the workspace
			var workspace = new DefaultTemplate("X-00000001");
			Store.LoginAsUser(Users.Admin);
			workspace.NavigateTo();

			//Open the smartform
			workspace.LnkEditProject.Click();
			var sfPage1 = new SmartFormPageBaseAttributes();
			sfPage1.BtnContinue.Click();
			var sfPage2 = new SmartFormPageTestDeAttributes();


			//Open the person chooser and select a person
			sfPage2.EntPersonEntity.BtnSelect.Click();
			var personChooser = new ChooserPopup("Person");
			personChooser.SwitchTo();
			personChooser.SelectValue("Administrator");
			personChooser.BtnOk.Click();
			personChooser.SwitchBackToParent(WaitForPopupToClose.Yes);

			//Wait until the parent page refreshes
			Wait.Until(d => sfPage2.EntPersonEntity.LnkCdtEntLink.Text.Contains(("Administrator")));
			//Wait until the page has the expected link 
			Wait.Until(d => Link(By.LinkText("System Administrator")).Exists);
		}

		[Test(Description = "Creates a new user on the fly from the person chooser and assigns to the person entity")]
		public void CreatePersonFromChooser()
		{
			//Go define the quick creation view first
			SetPersonQuickCreationView();

			//Navigate to the workspace
			var workspace = new DefaultTemplate("X-00000001");
			Store.LoginAsUser(Users.Admin);
			workspace.NavigateTo();

			//Open the smartform
			workspace.LnkEditProject.Click();
			var sfPage1 = new SmartFormPageBaseAttributes();
			sfPage1.BtnContinue.Click();
				
			//Open the person chooser
			var sfPage2 = new SmartFormPageTestDeAttributes();
			sfPage2.EntPersonEntity.BtnSelect.Click();
			var personChooser = new ChooserPopup("Person");
			personChooser.SwitchTo();

			//Create a new contact
			personChooser.BtnAddUser.Click();
			
			//Wait until the person creation view loads
			Wait.Until(d => (new TextBox(By.Name("Person.firstName"))).Exists);
			
			//Create a new contact
			String firstName = "FirstName_" + DataGen.AllLowerCaseString(4);
			String lastName = "LastName_" + DataGen.AllLowerCaseString(4);
			String email = "Email_" + DataGen.AllLowerCaseString(4) + "@huronconsultinggroup.com";
			var personQuickCreationPage = new PersonQuickCreationPage();
			personQuickCreationPage.CreatePerson(firstName, lastName, email);

			//Wait until the current chooser is updated with the contact that is just created
			Wait.Until(d => (new Radio(By.XPath("//td[text()='" + lastName + "']/../td[1]/input[@type='radio']"))).Exists);
			personChooser.SelectValue(lastName);
			personChooser.BtnOk.Click();
			personChooser.SwitchBackToParent(WaitForPopupToClose.Yes);

			//Wait until the parent refreshes
			Wait.Until(d => sfPage2.EntPersonEntity.LnkCdtEntLink.Text.Contains((lastName)));
		}

		/*
		 * Uncomment this when we add a person entity with an input text control in the original store
		[Test(Description = "Creates a contact using 'Add User' link from the input textbox and assigns to the person entity")]
		public void CreatePersonFromTextBox()
		{
			
			//Go define the quick creation view first
			SetPersonQuickCreationView();

			//Navigate to the workspace
			var workspace = new DefaultTemplate("X-00000001");
			Store.LoginAsUser(Users.Admin);
			workspace.NavigateTo();

			//Open the smartform
			workspace.LnkEditProject.Click();

			var sfPage1 = new SmartFormPageBaseAttributes();
			sfPage1.BtnContinue.Click();
			var sfPage2 = new SmartFormPageTestDeAttributes();
			var personEntity = sfPage2.EntPersonEntity;

			//Skip if there isn't a textbox
			if (personEntity.TxtCdtEntInput.Exists && personEntity.TxtCdtEntInput)
			{
				var input = "FirstName_" + DataGen.AllUpperCaseString(5);
				personEntity.TxtCdtEntInput.Value = input;
				Wait.Until(d => personEntity.LnkAddUser.Exists);
				personEntity.LnkAddUser.Click();

				//Wait until the person creation view loads
				Wait.Until(d => (new TextBox(By.Name("Person.firstName"))).Exists);

				//Create a new contact
				String firstName = input;
				String lastName = "LastName_" + DataGen.AllLowerCaseString(4);
				String email = "Email_" + DataGen.AllLowerCaseString(4) + "@huronconsultinggroup.com";
				var personQuickCreationPage = new PersonQuickCreationPage();
				personQuickCreationPage.CreatePerson(firstName, lastName, email);

				//Wait until this contact appears in the json results
				var lnkJsonResult = new Link(By.XPath("//strong[text()='" + input + "']"));
				Wait.Until(d => lnkJsonResult.Exists);
				lnkJsonResult.Click();

				//Wait until the parent refreshes
				Wait.Until(d => personEntity.LnkCdtEntLink.Text.Contains((lastName)));
			}
		}
		*/
	}
}
