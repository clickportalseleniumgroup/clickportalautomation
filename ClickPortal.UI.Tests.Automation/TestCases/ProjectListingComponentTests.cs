﻿using System;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.Components;
using CCWebUIAuto.PrimitiveElements;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using ClickPortal.UI.Tests.Automation.Pages.TestStore.TestProjType;
using ClickPortal.UI.Tests.Automation.TestStore;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
	class ProjectListingComponentTests : BaseTest
	{
		[Test(Description = "4.2.1 Paging")]
		public void Paging()
		{
			var page = new Projects();
			var component = page.ProjectsComponent;
			Store.LoginAsUser(Users.Admin);
			page.NavigateTo();
			component.SetPageSize("3");
			Wait.Until(d => component.BtnNextPage.Enabled && component.TxtRowsPerPage.Value == "3");

			Assert.AreEqual("1", component.TxtPage.Value);
			component.BtnNextPage.Click();
			Wait.Until(d => page.ProjectsComponent.BtnNextPage.Enabled && "2" == component.TxtPage.Value);
			component.BtnNextPage.Click();
			Wait.Until(d => page.ProjectsComponent.BtnPrevPage.Enabled && "3" == component.TxtPage.Value);

			component.BtnPrevPage.Click();
			Wait.Until(d => page.ProjectsComponent.BtnPrevPage.Enabled && "2" == component.TxtPage.Value);
			component.BtnPrevPage.Click();
			Wait.Until(d => !page.ProjectsComponent.BtnPrevPage.Enabled && "1" == component.TxtPage.Value);
			component.SetPageSize("10");
		}

		[Test(Description = "4.2.2 Filters 'primitives'")]
		public void FiltersPrimitives()
		{
			var id = "Test_4_2_2_" + DataGen.String(4);

			Store.LoginAsUser(Users.Admin);
			var projCreatorPage = new ProjectCreator();
			projCreatorPage.NavigateTo();
			projCreatorPage.SelProjectType.SelectOption("TestProjType");
			projCreatorPage.BtnCreate.Click();
			var sfPage1 = new SmartFormPageBaseAttributes();
			sfPage1.TxtId.Value = id;
			sfPage1.TxtName.Value = id;
			sfPage1.BtnContinue.Click();

			var page = new Projects();
			var component = page.ProjectsComponent;
			page.NavigateTo();
			component.SetCriteria("Name", id);
			Assert.AreEqual(1, component.GetNumberOfRowsDisplayed());
			component.BtnClear.Click();
			Wait.Until(d => !component.BtnClear.Enabled);
			Assert.AreNotEqual(1, component.GetNumberOfRowsDisplayed());
		}

		[Test(Description = "4.2.3 Date attributes can be filtered with day or minute resolution")]
		public void DatesFilterToDayOrMinute()
		{
			var page = new Projects();
			var component = page.ProjectsComponent;
			Store.LoginAsUser(Users.Admin);
			page.NavigateTo();
			
			component.SetCriteria("Date Created", "2/24/2012 4:04 PM");
			Wait.Until(d => 1 == component.GetNumberOfRowsDisplayed());
			Assert.AreEqual("2/24/2012 4:04 PM", component.GetValueAt(4, 0));
			
			component.SetCriteria("Date Created", "2/24/2012 4:06 PM");
			Wait.Until(d => 1 == component.GetNumberOfRowsDisplayed());
			Assert.AreEqual("2/24/2012 4:06 PM", component.GetValueAt(4, 0));

			component.SetCriteria("Date Created", "2/24/2012");
			Wait.Until(d => 2 == component.GetNumberOfRowsDisplayed());
			Assert.IsTrue(component.GetValueAt(4, 0).StartsWith("2/24/2012"));
			Assert.IsTrue(component.GetValueAt(4, 1).StartsWith("2/24/2012"));
		}

		[Test(Description = "4.2.4 Sorts by primitives")]
		public void SortsByPrimitives()
		{
			var prefix = "4_2_4_" + DataGen.String(4);
			var idA = prefix + "A";
			var idB = prefix + "B";

			Store.LoginAsUser(Users.Admin);
			var projCreatorPage = new ProjectCreator();
			projCreatorPage.NavigateTo();
			projCreatorPage.SelProjectType.SelectOption("TestProjType");
			projCreatorPage.BtnCreate.Click();
			var sfPage1 = new SmartFormPageBaseAttributes();
			sfPage1.TxtId.Value = idA;
			sfPage1.TxtName.Value = idA;
			sfPage1.BtnContinue.Click();

			projCreatorPage.NavigateTo();
			projCreatorPage.NavigateTo(); // do it twice to clear success message
			projCreatorPage.SelProjectType.SelectOption("TestProjType");
			projCreatorPage.BtnCreate.Click();
			sfPage1.TxtId.Value = idB;
			sfPage1.TxtName.Value = idB;
			sfPage1.BtnContinue.Click();

			var page = new Projects();
			ProjectListingComponent component = page.ProjectsComponent;
			Store.LoginAsUser(Users.Admin);
			page.NavigateTo();

			var lnkColumnHeader = new Link(By.LinkText("Name"));
			component.SetCriteria("Name", prefix);
			Wait.Until(d => component.GetNumberOfRowsDisplayed() == 2);
			String firstValue = component.GetValueAt(1, 0);
			String secondValue = firstValue == idA ? idB : idA;

			lnkColumnHeader.Click();
			Wait.Until(d => component.GetValueAt(1, 0) == secondValue);

			lnkColumnHeader.Click();
			Wait.Until(d => component.GetValueAt(1, 0) == firstValue);
		}

		[Test(Description = "4.2.5 Does not sort/filter entities or sets")]
		public void DoesNotSortEntitiesOrSets()
		{
			Store.LoginAsUser(Users.Admin);
			var testPage = new ProjectListingTesting();
			testPage.NavigateTo();
			var lnkIdHeader = testPage.SingleType.GetChildElementByXPath<Link>("//a[contains(text(), 'ID')]");
			var lnkNameHeader = testPage.SingleType.GetChildElementByXPath<Link>("//a[contains(text(), 'Name')]");

			// wait until the component is rendered
			// the sort indicator screws ups detection by text(), so look for either ID or Header (since it can't sort by both)
			Wait.Until(d => lnkNameHeader.Exists || lnkIdHeader.Exists);

			var lnkHypotheticalEntityHeader = testPage.SingleType.GetChildElementByXPath<Link>("//a[contains(text(), 'somebody')]");
			Assert.IsFalse(lnkHypotheticalEntityHeader.Exists);
			var tdExpectedEntityHeader = testPage.SingleType.GetChildElementByXPath<Container>("//td[contains(text(), 'somebody')]");
			Assert.IsTrue(tdExpectedEntityHeader.Exists);

			var lnkHypotheticalESetHeader = testPage.SingleType.GetChildElementByXPath<Link>("//a[contains(text(), 'stuff')]");
			Assert.IsFalse(lnkHypotheticalESetHeader.Exists);
			var tdExpectedESetHeader = testPage.SingleType.GetChildElementByXPath<Container>("//td[contains(text(), 'stuff')]");
			Assert.IsTrue(tdExpectedESetHeader.Exists);

			var selSortFields = testPage.SingleType.GetChildElementByXPath<Select>("//select");
			Assert.IsTrue(selSortFields.Contains("Name"));
			Assert.IsFalse(selSortFields.Contains("somebody"));
			Assert.IsFalse(selSortFields.Contains("stuff"));
		}

		[Test(Description = "4.2.7 Add columns via script (sortable/unsortable, filterable/unfilterable)")]
		public void ScriptedColumns()
		{
			Store.LoginAsUser(Users.Admin);
			var testPage = new ProjectListingTesting();
			testPage.NavigateTo();
			var component = testPage.SingleType;
			var lnkIntHeader = component.GetChildElementByXPath<Link>("//a[contains(text(), 'int')]");
			var lnkMoneyHeader = component.GetChildElementByXPath<Link>("//a[contains(text(), 'money')]");
			var lnkSomebodyHeader = component.GetChildElementByXPath<Link>("//a[contains(text(), 'somebody')]");
			var lnkStuffHeader = component.GetChildElementByXPath<Link>("//a[contains(text(), 'stuff')]");
			var tdSomebodyHeader = component.GetChildElementByXPath<Link>("//td[contains(text(), 'somebody')]");
			var tdStuffHeader = component.GetChildElementByXPath<Link>("//td[contains(text(), 'stuff')]");

			Wait.Until(d => lnkIntHeader.Exists);
			Assert.IsTrue(lnkMoneyHeader.Exists);
			Assert.IsFalse(lnkSomebodyHeader.Exists); //not sortable, so no link
			Assert.IsFalse(lnkStuffHeader.Exists);
			Assert.IsTrue(tdSomebodyHeader.Exists); //not sortable, no link, but header is still there
			Assert.IsTrue(tdStuffHeader.Exists);

			var selSortFields = testPage.SingleType.GetChildElementByXPath<Select>("//select");
			Assert.IsTrue(selSortFields.Contains("int"));
			Assert.IsTrue(selSortFields.Contains("money"));
			Assert.IsFalse(selSortFields.Contains("somebody"));
			Assert.IsFalse(selSortFields.Contains("stuff"));
		}

		[Test(Description="4.2.9 SmartForm links")]
		public void SmartFormLinks()
		{
			Store.LoginAsUser(Users.Admin);
			var testPage = new ProjectListingTesting();

			testPage.NavigateTo();
			var lnkTextComponentIconText = testPage.TextLinks.GetChildElementByXPath<Link>("//a[contains(text(), 'Edit Me')]");
			var lnkTextComponentIconImg = testPage.TextLinks.GetChildElementByXPath<Link>("//img[contains(@src, 'smartForm.gif')]");
			Wait.Until(d => lnkTextComponentIconText.Exists);
			Assert.IsFalse(lnkTextComponentIconImg.Exists);
			var lnkSmartFormPage = Link(By.LinkText("Base Attributes"));
			testPage.TextLinks.SetCriteria("ID", "X-00000001");
			Wait.Until(d => lnkTextComponentIconText.Exists);
			Assert.IsFalse(lnkSmartFormPage.Exists);
			lnkTextComponentIconText.Click();
			Wait.Until(d => lnkSmartFormPage.Exists);
			lnkSmartFormPage.Click();
			var smartFormPage = new SmartFormPageBaseAttributes();
			Wait.Until(d => smartFormPage.TxtName.Exists);

			testPage.NavigateTo();
			var lnkImgComponentIconText = testPage.IconLinks.GetChildElementByXPath<Link>("//a[contains(text(), 'Edit Me')]");
			var lnkImgComponentIconImg = testPage.IconLinks.GetChildElementByXPath<Link>("//img[contains(@src, 'smartForm.gif')]");
			Wait.Until(d => lnkImgComponentIconImg.Exists);
			Assert.IsFalse(lnkImgComponentIconText.Exists);
			testPage.IconLinks.SetCriteria("ID", "X-00000001");
			Assert.IsFalse(lnkSmartFormPage.Exists);
			lnkImgComponentIconImg.Click();
			Wait.Until(d => lnkSmartFormPage.Exists);
			lnkSmartFormPage.Click();
			Wait.Until(d => smartFormPage.TxtName.Exists);
		}

		[Test(Description = "4.2.10 Activity Dropdown")]
		public void ActivityLinks()
		{
			Store.LoginAsUser(Users.Admin);
			var testPage = new ProjectListingTesting();

			testPage.NavigateTo();
			var lnkTextComponentIconText = testPage.TextLinks.GetChildElementByXPath<Link>("//a[contains(text(), 'Activity Time')]");
			var lnkTextComponentIconImg = testPage.TextLinks.GetChildElementByXPath<Link>("//img[contains(@src, 'activity_type_default.gif')]");
			Wait.Until(d => lnkTextComponentIconText.Exists);
			Assert.IsFalse(lnkTextComponentIconImg.Exists);
			var lnkComment = Link(By.LinkText("Comment"));
			testPage.TextLinks.SetCriteria("ID", "X-00000001");
			Wait.Until(d => lnkTextComponentIconText.Exists);
			Assert.IsFalse(lnkComment.Exists);
			lnkTextComponentIconText.Click();
			Wait.Until(d => lnkComment.Exists);

			testPage.NavigateTo();
			var lnkImgComponentIconText = testPage.IconLinks.GetChildElementByXPath<Link>("//a[contains(text(), 'Activity Time')]");
			var lnkImgComponentIconImg = testPage.IconLinks.GetChildElementByXPath<Link>("//img[contains(@src, 'activity_type_default.gif')]");
			Wait.Until(d => lnkImgComponentIconImg.Exists);
			Assert.IsFalse(lnkImgComponentIconText.Exists);
			testPage.IconLinks.SetCriteria("ID", "X-00000001");
			Assert.IsFalse(lnkComment.Exists);
			lnkImgComponentIconImg.Click();
			Wait.Until(d => lnkComment.Exists);
		}

		[Ignore("Tests against this component fail, and it's not clear why")]
		[Test(Description = "4.2.11 Sets derived by script can be filtered")]
		public void FilterScriptDerivedSet()
		{
			Store.LoginAsUser(Users.Admin);
			var testPage = new ProjectListingTesting();
			testPage.NavigateTo();
			var component = testPage.ReplaceSetInScript;
			Wait.Until(d => component.GetNumberOfRowsDisplayed() == 2);
			component.SetCriteria("ID", "a-00000002");
			Wait.Until(d => component.GetValueAt(0, 0) == "a-00000002");
			component.SetCriteria("ID", "a-00000001");
			Wait.Until(d => component.GetValueAt(0, 0) == "a-00000001");
			component.Clear();
		}

		[Test(Description="4.2.13 Respects type selection")]
		public void RespectsTypeSelection()
		{
			Store.LoginAsUser(Users.Admin);
			var page = new Projects();
			var component = page.ProjectsComponent;
			page.NavigateTo();

			component.RunOnPropertiesPopup<ProjectListingComponentPropertiesPopup>(properties => {
				var popup = (ProjectListingComponentPropertiesPopup) properties;
				popup.LnkProjectFilteringOptions.Click();
				Wait.Until(d => popup.ProjectFilteringOptions.SelProjectTypes.Displayed);
				popup.ProjectFilteringOptions.SelProjectTypes.DeselectAll();
				popup.ProjectFilteringOptions.SelProjectTypes.SelectOption("VerySmallProjType");
			});
			Wait.Until(d => {
				component.SetCriteria("ID", "X-00000001");
				return component.GetNumberOfRowsDisplayed() == 0;
			});
			Wait.Until(d => {
				component.SetCriteria("ID", "a-00000001");
				return component.GetNumberOfRowsDisplayed() == 1;
			});

			component.RunOnPropertiesPopup<ProjectListingComponentPropertiesPopup>(properties => {
				var popup = (ProjectListingComponentPropertiesPopup) properties;
				popup.LnkProjectFilteringOptions.Click();
				Wait.Until(d => popup.ProjectFilteringOptions.SelProjectTypes.Displayed);
				popup.ProjectFilteringOptions.SelProjectTypes.DeselectAll();
				popup.ProjectFilteringOptions.SelProjectTypes.SelectOption("TestProjType");
			});
			Wait.Until(d => {
				component.SetCriteria("ID", "X-00000001");
				return component.GetNumberOfRowsDisplayed() == 1;
			});
			Wait.Until(d => {
				component.SetCriteria("ID", "a-00000001");
				return component.GetNumberOfRowsDisplayed() == 0;
			});

			component.RunOnPropertiesPopup<ProjectListingComponentPropertiesPopup>(properties => {
				var popup = (ProjectListingComponentPropertiesPopup) properties;
				popup.LnkProjectFilteringOptions.Click();
				Wait.Until(d => popup.ProjectFilteringOptions.SelProjectTypes.Displayed);
				popup.ProjectFilteringOptions.SelProjectTypes.DeselectAll();
				popup.ProjectFilteringOptions.SelProjectTypes.SelectOption("TestProjType");
				popup.ProjectFilteringOptions.SelProjectTypes.SelectOption("VerySmallProjType");
			});
			Wait.Until(d => {
				component.SetCriteria("ID", "X-00000001");
				return component.GetNumberOfRowsDisplayed() == 1;
			});
			Wait.Until(d => {
				component.SetCriteria("ID", "a-00000001");
				return component.GetNumberOfRowsDisplayed() == 1;
			});

			component.RunOnPropertiesPopup<ProjectListingComponentPropertiesPopup>(properties => {
				var popup = (ProjectListingComponentPropertiesPopup) properties;
				popup.LnkProjectFilteringOptions.Click();
				Wait.Until(d => popup.ProjectFilteringOptions.SelProjectTypes.Displayed);
				popup.ProjectFilteringOptions.SelProjectTypes.DeselectAll();
				popup.ProjectFilteringOptions.SelProjectTypes.SelectOption("Any");
			});
			Wait.Until(d => {
				component.SetCriteria("ID", "X-00000001");
				return component.GetNumberOfRowsDisplayed() == 1;
			});
			Wait.Until(d => {
				component.SetCriteria("ID", "a-00000001");
				return component.GetNumberOfRowsDisplayed() == 1;
			});
		}

		[Test(Description = "4.2.14 Enter filter, click link, go back, filter persists?")]
		public void FilterPersistance()
		{
			// For some reason Selenium works for this test if you return with the back key, but
			// not if you return with .NavigateTo().

			var testPage = new Projects();
			Store.LoginAsUser(Users.Admin);
			testPage.NavigateTo();
			var component = testPage.ProjectsComponent;
			var prefix = component.XpathPrefix;
			var selFilter = new Select(By.XPath(prefix + "//select[contains(@id,'_queryField1')]"));
			var txtFilter = new TextBox(By.XPath(prefix + "//input[contains(@id,'_queryCriteria1')]"));
			var lnkHome = new Link(By.LinkText("Home"));
			component.SetCriteria("Name", "%0001");

			// navigate away and return via back button
			lnkHome.Click();
			Wait.Until(d => new Link(By.LinkText("Project Creator")).Exists);
			CCPage.NavigateBack();
			Assert.AreEqual("Name", selFilter.SelectedOption);
			Assert.AreEqual("%0001", txtFilter.Value);
		}

		[Test(Description = "4.2.24 Filters of the pattern '%22' work correctly")]
		public void FiltersDoNotEncode()
		{
			var testPage = new Projects();
			Store.LoginAsUser(Users.Admin);
			testPage.NavigateTo();
			var component = testPage.ProjectsComponent;
			component.SetCriteria("Name", "%22");
			Assert.AreEqual("Search for '%22' on ID matches this, not a 'space'", component.GetValueAt(1, 0));
		}

		[Test(Description = "4.2.25 Works correctly if placed on the workspace of a project with '#' or '%' in the ID")]
		public void WorksInWorkspacesWithSpecialCharactersInId()
		{
			var workspace = new DefaultTemplate("X-00000013");
			Store.LoginAsUser(Users.Admin);
			workspace.NavigateTo();
			workspace.LnkProjectListingTab.Click();
			Wait.Until(d => workspace.ProjectListing.TxtPage.Displayed);
		}

		[Test(Description = "4.2.26 Do not show generic message for badly formed dates")]
		public void HandleBadlyFormedDates()
		{
			var testPage = new Projects();
			Store.LoginAsUser(Users.Admin);
			testPage.NavigateTo();
			var component = testPage.ProjectsComponent;
			component.SetCriteria("Date Modified", "this day sucks");
			var spanError = new Container(By.CssSelector(".Error"));
			Wait.Until(d => spanError.Exists);
			Assert.AreEqual("Invalid Date/Time. Please use the format \"mm/dd/yyyy\" or \"mm/dd/yyyy hh:mm (AM/PM)\"", spanError.Text);
		}

		[Test(Description="Add column with script")]
		public void AddColumnWithScript()
		{
			var testPage = new Projects();
			var component = testPage.ProjectsComponent;
			var name = "TestProj_" + DataGen.String(4);
			var projCreatorPage = new ProjectCreator();
			var sfPage1 = new SmartFormPageBaseAttributes();
			var sfPage2 = new SmartFormPageTestDeAttributes();

			Store.LoginAsUser(Users.Admin);
			projCreatorPage.NavigateTo();
			projCreatorPage.NavigateTo(); // clear created successfully message
			projCreatorPage.SelProjectType.SelectOption("TestProjType");
			projCreatorPage.BtnCreate.Click();
			sfPage1.TxtName.Value = name;
			sfPage1.BtnContinue.Click();
			sfPage2.TxtString.Value = name;
			sfPage2.BtnContinue.Click();

			testPage.NavigateTo();
			component.RunOnPropertiesPopup<ProjectListingComponentPropertiesPopup>(properties => {
				var popup = (ProjectListingComponentPropertiesPopup) properties;
				popup.LnkProjectFilteringOptions.Click();
				Wait.Until(d => popup.ProjectFilteringOptions.SelProjectTypes.Displayed);
				popup.ProjectFilteringOptions.SelProjectTypes.DeselectAll();
				popup.ProjectFilteringOptions.SelProjectTypes.SelectOption("TestProjType");
				popup.ProjectFilteringOptions.BtnEditScript.Click();
				var scriptEditor = new WorkflowScriptEditor();
				scriptEditor.SwitchTo();
				scriptEditor.TxtScript.Value = 
					"wom.putContext('ProjectViewHeaders', ['Custom Column'], true);\n" +
					"wom.putContext('ProjectViewAttributes', ['Project.customAttributes.testDE.customAttributes.string'], true);";
				scriptEditor.BtnOk.Click();
				scriptEditor.SwitchBackToParent();
			});
			Wait.Until(d => Link(By.LinkText("Custom Column")).Exists);
			component.SetCriteria("Name", name);
			Wait.Until(d => name == component.GetValueAt(6, 0));

			component.RunOnPropertiesPopup<ProjectListingComponentPropertiesPopup>(properties => {
				var popup = (ProjectListingComponentPropertiesPopup) properties;
				popup.LnkProjectFilteringOptions.Click();
				Wait.Until(d => popup.ProjectFilteringOptions.SelProjectTypes.Displayed);
				popup.ProjectFilteringOptions.SelProjectTypes.DeselectAll();
				popup.ProjectFilteringOptions.SelProjectTypes.SelectOption("Any");
				popup.ProjectFilteringOptions.BtnEditScript.Click();
				var scriptEditor = new WorkflowScriptEditor();
				scriptEditor.SwitchTo();
				scriptEditor.BtnClear.Click();
				AcceptAlert();
				scriptEditor.BtnOk.Click();
				scriptEditor.SwitchBackToParent();
			});
			Wait.Until(d => !Link(By.LinkText("Custom Column")).Exists);
		}

		[Test(Description = "Add column with properties UI")]
		public void AddColumnWithUi()
		{
			var testPage = new Projects();
			var component = testPage.ProjectsComponent;
			var name = "TestProj_" + DataGen.String(4);
			var projCreatorPage = new ProjectCreator();
			var sfPage1 = new SmartFormPageBaseAttributes();
			var sfPage2 = new SmartFormPageTestDeAttributes();

			Store.LoginAsUser(Users.Admin);
			projCreatorPage.NavigateTo();
			projCreatorPage.NavigateTo(); // clear created successfully message
			projCreatorPage.SelProjectType.SelectOption("TestProjType");
			projCreatorPage.BtnCreate.Click();
			sfPage1.TxtName.Value = name;
			sfPage1.BtnContinue.Click();
			sfPage2.TxtString.Value = name;
			sfPage2.BtnContinue.Click();

			testPage.NavigateTo();
			component.RunOnPropertiesPopup<ProjectListingComponentPropertiesPopup>(properties => {
				var popup = (ProjectListingComponentPropertiesPopup) properties;
				popup.DisplayOptions.BtnAddColumn.Click();
				Wait.Until(d => popup.CustomColumnEditorDialog.TxtHeader.Displayed);
				popup.CustomColumnEditorDialog.TxtHeader.Value = "Custom Column";
				popup.CustomColumnEditorDialog.TxtDataPath.Value = "customAttributes.testDE.customAttributes.string";
				popup.CustomColumnEditorDialog.BtnOk.Click();
				Wait.Until(d => popup.DisplayOptions.SelCustomColumns.Contains("Custom Column"));
			});
			Wait.Until(d => new Container(By.XPath("//td[contains(text(), 'Custom Column')]")).Exists);
			component.SetCriteria("Name", name);
			Assert.AreEqual(name, component.GetValueAt(6, 0));


			component.RunOnPropertiesPopup<ProjectListingComponentPropertiesPopup>(properties => {
				var popup = (ProjectListingComponentPropertiesPopup) properties;
				Wait.Until(d => popup.DisplayOptions.SelCustomColumns.Contains("Custom Column"));
				popup.DisplayOptions.SelCustomColumns.SelectOption("Custom Column");
				popup.DisplayOptions.BtnRemoveColumn.Click();
				Wait.Until(d => !popup.DisplayOptions.SelCustomColumns.Contains("Custom Column"));
			});
			Wait.Until(d => !new Container(By.XPath("//td[contains(text(), 'Custom Column')]")).Exists);
		}

		[Test(Description = "Custom columns aren't sortable in sets with more than one project type")]
		public void CustomColumnNotSortableInPolymorphicSet()
		{
			var testPage = new Projects();
			var component = testPage.ProjectsComponent;

			Store.LoginAsUser(Users.Admin);
			testPage.NavigateTo();
			component.RunOnPropertiesPopup<ProjectListingComponentPropertiesPopup>(properties => {
				var popup = (ProjectListingComponentPropertiesPopup) properties;
				popup.DisplayOptions.BtnAddColumn.Click();
				Wait.Until(d => popup.CustomColumnEditorDialog.TxtHeader.Displayed);
				popup.CustomColumnEditorDialog.TxtHeader.Value = "Custom Column";
				popup.CustomColumnEditorDialog.TxtDataPath.Value = "customAttributes.testDE.customAttributes.string";
				popup.CustomColumnEditorDialog.TxtSortByDataPath.Value = "customAttributes.testDE.customAttributes.string";
				popup.CustomColumnEditorDialog.BtnOk.Click();
				Wait.Until(d => popup.DisplayOptions.SelCustomColumns.Contains("Custom Column"));
				popup.LnkProjectFilteringOptions.Click();
				Wait.Until(d => popup.ProjectFilteringOptions.SelProjectTypes.Displayed);
				popup.ProjectFilteringOptions.SelProjectTypes.DeselectAll();
				popup.ProjectFilteringOptions.SelProjectTypes.SelectOption("Any");
			});
			Wait.Until(d => new Container(By.XPath("//td[contains(text(), 'Custom Column')]")).Exists);
			Assert.IsFalse(Link(By.LinkText("Custom Column")).Exists);


			component.RunOnPropertiesPopup<ProjectListingComponentPropertiesPopup>(properties => {
				var popup = (ProjectListingComponentPropertiesPopup) properties;
				Wait.Until(d => popup.DisplayOptions.SelCustomColumns.Contains("Custom Column"));
				popup.DisplayOptions.SelCustomColumns.SelectOption("Custom Column");
				popup.DisplayOptions.BtnRemoveColumn.Click();
				Wait.Until(d => !popup.DisplayOptions.SelCustomColumns.Contains("Custom Column"));
			});
			Wait.Until(d => !new Container(By.XPath("//td[contains(text(), 'Custom Column')]")).Exists);
		}

		[Test(Description = "Custom columns are sortable in single type sets")]
		public void CustomColumnSortableInSingleTypeSet()
		{
			var testPage = new Projects();
			var component = testPage.ProjectsComponent;

			Store.LoginAsUser(Users.Admin);
			testPage.NavigateTo();
			component.RunOnPropertiesPopup<ProjectListingComponentPropertiesPopup>(properties => {
				var popup = (ProjectListingComponentPropertiesPopup) properties;
				popup.DisplayOptions.BtnAddColumn.Click();
				Wait.Until(d => popup.CustomColumnEditorDialog.TxtHeader.Displayed);
				popup.CustomColumnEditorDialog.TxtHeader.Value = "Custom Column";
				popup.CustomColumnEditorDialog.TxtDataPath.Value = "customAttributes.testDE.customAttributes.string";
				popup.CustomColumnEditorDialog.TxtSortByDataPath.Value = "customAttributes.testDE.customAttributes.string";
				popup.CustomColumnEditorDialog.BtnOk.Click();
				Wait.Until(d => popup.DisplayOptions.SelCustomColumns.Contains("Custom Column"));
				popup.LnkProjectFilteringOptions.Click();
				Wait.Until(d => popup.ProjectFilteringOptions.SelProjectTypes.Displayed);
				popup.ProjectFilteringOptions.SelProjectTypes.DeselectAll();
				popup.ProjectFilteringOptions.SelProjectTypes.SelectOption("TestProjType");
			});
			Wait.Until(d => Link(By.LinkText("Custom Column")).Exists);

			component.RunOnPropertiesPopup<ProjectListingComponentPropertiesPopup>(properties => {
				var popup = (ProjectListingComponentPropertiesPopup) properties;
				Wait.Until(d => popup.DisplayOptions.SelCustomColumns.Contains("Custom Column"));
				popup.DisplayOptions.SelCustomColumns.SelectOption("Custom Column");
				popup.DisplayOptions.BtnRemoveColumn.Click();
				Wait.Until(d => !popup.DisplayOptions.SelCustomColumns.Contains("Custom Column"));
				popup.LnkProjectFilteringOptions.Click();
				Wait.Until(d => popup.ProjectFilteringOptions.SelProjectTypes.Displayed);
				popup.ProjectFilteringOptions.SelProjectTypes.DeselectAll();
				popup.ProjectFilteringOptions.SelProjectTypes.SelectOption("Any");
			});
			Wait.Until(d => !Link(By.LinkText("Custom Column")).Exists);
		}

		[Test(Description="Properly Enable/Disable Custom Column Controls")]
		public void EnableDisableCustomColumnButtons()
		{
			var customColumn = DataGen.String(8);
			var testPage = new Projects();
			var component = testPage.ProjectsComponent;

			Store.LoginAsUser(Users.Admin);
			testPage.NavigateTo();
			component.RunOnPropertiesPopup<ProjectListingComponentPropertiesPopup>(properties => {
				var popup = (ProjectListingComponentPropertiesPopup) properties;
				Wait.Until(d => popup.DisplayOptions.BtnAddColumn.Enabled &&
								!popup.DisplayOptions.BtnEditColumn.Enabled &&
								!popup.DisplayOptions.BtnRemoveColumn.Enabled);

				popup.DisplayOptions.BtnAddColumn.Click();
				Wait.Until(d => popup.CustomColumnEditorDialog.TxtHeader.Displayed);
				popup.CustomColumnEditorDialog.TxtHeader.Value = customColumn;
				popup.CustomColumnEditorDialog.TxtDataPath.Value = "customAttributes.testDE.customAttributes.string";
				popup.CustomColumnEditorDialog.BtnOk.Click();
				Wait.Until(d => popup.DisplayOptions.SelCustomColumns.Contains(customColumn));

				Wait.Until(d => popup.DisplayOptions.BtnAddColumn.Enabled &&
								!popup.DisplayOptions.BtnEditColumn.Enabled &&
								!popup.DisplayOptions.BtnRemoveColumn.Enabled);

				popup.DisplayOptions.SelCustomColumns.SelectOption(customColumn);
				Wait.Until(d => popup.DisplayOptions.BtnAddColumn.Enabled &&
								popup.DisplayOptions.BtnEditColumn.Enabled &&
								popup.DisplayOptions.BtnRemoveColumn.Enabled);

				popup.DisplayOptions.BtnRemoveColumn.Click();
				Wait.Until(d => popup.DisplayOptions.BtnAddColumn.Enabled &&
								!popup.DisplayOptions.BtnEditColumn.Enabled &&
								!popup.DisplayOptions.BtnRemoveColumn.Enabled);
			});
		}

		[Test(Description="Works with an apostrophe in the name")]
		public void WorksWithApostropheInName()
		{
			const String modifiedDisplayName = "Project Listing '!@#$%^&*()";
			var testPage = new Projects();
			var component = testPage.ProjectsComponent;

			Store.LoginAsUser(Users.Admin);
			testPage.NavigateTo();
			component.RunOnPropertiesPopup<ProjectListingComponentPropertiesPopup>(properties => {
				var popup = (ProjectListingComponentPropertiesPopup) properties;
				popup.TxtTitle.Value = modifiedDisplayName;
			});
			
			testPage.NavigateTo();
			try {
				component.SetCriteria("Name", "Example Project");
				Assert.AreEqual("Example Project", component.GetValueAt(1, 0));
			} finally {
				component.RunOnPropertiesPopup<ProjectListingComponentPropertiesPopup>(properties => {
					var popup = (ProjectListingComponentPropertiesPopup) properties;
					popup.TxtTitle.Value = "Project Listing";
				});
			}
		}
	}
}
