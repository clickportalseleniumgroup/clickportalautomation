﻿using System.Diagnostics;
using System.Globalization;
using System.Text;
using CCWebUIAuto;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.Pages.BasePages.ProjectTypeCenter;
using CCWebUIAuto.PrimitiveElements;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
    [TestFixture]
    public class ProjectTypeProperties : BaseTest
    {
		[Test(Description = "// 1.1.1.1 Enter only display name with pattern 'ValidName', internal name becomes '_ValidName'")]
        public void CreateProjectTypeWithSimpleName()
        {
            // generate display and internal names for a type based on a random string
            var testDisplayName = DataGen.String(8);
            var testInternalName = "_" + testDisplayName;

			// grab references to the static layouts we will use
			var typeCenter = new ProjectTypeCenterPage();
			var newProjTypeForm = new PropertiesTab(testInternalName);

            // login here
			Store.LoginAsUser(Users.Admin);

            // navigate to the Project Type Center and verify our test type doesn't exist
            new ProjectTypeCenterPage().NavigateTo();
            
            Assert.False(typeCenter.ProjectTypeExists(testDisplayName));

            // while at the Project Type Center, go through the motions of creating a type
            // this could be encapsulated in a single method call, if other tests need to do 
            // this and just assume it works.  Here we step through each motion because it
            // is the thing being tested.  We will have other tests explicitely testing
            // sequences of keys and button presses outside this pattern
			typeCenter.BtnNew.Click(); // takes us to NewProjectTypeForm
			newProjTypeForm.DisplayName.Value = testDisplayName;
			newProjTypeForm.OkButton.Click();

            // return to the type center and verify out type is there
			typeCenter.NavigateTo();
			
            Assert.True(typeCenter.ProjectTypeExists(testDisplayName));
        }

		[Test(Description = "// 1.1.1.2 Enter only display name with pattern 'Valid Name with Spaces', internal name becomes '_ValidNameWithSpaces'")]
	    public void CreateProjectTypeWithSpaces()
	    {
			var nameFragmentA = DataGen.AllLowerCaseString(4);
			var nameFragmentB = DataGen.AllLowerCaseString(4);
			var capitalizedFragmentB = char.ToUpper(nameFragmentB[0]) + nameFragmentB.Substring(1);
			var testDisplayName = nameFragmentA + " " + nameFragmentB;
			var testInternalName = "_" + nameFragmentA + capitalizedFragmentB;
            var typeCenter = new ProjectTypeCenterPage();
			var newProjTypeForm = new PropertiesTab(testInternalName);

			Store.LoginAsUser(Users.Admin);
			typeCenter.NavigateTo();
			typeCenter.BtnNew.Click();
			newProjTypeForm.DisplayName.Value = testDisplayName;
			newProjTypeForm.OkButton.Click();

			typeCenter.NavigateTo();
            Assert.True(typeCenter.ProjectTypeExists(testDisplayName));
	    }

	    [Test(Description = "1.1.1.3 Enter a display name with pattern 'Has Custom Internal Name', customize internal name as 'IsCustomInternalName'")]
	    public void CreateProjectTypeWithCustomName()
	    {
            var testDisplayName = DataGen.String(8);
            var internalNameRoot = DataGen.String(8);
            var testInternalName = "_" + internalNameRoot;
			var typeCenter = new ProjectTypeCenterPage();
            var newProjTypeForm = new PropertiesTab(testInternalName);

			Store.LoginAsUser(Users.Admin);
			typeCenter.NavigateTo();

			typeCenter.BtnNew.Click();
			newProjTypeForm.DisplayName.Value = testDisplayName;
			newProjTypeForm.TxtCustomizeInternalName.Value = internalNameRoot;
			newProjTypeForm.OkButton.Click();

			typeCenter.NavigateTo();
            Assert.True(typeCenter.ProjectTypeExists(testDisplayName));	    
	    }

	    [Test(Description = "1.1.1.4 Enter a display name with invalid characters like '%*()', input is sanitized when building internal name, receive error message on submit")]
	    public void CreateProjectTypeWithBadDisplayName()
	    {
            var displayNameRoot = DataGen.String(4);
            var testDisplayName = displayNameRoot + "$*()";
            var testInternalName = "_" + displayNameRoot;
            var typeCenter = new ProjectTypeCenterPage();
            var newProjTypeForm = new PropertiesTab(testInternalName);

			Store.LoginAsUser(Users.Admin);
			typeCenter.NavigateTo();

			typeCenter.BtnNew.Click();
			newProjTypeForm.DisplayName.Value = testDisplayName;
			newProjTypeForm.OkButton.Click();

			var alert = Web.Driver.SwitchTo().Alert();
            Assert.True(alert.Text.Contains("The display name you have supplied contains one or more invalid characters."));
            alert.Accept();
            
			typeCenter.NavigateTo();
            Assert.False(typeCenter.ProjectTypeExists(testDisplayName));		    
	    }

		[Test(Description = "1.1.1.5 Customize internal name with invalid characters like '%*()', input is sanitized when building internal name")]
		public void CreateProjectTypeWithSanitizedInternalName()
		{
            var testDisplayName = DataGen.String(4);
            var internalNameRoot = DataGen.String(4);
            var internalNameToBeSanitized = internalNameRoot + "$*()";
            var testInternalName = "_" + testDisplayName.Substring(0, 4);
            var typeCenter = new ProjectTypeCenterPage();
            var newProjTypeForm = new PropertiesTab(testInternalName);

			Store.LoginAsUser(Users.Admin);
			typeCenter.NavigateTo();

			typeCenter.BtnNew.Click();
			newProjTypeForm.DisplayName.Value = testDisplayName;
			newProjTypeForm.TxtCustomizeInternalName.Value = internalNameToBeSanitized;
			newProjTypeForm.OkButton.Click();

			var alert = Web.Driver.SwitchTo().Alert();
            Assert.True(alert.Text.Contains(internalNameRoot));
            alert.Accept();
            ClickPortalUI.Wait.Until(d => new Button(By.CssSelector("input[value='Security Policies...']")).Exists);
			
            typeCenter.NavigateTo();
            Assert.True(typeCenter.ProjectTypeExists(testDisplayName));	
		}
  
        [Test(Description = "1.1.1.6 Display name changes are echoed in internal name with automatic 'camel-casing' and removal of white space, until a change is made directly to internal name")]
        public void SimpleInputCascadeOnProjectType()
		{
			var typeCenter = new ProjectTypeCenterPage();
            var newProjTypeForm = new PropertiesTab("dummyValue");

			Store.LoginAsUser(Users.Admin);
			typeCenter.NavigateTo();
			typeCenter.BtnNew.AsyncClick();
            
			// step A, input a display name with a space, verify the internal name is sanitized
			var stepAFragementA = DataGen.String(4);
			var stepAFragementB = DataGen.AllLowerCaseString(4);
			var stepADisplayName = stepAFragementA + " " + stepAFragementB;
			var stepACustomInternal = stepAFragementA + stepAFragementB;
			var stepAInternal = "_" + stepACustomInternal;
            // Need to capitalize 6th character to adapt to camel_casing rules
            var newString = new StringBuilder(stepAInternal);
            var upperCase = stepAInternal[5].ToString(CultureInfo.InvariantCulture).ToUpper();
            newString.Remove(5,1);
            var myString = newString.Insert(5, upperCase).ToString();

			newProjTypeForm.DisplayName.Value = stepADisplayName;
			Trace.WriteLine("Custom internal name:  " + newProjTypeForm.TxtCustomizeInternalName.Value);
            Trace.WriteLine("Custom internal name:  " + newProjTypeForm.SpanInternalName.Text);

            Assert.AreEqual(newProjTypeForm.SpanInternalName.Text, myString);

			// step B, modify the display name, see the sanitized internal name change to match
			var stepBFragementA = DataGen.String(4);
            var stepBFragementB = DataGen.AllLowerCaseString(4);
			var stepBDisplayName = stepBFragementA + " " + stepBFragementB;
			var stepBCustomInternal = stepBFragementA + stepBFragementB;
			var stepBInternal = "_" + stepBCustomInternal;

            var newString2 = new StringBuilder(stepBInternal);
            var upperCase2 = stepBInternal[5].ToString(CultureInfo.InvariantCulture).ToUpper();
            newString2.Remove(5, 1);
            var myString2 = newString2.Insert(5, upperCase2).ToString();

			newProjTypeForm.DisplayName.Value = stepBDisplayName;
            Assert.AreEqual(newProjTypeForm.SpanInternalName.Text, myString2);

			// step C, change the internal root and nothing happens to the display name (fields become 'disconnected')
			var stepCCustomInternal = DataGen.String(8);
			var stepCInternal = "_" + stepCCustomInternal;
			newProjTypeForm.TxtCustomizeInternalName.Value = stepCCustomInternal;
			Assert.AreEqual(newProjTypeForm.SpanInternalName.Text, stepCInternal); // changed to match new 'custom' portion

			// step D, change the display name again and it no longer modifies the internal root
			var stepDDisplayName = DataGen.String(8);
			newProjTypeForm.DisplayName.Value = stepDDisplayName;
			Assert.AreEqual(newProjTypeForm.SpanInternalName.Text, stepCInternal); // unchanged from step C
		}

        // It is tempting to split these into different tests for purity, but since the edit and delete
        // tests would have to create attributes, it's easiest to combine them into one.  An alternative
        // would be to have pre-created attributes for edit and deletion tests.
	    [Test(Description = "1.1.2-4 Add, edit, and delete custom attribute")]
        public void ManageCustomAttributes()
	    {
            var propertiesTab = new PropertiesTab("_VerySmallProjType");
            var internalName = DataGen.String(8);
            var displayName = DataGen.String(8);

            // Login and open Project Type Center
			Store.LoginAsUser(Users.Admin);
			propertiesTab.NavigateTo();

            // Create new attribute
            Assert.IsFalse(propertiesTab.Table.Text.Contains(internalName));
            Assert.IsFalse(propertiesTab.Table.Text.Contains(displayName));
            propertiesTab.BtnNewCustomAttribute.AsyncClick();
            PopUpWindow.SwitchTo("Field Definition");
			var createPopup = new CustomAttributeDefinitionPopup();
            createPopup.SelDataType.SelectOption("Boolean");
		    createPopup.TxtDisplayName.Value = displayName;
		    createPopup.TxtInternalName.Value = internalName;
			createPopup.BtnOk.Click();
            PopUpWindow.SwitchTo("Project Type (VerySmallProjType)");
            Assert.IsTrue(propertiesTab.Table.Text.Contains(internalName));
            Assert.IsTrue(propertiesTab.Table.Text.Contains(displayName));

            // Edit attribute
	        var newDisplayName = DataGen.String(8);
            var attrLink = new Link(By.LinkText(displayName));
            attrLink.AsyncClick();
            PopUpWindow.SwitchTo("Field Definition");
			var editPopup = new CustomAttributeDefinitionPopup();
            editPopup.TxtDisplayName.Value = newDisplayName;
            editPopup.BtnOk.Click();
            PopUpWindow.SwitchTo("Project Type (VerySmallProjType)");
            Assert.IsTrue(propertiesTab.Table.Text.Contains(internalName));
            Assert.IsFalse(propertiesTab.Table.Text.Contains(displayName));
            Assert.IsTrue(propertiesTab.Table.Text.Contains(newDisplayName));

            // Remove attribute
            propertiesTab.CheckRow(internalName);
            propertiesTab.BtnDeleteCustomAttribute.Click();
            Assert.IsFalse(propertiesTab.Table.Text.Contains(internalName));
            Assert.IsFalse(propertiesTab.Table.Text.Contains(displayName));
            Assert.IsFalse(propertiesTab.Table.Text.Contains(newDisplayName));
	    }

        [Test(Description = "1.1.6 Set the display attribute")]
        public void SetDisplayAttribute()
        {
            var propertiesTab = new PropertiesTab("_VerySmallProjType");

            // Login and open Project Type Center
            Store.LoginAsUser(Users.Admin);
            propertiesTab.NavigateTo();
			var existingValue = propertiesTab.DisplayValue.Value;

            // set the display value and verify it persisted
			var popup = new SelectPropertyModalPopup();
			popup.SelectProperty("dateArchived", propertiesTab.BtnChooseDisplayValue);
            propertiesTab.OkButton.Click();
			Assert.AreEqual(propertiesTab.DisplayValue.Value, "dateArchived");

            // reset the display value to it's original value and verify it persisted
			popup.SelectProperty(existingValue, propertiesTab.BtnChooseDisplayValue);
            propertiesTab.OkButton.Click();
			Assert.AreEqual(propertiesTab.DisplayValue.Value, existingValue);
        }
    }
}

