﻿using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages.BasePages.ProjectTypeCenter;
using CCWebUIAuto.PrimitiveElements;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
    [TestFixture]
    public class ProjectTypeView : BaseTest
    {
        public const string TestProjType = "_VerySmallProjType";

        [Test]
        [Description("1.2.1 & 1.2.2 Modify a view name for a project type")]
        public void ModifyViewName()
        {
			var myProjectTypeViewTab = new ViewsTab(TestProjType);
			//var myProjectTypeViewTab = BasePagesX.ProjectTypeCenter.ViewsTab(TestProjType);
            var viewNameA = "Test-View-2-" + DataGen.String(4);
            var viewNameB = viewNameA + "-Modified";

            Store.LoginAsUser(Users.Admin);
            myProjectTypeViewTab.NavigateTo();
            Assert.IsFalse(myProjectTypeViewTab.VerifyViewExists(viewNameA), "view exists when not expected.");
            myProjectTypeViewTab.CreateNewView(viewNameA);
            Wait.Until(d => myProjectTypeViewTab.VerifyViewExists(viewNameA));
            myProjectTypeViewTab.ModifyViewName(viewNameA, viewNameB);
			Wait.Until(d => myProjectTypeViewTab.VerifyViewExists(viewNameB));
            Assert.IsFalse(myProjectTypeViewTab.VerifyViewExists(viewNameA), "view exists when not expected.");
        }

        [Test]
        [Description("1.2.5 Set creation, summary, and print views")]
        public void SetCreationSummaryPrintViews()
        {
            var myProjectTypeViewTab = new ViewsTab(TestProjType);
            var creationViewName = "Creation_View_" + DataGen.String(4);
            var creationViewIndicator =
                new Image(
                    By.XPath("//*[@id='_webrRSV_DIV_0']/table/tbody/tr[*]/td[4]/a[text()='" + creationViewName + "']/../../td[9]/img"));
            var summaryViewName = "Summary_View_" + DataGen.String(4);
            var summaryViewIndicator =
				new Image(
                    By.XPath("//*[@id='_webrRSV_DIV_0']/table/tbody/tr[*]/td[4]/a[text()='" + summaryViewName + "']/../../td[10]/img"));
            var printViewName = "Print_View_" + DataGen.String(4);
            var printViewIndicator =
				new Image(
                    By.XPath("//*[@id='_webrRSV_DIV_0']/table/tbody/tr[*]/td[4]/a[text()='" + printViewName + "']/../../td[11]/img"));

            Store.LoginAsUser(Users.Admin);
            myProjectTypeViewTab.NavigateTo();
            myProjectTypeViewTab.CreateNewView(creationViewName);
            myProjectTypeViewTab.CreateNewView(summaryViewName);
            myProjectTypeViewTab.CreateNewView(printViewName);

            myProjectTypeViewTab.SetStandardViewProperties(creationViewName, summaryViewName, printViewName);
            Assert.IsTrue(creationViewIndicator.Exists);
            Assert.IsTrue(summaryViewIndicator.Exists);
            Assert.IsTrue(printViewIndicator.Exists);
        }

        [Test]
        [Description("1.2.6 Create a validation script for project Type view")]
        public void CreateValidationScript()
        {
            var myProjectTypeViewTab = new ViewsTab(TestProjType);
            Store.LoginAsUser(Users.Admin);
            myProjectTypeViewTab.NavigateTo();
            myProjectTypeViewTab.SetValidationScript("TheView", "// validation script");
            Assert.IsTrue(myProjectTypeViewTab.VerifyValidationScriptExists("TheView", "// validation script"), "Validation script does not exist when expected.");
        }

        [Test]
        [Description("1.2.1 & 1.2.4 Create and delete a view")]
        public void CreateAndDeleteView()
        {
            var myProjectTypeViewTab = new ViewsTab(TestProjType);
            var viewName = "CreateAndDelete_" + DataGen.String(4);

            Store.LoginAsUser(Users.Admin);
            myProjectTypeViewTab.NavigateTo();
            Assert.IsFalse(myProjectTypeViewTab.VerifyViewExists(viewName), "view exists when not expected.");
            myProjectTypeViewTab.CreateNewView(viewName);
            Assert.IsTrue(myProjectTypeViewTab.VerifyViewExists(viewName), "view does not exist.");
            myProjectTypeViewTab.DeleteView(viewName);
            ClickPortalUI.Wait.Until(d => !myProjectTypeViewTab.VerifyViewExists(viewName));
            Assert.IsFalse(myProjectTypeViewTab.VerifyViewExists(viewName), "view exists when not expected.");
        }
    }
}