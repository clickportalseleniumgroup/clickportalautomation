﻿using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages.BasePages.ProjectTypeCenter;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using NUnit.Framework;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
    [TestFixture]
    public class ProjectTypeState : BaseTest
    {
        public const string TestProjType = "_VerySmallProjType";

        [Test]
        public void CreateProjectTypeState()
        {
			var myProjectTypeStatesTab = new StatesTab(TestProjType);
	        var name = "StateTest-1-" + DataGen.String(4);
            Store.LoginAsUser(Users.Admin);
            myProjectTypeStatesTab.NavigateTo();
			myProjectTypeStatesTab.CreateNewState(name);
			Assert.IsTrue(myProjectTypeStatesTab.VerifyStateExists(name), "StateTest-1 does not exist.");
        }

        [Test]
        public void ModifyProjectTypeStateName()
        {
            var myProjectTypeStatesTab = new StatesTab(TestProjType);
	        var nameA = "orig-" + DataGen.String(4);
			var nameB = "mod-" + DataGen.String(4);
            Store.LoginAsUser(Users.Admin);
            myProjectTypeStatesTab.NavigateTo();
			myProjectTypeStatesTab.CreateNewState(nameA);
			Assert.IsTrue(myProjectTypeStatesTab.VerifyStateExists(nameA), "state does not exist.");
			myProjectTypeStatesTab.ModifyStateName(nameA, nameB);
			Assert.IsTrue(myProjectTypeStatesTab.VerifyStateExists(nameB), "modified state does not exist.");
        }

        [Test]
        public void DeleteProjectTypeState()
        {
            var stateName = "delMe_" + DataGen.String(4);
            var myProjectTypeStatesTab = new StatesTab(TestProjType);
            Store.LoginAsUser(Users.Admin);
            myProjectTypeStatesTab.NavigateTo();
            myProjectTypeStatesTab.CreateNewState(stateName);
            Assert.IsTrue(myProjectTypeStatesTab.VerifyStateExists(stateName), "delMe does not exist.");
            myProjectTypeStatesTab.DeleteState(stateName);
            Assert.IsFalse(myProjectTypeStatesTab.VerifyStateExists(stateName), "delMe exists when not expected.");
        }

        [Test]
        public void AssignProjectViewForState()
        {
			var myDataTypeViewsTab = new CCWebUIAuto.Pages.BasePages.DataTypeCenter.ViewsTab("ProjectStatus");
	        var viewName = "TestView-For-State-" + DataGen.String(4);
            Store.LoginAsUser(Users.Admin);
            myDataTypeViewsTab.NavigateTo();
			myDataTypeViewsTab.CreateNewView(viewName);
			Wait.Until(d => myDataTypeViewsTab.ViewExists(viewName));

            var myProjectTypeStatesTab = new StatesTab(TestProjType);

            myProjectTypeStatesTab.NavigateTo();
			myProjectTypeStatesTab.EditProperties(viewName);
        }
    }
}
