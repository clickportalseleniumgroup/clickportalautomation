﻿using System.Linq;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages.BasePages.ProjectTypeCenter;
using CCWebUIAuto.PrimitiveElements;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
    public class ProjectTypeCenterSmartFormTabTests : BaseTest
    {
        private const string UseProjType = "_VerySmallProjType";

        [Test(Description="1.9.1 Create, Edit, and Delete Steps")]
        public void ManageSteps()
        {
            var stepName = "1_9_1_Step_Name_" + DataGen.String(4);
            var modStepName = "1_9_1_Step_Name_" + DataGen.String(4);
            var smartFormTab = new SmartFormTabManageSteps(UseProjType);

            Store.LoginAsUser(Users.Admin);
            smartFormTab.NavigateTo();
            smartFormTab.CreateStep(stepName, "TheView");
            Assert.IsTrue(new Link(By.LinkText(stepName)).Exists);

            smartFormTab.EditStep(stepName, modStepName);
			Assert.IsFalse(new Link(By.LinkText(stepName)).Exists);
			Assert.IsTrue(new Link(By.LinkText(modStepName)).Exists);

            smartFormTab.DeleteStep(modStepName);
			Assert.IsFalse(new Link(By.LinkText(stepName)).Exists);
			Assert.IsFalse(new Link(By.LinkText(modStepName)).Exists);
        }

        [Test(Description="1.9.2 Create branching criteria")]
        public void CreateBranchingCriteria()
        {
            var stepA = "1_9_2_" + DataGen.String(4);
            var stepB = "1_9_2_" + DataGen.String(4);
            var smartFormTab = new SmartFormTabManageSteps(UseProjType);

            Store.LoginAsUser(Users.Admin);
            smartFormTab.NavigateTo();
            smartFormTab.CreateStep(stepA, "TheView");
            smartFormTab.CreateStep(stepB, "TheView");
            smartFormTab.AddBranch(stepA, stepB);
            smartFormTab.AddBranchCriteria(stepA, stepB, "gBoolean", "True");
            var criteria = smartFormTab.GetBranchCriteria(stepA, stepB).ToList();
            Assert.IsTrue(criteria.Contains("gBoolean = True"));
            
            // cleanup steps, to keep lists from getting too long for other steps to run normally
            smartFormTab.DeleteStep(stepA);
            smartFormTab.DeleteStep(stepB);
        }

        [Test(Description="1.9.3 Copy branches, don't erase existing branches")]
        public void CopyBranchNoErase()
        {
            var stepA = "1_9_3_" + DataGen.String(4);
            var stepB = "1_9_3_" + DataGen.String(4);
            var stepC = "1_9_3_" + DataGen.String(4);
            var smartFormTab = new SmartFormTabManageSteps(UseProjType);

            Store.LoginAsUser(Users.Admin);
            smartFormTab.NavigateTo();
            smartFormTab.CreateStep(stepA, "TheView");
            smartFormTab.CreateStep(stepB, "TheView");
            smartFormTab.CreateStep(stepC, "TheView");
            smartFormTab.AddBranch(stepA, stepB);
            smartFormTab.AddBranchCriteria(stepA, stepB, "gBoolean", "True");
            smartFormTab.AddBranch(stepC, stepA);
            smartFormTab.AddBranchCriteria(stepC, stepA, "gBoolean", "True");
            smartFormTab.CopyBranching(stepA, stepC);
            var criteriaA = smartFormTab.GetBranchCriteria(stepC, stepB);
            var criteriaB = smartFormTab.GetBranchCriteria(stepC, stepA);
            Assert.IsTrue(criteriaA.Contains("gBoolean = True"));
            Assert.IsTrue(criteriaB.Contains("gBoolean = True"));

            // cleanup steps, to keep lists from getting too long for other steps to run normally
            smartFormTab.DeleteStep(stepA);
            smartFormTab.DeleteStep(stepB);
            smartFormTab.DeleteStep(stepC);
        }

        [Test(Description = "1.9.3 Copy branches, don't erase existing branches")]
        public void CopyBranchEraseExisting()
        {
            var stepA = "1_9_3_" + DataGen.String(4);
            var stepB = "1_9_3_" + DataGen.String(4);
            var stepC = "1_9_3_" + DataGen.String(4);
            var smartFormTab = new SmartFormTabManageSteps(UseProjType);

            Store.LoginAsUser(Users.Admin);
            smartFormTab.NavigateTo();
            smartFormTab.CreateStep(stepA, "TheView");
            smartFormTab.CreateStep(stepB, "TheView");
            smartFormTab.CreateStep(stepC, "TheView");
            smartFormTab.AddBranch(stepA, stepB);
            smartFormTab.AddBranchCriteria(stepA, stepB, "gBoolean", "True");
            smartFormTab.AddBranch(stepC, stepA);
            smartFormTab.AddBranchCriteria(stepC, stepA, "gBoolean", "True");
            smartFormTab.CopyBranching(stepA, stepC, eraseAll: true);
            var criteriaA = smartFormTab.GetBranchCriteria(stepC, stepB);
            Assert.IsTrue(criteriaA.Contains("gBoolean = True"));

			var lnkBranchCriteria = new Link(
                By.XPath("(//*[@id='_webrRSV_DIV_0']/table/tbody/tr/td[3]/a[text()='" + stepC + "" +
                         "'])[1]/../../td[4]/table/tbody/tr/td/table/tbody/tr/td[2]/span[text()='" + stepA +
                         "']/../../td[1]/a"));
            Assert.IsFalse(lnkBranchCriteria.Exists);

            // cleanup steps, to keep lists from getting too long for other steps to run normally
            smartFormTab.DeleteStep(stepA);
            smartFormTab.DeleteStep(stepB);
            smartFormTab.DeleteStep(stepC);
        }

        [Test(Description = "1.9.4 Create, edit, and delete sections")]
        public void ManageSections()
        {
            var step = "1_9_4_" + DataGen.String(4);
            var modStep = "1_9_4_" + DataGen.String(4);
            var section = "1_9_4_Section_" + DataGen.String(4);
            var modSection = "1_9_4_Section_" + DataGen.String(4);

            var stepsTab = new SmartFormTabManageSteps(UseProjType);
			var sectionsTab = new SmartFormTabManageSections(UseProjType);

            Store.LoginAsUser(Users.Admin);
            stepsTab.NavigateTo();
            stepsTab.CreateStep(step, "TheView");
            stepsTab.CreateStep(modStep, "TheView");
            sectionsTab.NavigateTo();
            sectionsTab.CreateSection(section, null, true, new[] { step });
			var stepLink = new Link(By.LinkText(step));
			var sectionLink = new Link(By.LinkText(section));
            Assert.IsTrue(stepLink.Exists);
            Assert.IsTrue(sectionLink.Exists);

            sectionsTab.EditSection(section, modSection, stepNames: new[] { modStep });
			var modStepLink = new Link(By.LinkText(modStep));
			var modSectionLink = new Link(By.LinkText(modSection));
            Assert.IsFalse(stepLink.Exists);
            Assert.IsFalse(sectionLink.Exists);
            Assert.IsTrue(modStepLink.Exists);
            Assert.IsTrue(modSectionLink.Exists);

            sectionsTab.DeleteSection(modSection);
            Assert.IsFalse(stepLink.Exists);
            Assert.IsFalse(sectionLink.Exists);
            Assert.IsFalse(modStepLink.Exists);
            Assert.IsFalse(modSectionLink.Exists);

            // cleanup steps, to keep lists from getting too long for other steps to run normally
            stepsTab.NavigateTo();
            stepsTab.DeleteStep(step);
            stepsTab.DeleteStep(modStep);
        }
    }
}