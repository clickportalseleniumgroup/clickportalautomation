﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CCWebUIAuto;
using CCWebUIAuto.Helpers;
using CCWebUIAuto.Pages;
using CCWebUIAuto.Pages.BasePages;
using CCWebUIAuto.PrimitiveElements;
using ClickPortal.UI.Tests.Automation.Pages.TestStore;
using ClickPortal.UI.Tests.Automation.Pages.TestStore.TestProjType;
using ClickPortal.UI.Tests.Automation.TestStore.TestProjType;
using NUnit.Framework;
using OpenQA.Selenium;

namespace ClickPortal.UI.Tests.Automation.TestCases
{
	public class ActivitiesExecution : BaseTest
	{
		[Test(Description = "Executes the activity 'Confirm Login Without Activity' with the system administrator credentials")]
		public void ExecuteConfirmLoginWithoutMessageActivity()
		{
			//Navigate to the workspace
			var workspace = new DefaultTemplate("X-00000001");
			Store.LoginAsUser(Users.Admin);
			workspace.NavigateTo();

			//Execute the activity
			workspace.LnkConfirmLoginNoMessageActivity.Click();

			//Confirm credentials on the activity form
			var activityForm = new ConfirmLoginNoMessageActivity(workspace.InstanceId);
			activityForm.SwitchTo();
			activityForm.TxtNotesAsStr.Value = DataGen.AllLowerCaseString(5);
			activityForm.BtnOk.Click();

			//Login confirmation dialog pops up. Submit the credentials
			activityForm.DlgLoginConfirmation.SubmitCredentials("administrator", "1234");
			activityForm.SwitchBackToParent(WaitForPopupToClose.Yes);

			//Wait until the parent page refreshes and the activity appears in the project history
			Wait.Until(d => (new Link(By.XPath("//td[contains(@id, '_filterBar')]/../..//a[text()='Confirm Login without Message']"))).Exists);
		}
	}
}
